mergeInto(LibraryManager.library, {
  ShowVideo: function (url) {
    var video = document.getElementById('video-player');
    var source = document.createElement('source');
    source.setAttribute('src', UTF8ToString(url));
    source.setAttribute('type', 'video/mp4');
    video.appendChild(source);
    video.style.display = "block";
    video.load();
    video.play();
    video.focus();
  },
  HideVideo: function () {
    var video = document.getElementById('video-player');
    video.pause();
    video.style.display = "none";
    while (video.firstChild) {
        video.removeChild(video.firstChild);
    }
  },
  HideLoader: function () {
    var loader = document.getElementById("loader");
    loader.className = "loader";
    setTimeout(function () {
      loader.style.display = "none";
    }, 3000);
  }
});