using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class WaitShow : Mb {
    public float wait;
    public bool isOut;
    void Awake() { StaCor(SclCor(0.3f)); }
    IEnumerator SclCor(float tm) {
        Tls = V3.O;
        yield return Wf.Sec(wait);
        for (float t = 0; t < tm; t += Dt) {
            Tls = V3.V(M.Lerp(t / tm, isOut ? EaseTp.OutBack : EaseTp.OutQuart));
            yield return null;
        }
        Tls = V3.I;
    }
}
