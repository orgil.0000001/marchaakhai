using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StartMenu))]
public class StartEd : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        if (GUILayout.Button("Product Name"))
            UnityEditor.PlayerSettings.productName = "" + ((StartMenu)target).tp;
    }
}
