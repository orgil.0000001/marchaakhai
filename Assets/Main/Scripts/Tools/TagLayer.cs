namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Joystick, Dead, Target, Canvas, Coin, Balloon, Foods, Box, Basket, Bido, btnSound, Particle, Missile, DragCard, drag, ChooseCard, Card, DragBasket }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Water = 4, UI = 5, PostProcessing = 8; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Water = 16, UI = 32, PostProcessing = 256; }
}