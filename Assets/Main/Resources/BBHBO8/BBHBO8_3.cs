using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO8_3 : GameMb {
    BBHBO8 main => BBHBO8._;
    enum ToolTp { Sun, Shadow, Can, Sprayer, Shovel }
    class Btn {
        public Transform tf;
        public string name;
        public Song song;
        public List<ToolTp> tools;
        public Btn(Transform tf, string name, Song song, params ToolTp[] tools) {
            this.tf = tf;
            this.name = name;
            this.song = song;
            this.tools = tools.Lis();
        }
    }
    List<Btn> datas;
    List<Transform> tfs;
    Text nameTxt;
    Transform flowerTf, boxTf;
    int idx = 0;
    public override void Init() {
        data.txt.text = Song.txts[0][3].txt;
        for (int i = 0; i < 5; i++) {
            ToolTp tp = (ToolTp)i;
            go.Child(1, i).Btn().onClick.AddListener(() => StaCor(ClkBtnCor(tp)));
        }
        boxTf = go.Child(2);
        Transform pTf = go.Child(3);
        flowerTf = pTf.ChildName("Flower");
        tfs = Lis(pTf.ChildName("Sun"), pTf.ChildName("Shadow"), pTf.ChildName("Can"), pTf.ChildName("Sprayer"), pTf.ChildName("Shovel"));
        nameTxt = go.Child(3, 8).Txt();
        datas = Lis(
            new Btn(flowerTf.Child(0), Song.txts[6][0].txt, new Song(BBHBO8.path, "3_1"), ToolTp.Sun, ToolTp.Can, ToolTp.Sun, ToolTp.Can),
            new Btn(flowerTf.Child(1), Song.txts[6][1].txt, new Song(BBHBO8.path, "3_2"), ToolTp.Sun, ToolTp.Shovel, ToolTp.Can),
            new Btn(flowerTf.Child(2), Song.txts[6][2].txt, new Song(BBHBO8.path, "3_3"), ToolTp.Shadow, ToolTp.Sprayer, ToolTp.Shadow, ToolTp.Sprayer),
            new Btn(flowerTf.Child(3), Song.txts[6][3].txt, new Song(BBHBO8.path, "3_4"), ToolTp.Sun, ToolTp.Can, ToolTp.Sun)
        );
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            datas.RndShuffle();
            tfs.ForEach(e => e.Tls(V3.O));
            boxTf.Tls(V3.O);
            main.Fill(0);
            B.LevelBar();
        }
        idx = 0;
        datas.ForEach(d => d.tf.Hide());
        datas[B.gameIdx].tf.Show();
        Show();
        nameTxt.text = datas[B.gameIdx].name;
        StaCor(StaBoxCor());
        main.PlaySong();
    }
    void Show() {
        if (idx < datas[B.gameIdx].tools.Count) {
            for (int i = 0; i < datas[B.gameIdx].tools.Count; i++)
                datas[B.gameIdx].tf.ChildAct(i == idx, i);
            flowerTf.Tls(V3.I);
        }
    }
    IEnumerator StaBoxCor() {
        isClk = true;
        yield return Wf.Sec(0.5f);
        yield return BoxCor(true, datas[B.gameIdx].tools[idx]);
        isClk = false;
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        var e = isShow ? (V3.O, V3.V(0.8f), EaseTp.OutSine) : (V3.V(0.8f), V3.O, EaseTp.InSine);
        yield return SclCor(flowerTf.ParGo(), e.Item1, e.Item2, tm, e.Item3);
    }
    IEnumerator BoxCor(bool isBox, ToolTp tp = default, float tm = 0.2f) {
        if (isBox) {
            for (int i = 0; i < 5; i++)
                boxTf.ChildAct(i == (int)tp, 0, i);
            yield return SclCor(boxTf.gameObject, V3.O, V3.I, tm, EaseTp.OutSine);
        } else {
            yield return SclCor(boxTf.gameObject, V3.I, V3.O, tm, EaseTp.InSine);
        }
    }
    bool isClk = false;
    List<Vector3> sun = Lis(V3.I, V3.V(1.1f));
    List<float> umbrella = Lis(-20f, -30), can = Lis(10f, 20, 0, 1), sprayer = Lis(0f, 10, 0, 1, 0, 25);
    List<Vector3> shovel = Lis(V3.Xy(160, 180), V3.Xy(120, 100), V3.Xy(60, 30), V3.Xy(0, 100));
    IEnumerator ClkBtnCor(ToolTp tp, float tm = 0.2f, float tTm = 4) {
        if (!isClk) {
            isClk = true;
            Transform pTf = tfs[(int)tp];
            int cnt = 5;
            bool isCorrect = tp == datas[B.gameIdx].tools[idx];
            B.PlaySong(isCorrect);
            if (isCorrect)
                yield return BoxCor(false);
            if (tp == ToolTp.Sun) {
                yield return SclCor(pTf.gameObject, V3.O, V3.I, tm, EaseTp.OutSine);
                float tm2 = tTm / (cnt * 2);
                for (int i = 0; i < cnt; i++) {
                    yield return SclCor(pTf.gameObject, sun[0], sun[1], tm2, EaseTp.OutSine);
                    yield return SclCor(pTf.gameObject, sun[1], sun[0], tm2, EaseTp.InSine);
                }
                yield return SclCor(pTf.gameObject, V3.I, V3.O, tm, EaseTp.InSine);
            } else if (tp == ToolTp.Shadow) {
                pTf.Tle(0, 0, umbrella[0]);
                yield return SclCor(pTf.gameObject, V3.O, V3.I, tm, EaseTp.OutSine);
                float tm2 = tTm / (cnt * 2);
                for (int i = 0; i < cnt; i++) {
                    yield return Cor(t => pTf.Tle(0, 0, M.Lerp(umbrella[0], umbrella[1], t)), tm2);
                    yield return Cor(t => pTf.Tle(0, 0, M.Lerp(umbrella[1], umbrella[0], t)), tm2);
                }
                yield return SclCor(pTf.gameObject, V3.I, V3.O, tm, EaseTp.InSine);
            } else if (tp == ToolTp.Can) {
                Image img = pTf.Child(1).Img();
                pTf.Tle(0, 0, can[0]);
                img.color = img.color.A(can[2]);
                yield return SclCor(pTf.gameObject, V3.O, V3.I, tm, EaseTp.OutSine);
                float tm2 = tTm / (cnt * 2);
                for (int i = 0; i < cnt; i++) {
                    yield return Cor(t => {
                        pTf.Tle(0, 0, M.Lerp(can[0], can[1], t));
                        img.color = img.color.A(M.Lerp(can[2], can[3], t));
                    }, tm2);
                    yield return Cor(t => {
                        pTf.Tle(0, 0, M.Lerp(can[1], can[0], t));
                        img.color = img.color.A(M.Lerp(can[3], can[2], t));
                    }, tm2);
                }
                yield return SclCor(pTf.gameObject, V3.I, V3.O, tm, EaseTp.InSine);
            } else if (tp == ToolTp.Sprayer) {
                Image img = pTf.Child(2).Img();
                Transform tf2 = pTf.Child(0);
                pTf.Tle(0, 0, sprayer[0]);
                img.color = img.color.A(sprayer[2]);
                tf2.Tle(0, 0, sprayer[4]);
                yield return SclCor(pTf.gameObject, V3.O, V3.I, tm, EaseTp.OutSine);
                float tm2 = tTm / (cnt * 2);
                for (int i = 0; i < cnt; i++) {
                    yield return Cor(t => {
                        pTf.Tle(0, 0, M.Lerp(sprayer[0], sprayer[1], t));
                        img.color = img.color.A(M.Lerp(sprayer[2], sprayer[3], t));
                        tf2.Tle(0, 0, M.Lerp(sprayer[4], sprayer[5], t));
                    }, tm2);
                    yield return Cor(t => {
                        pTf.Tle(0, 0, M.Lerp(sprayer[1], sprayer[0], t));
                        img.color = img.color.A(M.Lerp(sprayer[3], sprayer[2], t));
                        tf2.Tle(0, 0, M.Lerp(sprayer[5], sprayer[4], t));
                    }, tm2);
                }
                yield return SclCor(pTf.gameObject, V3.I, V3.O, tm, EaseTp.InSine);
            } else if (tp == ToolTp.Shovel) {
                yield return Cor(t => {
                    pTf.Tlp(V3.Lerp(shovel[0], shovel[1], t));
                    pTf.Tls(V3.Lerp(V3.O, V3.I, t));
                }, tm);
                float tm2 = tTm / (cnt * 3);
                for (int i = 0; i < cnt; i++) {
                    yield return Cor(t => pTf.Tlp(V3.Lerp(shovel[1], shovel[2], t)), tm2);
                    yield return Cor(t => pTf.Tlp(V3.Lerp(shovel[2], shovel[3], t)), tm2);
                    yield return Cor(t => pTf.Tlp(V3.Lerp(shovel[3], shovel[1], t)), tm2);
                }
                yield return Cor(t => {
                    pTf.Tlp(V3.Lerp(shovel[1], shovel[0], t));
                    pTf.Tls(V3.Lerp(V3.I, V3.O, t));
                }, tm);
            }
            if (isCorrect) {
                idx++;
                // yield return SclCor(flowerTf.gameObject, V3.I, V3.V(1.1f), 0.1f);
                Show();
                if (idx == datas[B.gameIdx].tools.Count) {
                    B.PlaySong(datas[B.gameIdx].song);
                    yield return Wf.Sec(datas[B.gameIdx].song.len);
                    B.Good();
                    yield return Wf.Sec(2);
                    StaCor(main.WinCor());
                } else {
                    yield return BoxCor(true, datas[B.gameIdx].tools[idx]);
                }
            } else if (idx > 0) {
                idx--;
                // yield return SclCor(flowerTf.gameObject, V3.V(0.9f), V3.I, 0.1f);
                Show();
                yield return BoxCor(false);
                yield return BoxCor(true, datas[B.gameIdx].tools[idx]);
            }
            isClk = false;
        }
    }
}