using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO8_2 : GameMb {
    BBHBO8 main => BBHBO8._;
    List<RectTransform> rts = new List<RectTransform>(), rts2 = new List<RectTransform>();
    Transform pTf, parTf;
    RectTransform rt;
    Vector3 rtP;
    List<(Sprite, string, string)> datas = new List<(Sprite, string, string)>();
    public override void Init() {
        data.txt.text = Song.txts[0][2].txt;
        for (int i = 0; i < 4; i++)
            for (int j = 1; j < Song.txts[2 + i].Count; j++)
                datas.Add((O.LoadSpr(BBHBO8.path + "z2_" + (i + 1) + "_" + j), Song.txts[2 + i][j].txt, "" + i));
        pTf = go.Child(1, 1, 0);
        for (int i = 0; i < pTf.childCount; i++)
            rts.Add(pTf.Child(i).Rt());
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
        }
        datas.RndShuffle();
        rts.ForEach(rt => { rt.Par(pTf); rt.Tls(V3.I); });
        List<int> l = Lis(0, 1, 2, 3);
        l.RndShuffle();
        rts2.Clear();
        for (int i = 0; i < l.Count; i++) {
            go.Child(1, 0, 2, i, 0).Txt().text = Song.txts[2 + l[i]][0].txt;
            rts2.Add(go.Child(1, 0, 3, i).Rt());
            rts2[i].name = "" + l[i];
        }
        for (int i = 0; i < rts.Count; i++) {
            rts[i].Child(0).Img().sprite = datas[i].Item1;
            rts[i].Child(1).Txt().text = datas[i].Item2;
            rts[i].name = datas[i].Item3;
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    int idx;
    void Update() {
        if (B.lvlIdx == 1 && !main.isWin) {
            if (IsMbD) {
                rt = rts.Find(btn => btn.IsRt(Mp));
                if (rt && rt.parent == pTf) {
                    rtP = rt.Tp();
                    mp = Mp;
                    parTf = rt.parent;
                    idx = rt.SibIdx();
                    rt.Par(pTf.Par());
                } else {
                    rt = null;
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + Mp - mp);
            }
            if (IsMbU && rt) {
                var pTf2 = rts2.Find(par => par.IsRt(Mp));
                if (pTf2 && rt.name == pTf2.name) {
                    B.PlaySong(true);
                    rt.Par(pTf2);
                    if (pTf.childCount == 0) StaCor(Cor());
                } else {
                    B.PlaySong(false);
                    rt.Par(parTf);
                    rt.SibIdx(idx);
                }
                main.Fill(12 - pTf.childCount);
            }
        }
    }
    IEnumerator Cor() {
        B.Good();
        yield return Wf.Sec(2);
        StaCor(main.WinCor());
    }
}