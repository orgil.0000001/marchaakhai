using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO2_1 : GameMb {
    BBHBO2 main => BBHBO2._;
    static Transform pTf, parTf;
    class Btn {
        public RectTransform rt;
        Vector3 pos;
        Vector2 sz;
        public bool isE => rt.name.IsS("e") && rt.parent == pTf;
        public Btn(int i) {
            rt = pTf.Child(i).Rt();
            pos = rt.Tlp();
            sz = rt.sizeDelta;
        }
        public void Reset(int i) {
            rt.Par(pTf);
            rt.SibIdx(i);
            rt.Tlp(pos);
            rt.sizeDelta = sz;
            rt.Tls(V3.I);
        }
    }
    List<Btn> rts = new List<Btn>();
    bool isClk = false, isWin = false;
    int cnt = 0;
    public override void Init() {
        data.txt.text = Song.txts[0][1].txt;
        pTf = go.Child(0, 0);
        parTf = go.Child(1, 0, 0);
        for (int i = 0; i < pTf.childCount; i++)
            rts.Add(new Btn(i));
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
        }
        cnt = 0;
        isWin = false;
        for (int i = 0; i < rts.Count; i++)
            rts[i].Reset(i);
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    void Update() {
        if (B.lvlIdx == 0 && !isWin) {
            if (IsMbD && !isClk) {
                var l = rts.FindAll(e => e.rt.IsRt(Mp));
                if (l.IsCount() && l.Last().isE) {
                    B.PlaySong(true);
                    StaCor(MvCor(l.Last()));
                } else if (pTf.Rt().IsRt(Mp)) {
                    B.PlaySong(false);
                }
            }
        }
    }
    IEnumerator MvCor(Btn e, float w = 160, float sclTm = 0.2f, float waitTm = 0.1f, float mvTm = 0.2f) {
        isClk = true;
        var group = parTf.Gc<VerticalLayoutGroup>();
        float y = parTf.localPosition.y;
        var rt = parTf.parent.Rt();
        e.rt.Par(rt.Par(1));
        Vector2 sz0 = e.rt.sizeDelta, sz1 = e.rt.sizeDelta * 1.2f, sz2 = V2.V(w, sz0.y / sz0.x * w);
        Vector3 p0 = rt.TfInvPnt(e.rt.Tp()), p1 = V3.Y(rt.sizeDelta.y / 2 - sz2.y / 2 - group.padding.top);
        yield return Cor(t => e.rt.sizeDelta = V2.Lerp(sz0, sz1, t), sclTm, EaseTp.OutSine);
        yield return Wf.Sec(waitTm);
        yield return Cor(t => {
            e.rt.Tp(rt.TfPnt(V3.Lerp(p0, p1, t)));
            e.rt.sizeDelta = V2.Lerp(sz1, sz2, t);
            parTf.TlpY(M.Lerp(y, y - sz2.y - group.spacing, t));
        }, mvTm);
        main.Fill(++cnt);
        parTf.TlpY(y);
        e.rt.Par(parTf);
        e.rt.SibIdx(0);
        if (!rts.Any(e => e.isE)) {
            isWin = true;
            yield return Wf.Sec(2);
            StaCor(main.WinCor());
        }
        isClk = false;
    }
}