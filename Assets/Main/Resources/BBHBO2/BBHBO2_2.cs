using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO2_2 : GameMb {
    BBHBO2 main => BBHBO2._;
    List<Sprite> corrects = new List<Sprite>(), wrongs = new List<Sprite>();
    List<Button> btns = new List<Button>();
    public Sprite defSpr, correctSpr, wrongSpr;
    public override void Init() {
        data.txt.text = Song.txts[0][2].txt;
        for (int i = 1; i <= 5; i++)
            corrects.Add(O.LoadSpr(BBHBO2.path + "z2z" + i));
        for (int i = 1; i <= 10; i++)
            wrongs.Add(O.LoadSpr(BBHBO2.path + "z2b" + i));
        for (int i = 0; i < 2; i++) {
            Button btn = go.Child(1, i).Btn();
            btns.Add(btn);
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn)));
        }
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            corrects.RndShuffle();
            wrongs.RndShuffle();
            main.Fill(0);
            B.LevelBar();
        }
        for (int i = 0, j = Rnd.B ? 1 : 0; i < 2; i++) {
            btns[i].Img().sprite = defSpr;
            btns[i].name = i == j ? "1" : "0";
            btns[i].Child(0, 0).Img().sprite = i == j ? corrects[B.gameIdx] : wrongs[B.gameIdx];
        }
        main.PlaySong();
    }
    bool isClk = false;
    IEnumerator BtnClkCor(Button btn) {
        if (!isClk) {
            isClk = true;
            bool isCorrect = btn.name == "1";
            btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
            B.Correct(isCorrect);
            yield return Wf.Sec(2);
            if (!isCorrect) B.gameIdx--;
            else main.Fill(B.gameIdx + 1);
            StaCor(main.WinCor());
            isClk = false;
        }
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(btns, tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
    void Update() {
        if (B.lvlIdx == 1) {
        }
    }
}