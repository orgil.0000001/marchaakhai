using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO2_3 : GameMb {
    BBHBO2 main => BBHBO2._;
    List<Sprite> corrects = new List<Sprite>(), wrongs = new List<Sprite>();
    List<Button> btns = new List<Button>();
    public Button btnPf;
    public Vector2 btnSz, imgSz, spc;
    public Sprite faceSpr, backSpr;
    Vector3Int n = V3I.V(7, 2, 7);
    List<Sprite> sprs = new List<Sprite>();
    public override void Init() {
        data.txt.text = Song.txts[0][3].txt;
        Transform parTf = go.Child(1);
        for (int i = 1; i <= n.z; i++)
            sprs.Add(O.LoadSpr(BBHBO2.path + "z3_" + i));
        for (int i = 0; i < n.z * 2; i++) {
            Button btn = Ins(btnPf, parTf);
            btn.Tlp(V3.Xy((i % n.x).X(n.x, btnSz.x, spc.x), -(i / n.x).X(n.y, btnSz.y, spc.y)));
            btn.Rt().sizeDelta = btnSz;
            btn.Child(0).Rt().sizeDelta = imgSz;
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn)));
            btns.Add(btn);
        }
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            clkBtns.Clear();
            List<int> l = new List<int>();
            for (int i = 0; i < btns.Count; i++) l.Add(i % n.z);
            l.RndShuffle();
            for (int i = 0; i < btns.Count; i++) {
                btns[i].Img().sprite = backSpr;
                btns[i].Child(0, 0).Img().sprite = sprs[l[i]];
                btns[i].ChildHide(0);
                btns[i].name = "" + l[i];
            }
            main.Fill(0);
            B.LevelBar();
        }
        main.PlaySong();
    }
    bool isClk = false;
    List<Button> clkBtns = new List<Button>();
    IEnumerator BtnClkCor(Button btn, float tm = 0.2f) {
        if (!isClk && !clkBtns.Contains(btn)) {
            isClk = true;
            clkBtns.Add(btn);
            yield return SclCor(btn.gameObject, V3.I, V3.zpp, tm, EaseTp.InSine);
            btn.Img().sprite = faceSpr;
            btn.ChildShow(0);
            yield return SclCor(btn.gameObject, V3.zpp, V3.I, tm, EaseTp.OutSine);
            if (clkBtns.Count.IsEven()) {
                Button btn0 = clkBtns.Last(1), btn1 = clkBtns.Last();
                if (btn0.name == btn1.name) {
                    B.PlaySong(true);
                } else {
                    B.PlaySong(false);
                    yield return Wf.Sec(1);
                    StaCor(SclCor(btn0.gameObject, V3.I, V3.zpp, tm, EaseTp.InSine));
                    StaCor(SclCor(btn1.gameObject, V3.I, V3.zpp, tm, EaseTp.InSine));
                    yield return Wf.Sec(tm);
                    btn0.Img().sprite = backSpr;
                    btn1.Img().sprite = backSpr;
                    btn0.ChildHide(0);
                    btn1.ChildHide(0);
                    StaCor(SclCor(btn0.gameObject, V3.zpp, V3.I, tm, EaseTp.OutSine));
                    StaCor(SclCor(btn1.gameObject, V3.zpp, V3.I, tm, EaseTp.OutSine));
                    yield return Wf.Sec(tm);
                    clkBtns.RmvAtLast();
                    clkBtns.RmvAtLast();
                }
            }
            if (clkBtns.Count == btns.Count) {
                yield return Wf.Sec(2);
                StaCor(main.WinCor());
            }
            isClk = false;
        }
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
}