using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO1_3 : GameMb {
    BBHBO1 main => BBHBO1._;
    public GameObject photoGo;
    List<RectTransform> rts = new List<RectTransform>(), rts2 = new List<RectTransform>();
    Transform pTf, parTf;
    RectTransform rt;
    Vector3 rtP;
    CanvasGroup photoCg;
    Image flashImg, picImg;
    GameObject picGo;
    bool isTake = false, isPic = false;
    int idx;
    public override void Init() {
        data.txt.text = Song.txts[0][5].txt;
        pTf = go.Child(1, 1, 0);
        Transform pTf2 = go.Child(1, 0, 3);
        for (int i = 0; i < 3; i++) {
            Text txt = go.Child(1, 0, 2, i, 0).Txt();
            txt.text = Song.txts[0][6 + i].txtU;
            txt.fontSize = Song.IsKazak ? 24 : Song.IsTuva ? 22 : 35;
        }
        for (int i = 0; i < pTf.childCount; i++)
            rts.Add(pTf.Child(i).Rt());
        for (int i = 0; i < pTf2.childCount; i++)
            rts2.Add(pTf2.Child(i).Rt());
        picGo = go.ChildGo(1, 0, 4);
        photoGo.Par(tf);
        photoCg = photoGo.Child<CanvasGroup>(1);
        flashImg = photoCg.Sib(0).Img();
        picImg = photoCg.Child(0, 0).Img();
        var sz = photoCg.Sib(2).Rt();
        photoCg.Rt().sizeDelta = sz.sizeDelta + V2.V(60);
        photoCg.Child(0).Rt().sizeDelta = sz.sizeDelta;
        picImg.Tlp(-sz.Tlp());
        picImg.Rt().sizeDelta = photoCg.transform.root.Rt().sizeDelta;
    }
    public override void CrtData() {
        isTake = isPic = false;
        picGo.Tls(V3.O);
        photoGo.Hide();
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
        }
        rts.ForEach(rt => { rt.Par(pTf); rt.Tls(V3.I); });
        List<int> l = new List<int>();
        for (int i = 0; i < Song.txts[8].Count; i++) l.Add(i);
        l = Rnd.Lis(rts.Count, l);
        for (int i = 0; i < rts.Count; i++) {
            rts[i].Child(0).Img().sprite = O.LoadSpr(BBHBO1.path + "z3_" + (l[i] + 1));
            rts[i].Child(1).Txt().text = Song.txts[8][l[i]].txt;
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    void Update() {
        if (B.lvlIdx == 2 && !main.isWin) {
            if (IsMbD) {
                rt = rts.Find(btn => btn.IsRt(Mp));
                if (rt) {
                    rtP = rt.Tp();
                    mp = Mp;
                    idx = rt.SibIdx();
                    parTf = rt.parent;
                    rt.Par(pTf.Par());
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + Mp - mp);
            }
            if (IsMbU && rt) {
                var pTf2 = rts2.Find(par => par.IsRt(Mp));
                if (pTf2 && pTf2.childCount < 4) {
                    B.PlaySong(true);
                    rt.Par(pTf2);
                } else {
                    B.PlaySong(false);
                    rt.Par(parTf);
                    rt.SibIdx(idx);
                }
                if (rts2[0].childCount > 0 && rts2[1].childCount > 0 && rts2[2].childCount > 0) {
                    if (!isPic) {
                        isPic = true;
                        StaCor(CamCor());
                    }
                } else if (isPic) {
                    isPic = false;
                    StaCor(SclCor(picGo, picGo.Tls(), V3.O, 0.2f, EaseTp.OutSine));
                }
            }
        }
    }
    IEnumerator CamCor(float tm = 0.8f) {
        yield return SclCor(picGo, V3.O, V3.I, 0.2f, EaseTp.InSine);
        for (float t = 0; isPic && !isTake; t += Dt) {
            picGo.Tls(V3.V(M.Remap(t % tm < tm / 2 ? t % tm : tm - t % tm, 0, tm / 2, 1, 1.15f)));
            yield return null;
        }
    }
    public void Take() {
        if (!isTake)
            StaCor(TakeCor());
    }
    IEnumerator TakeCor(float flashTm = 0.5f, float sclTm = 0.2f, float sclATm = 0.05f, float showTm = 1) {
        B.PlaySong(O.LoadClip(BBHBO1.path + "camera"));
        isTake = true;
        photoGo.Show();
        photoCg.alpha = 0;
        yield return new WaitForEndOfFrame();
        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        tex.Apply();
        StaCor(Cor(t => flashImg.color = flashImg.color.A(1 - t), 0.5f));
        picImg.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), V2.V(0.5f, 0.5f), 100);
        photoCg.alpha = 1;
        picImg.color = picImg.color.A(0);
        yield return SclCor(photoCg.gameObject, V3.V(0.8f), V3.I, sclTm, EaseTp.OutSine);
        yield return Wf.Sec(flashTm - sclTm);
        yield return Cor(t => picImg.color = picImg.color.A(t), showTm);
        yield return Wf.Sec(2);
        // StaCor(SclCor(photoCg.gameObject, V3.I, V3.V(0.8f), sclTm, EaseTp.InSine));
        // yield return Wf.Sec(sclTm - sclATm);
        // yield return Cor(t => photoCg.alpha = 1 - t, sclATm);
        StaCor(main.WinCor());
    }
}