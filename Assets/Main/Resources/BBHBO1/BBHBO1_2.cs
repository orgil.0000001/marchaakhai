using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO1_2 : GameMb {
    BBHBO1 main => BBHBO1._;
    public RectTransform btnTxtPf, btnImgPf, linePf;
    public int ansCnt = 4;
    public Vector2 btnSz = V2.V(150), btnImgSz = V2.V(130), btnSpc = V2.V(10, 200);
    RectTransform rt, lineRt;
    List<RectTransform> lines = new List<RectTransform>(), sels = new List<RectTransform>();
    List<List<RectTransform>> rts = new List<List<RectTransform>>() { new List<RectTransform>(), new List<RectTransform>() };
    bool isAnim = false;
    Transform parTf;
    class Ans {
        public string name;
        public List<((string, Sprite), (string, Sprite))> data = new List<((string, Sprite), (string, Sprite))>();
        public ((string, Sprite), (string, Sprite)) get;
        public Ans(int num, int n, params int[] a) {
            var l = Song.txts[num + 3];
            name = "" + num;
            for (int i = 0; i < a.Length; i++)
                data.Add(((l[a[i]].txt, O.LoadSpr(BBHBO1.path + "z2_" + num + "t" + (a[i] + 1))),
                    (l[n + i].txt, O.LoadSpr(BBHBO1.path + "z2_" + num + "_" + (i + 1)))));
        }
        public void Rnd() { get = data.Rnd(); }
    }
    List<Ans> answers;
    Vector3 sz2;
    public override void Init() {
        sz2 = V3.Scl((main.Rt().sizeDelta / 2).V3(), main.Tls);
        data.txt.text = Song.txts[0][4].txt;
        answers = Lis(new Ans(1, 1, 0, 0, 0, 0), new Ans(2, 2, 0, 1),
            new Ans(3, 1, 0, 0, 0), new Ans(4, 2, 0, 0, 0, 1));
        parTf = go.Child(1);
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
            answers.RndShuffle();
        }
        parTf.DstChilds();
        rts[0].Clear();
        rts[1].Clear();
        lines.Clear();
        sels.Clear();
        List<List<int>> idxs = new List<List<int>>() { new List<int>(), new List<int>() };
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < ansCnt; idxs[i].Add(j++)) ;
            idxs[i].RndShuffle();
        }
        List<Ans> l = new List<Ans>();
        for (int i = 0; i < ansCnt; i++) {
            l.Add(answers[B.gameIdx * ansCnt + i]);
        }
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < ansCnt; j++) {
                int k = B.gameIdx * ansCnt + idxs[i][j];
                if (i == 0) answers[k].Rnd();
                RectTransform btn = Ins(i == 1 ? btnTxtPf : btnImgPf, parTf);
                btn.name = answers[k].name;
                btn.Tlp(V3.Xy(-j.X(ansCnt, btnSz.y, btnSpc.x), i.X(2, btnSz.x, btnSpc.y)));
                btn.sizeDelta = btnSz;
                var e = i == 1 ? answers[k].get.Item1 : answers[k].get.Item2;
                btn.Child(i == 1 ? "0" : "0 0").Img().sprite = e.Item2;
                btn.Child(1).Txt().text = e.Item1;
                btn.Child(0).Rt().sizeDelta = btnImgSz;
                rts[i].Add(btn);
            }
        CrtLine();
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        if (!isShow) {
            yield return Wf.Sec(1);
            foreach (var tfs in rts)
                foreach (var b in tfs)
                    StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
            foreach (var b in lines)
                StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
            yield return Wf.Sec(tm);
        } else {
            foreach (var tfs in rts)
                foreach (var b in tfs)
                    StaCor(SclCor(b.gameObject, V3.O, V3.I, tm, EaseTp.OutSine));
            yield return Wf.Sec(tm);
        }
    }
    void Update() {
        if (B.lvlIdx == 1) {
            if (IsMbD && !isAnim) {
                rt = Rt();
                if (rt && !sels.Contains(rt)) {
                    mp = RtP(rt);
                    UpdBtn(rt, SelTp.Clk);
                } else {
                    rt = null;
                }
            }
            if (IsMb && rt) {
                RectTransform rt2 = Rt();
                if (rt == rt2) {
                    lineRt.Hide();
                } else {
                    Vector3 a = mp, b = !rt2 || rt.Tp().y > sz2.y == rt2.Tp().y > sz2.y || sels.Contains(rt2) ? Mp : RtP(rt2);
                    lineRt.position = (a + b) / 2;
                    lineRt.right = b - a;
                    lineRt.sizeDelta = lineRt.sizeDelta.X(V3.Dis(a, b) * main.mpScl.y);
                    lineRt.Show();
                }
            }
            if (IsMbU && rt) {
                RectTransform rt2 = Rt();
                if (rt2 && rt != rt2 && rt.name == rt2.name) {
                    StaCor(CorrectCor(rt, rt2));
                } else if (rt2 && rt.Tp().y > sz2.y != rt2.Tp().y > sz2.y && !sels.Contains(rt2)) {
                    StaCor(WrongCor(rt, rt2));
                } else {
                    UpdBtn(rt, SelTp.None);
                    lineRt.Hide();
                }
                rt = null;
            }
        }
    }
    Vector3 RtP(RectTransform rt) { return rt.TfPnt(V3.Y(rt.sizeDelta.y / (rt.Tp().y > sz2.y ? -2 : 2))); }
    RectTransform Rt() {
        int i0 = rts[0].FindIndex(rt => rt.IsRt(Mp)), i1 = rts[1].FindIndex(rt => rt.IsRt(Mp));
        return i0 >= 0 ? rts[0][i0] : i1 >= 0 ? rts[1][i1] : null;
    }
    IEnumerator CorrectCor(RectTransform rt, RectTransform rt2) {
        isAnim = true;
        B.PlaySong(true);
        UpdBtn(rt2, SelTp.Clk);
        UpdLine(SelTp.Clk);
        lineRt.Show();
        yield return null;
        sels.Add(rt, rt2);
        CrtLine();
        if (sels.Count == ansCnt * 2)
            StaCor(main.WinCor());
        isAnim = false;
    }
    IEnumerator WrongCor(RectTransform rt, RectTransform rt2) {
        isAnim = true;
        B.PlaySong(false);
        UpdBtn(rt, SelTp.Wrong);
        UpdBtn(rt2, SelTp.Wrong);
        UpdLine(SelTp.Wrong);
        lineRt.Show();
        yield return Wf.Sec(1);
        UpdBtn(rt, SelTp.None);
        UpdBtn(rt2, SelTp.None);
        UpdLine(SelTp.None);
        lineRt.Hide();
        isAnim = false;
    }
    enum SelTp { None, Clk, Wrong }
    void UpdBtn(RectTransform rt, SelTp tp) { rt.Img().color = tp == SelTp.Clk ? C.limeGreen : tp == SelTp.Wrong ? C.lightCoral : C.I; }
    void UpdLine(SelTp tp) { lineRt.Img().color = tp == SelTp.Clk ? C.limeGreen : tp == SelTp.Wrong ? C.lightCoral : C.black; }
    void CrtLine() {
        lineRt = Ins(linePf, parTf);
        lineRt.Hide();
        lines.Add(lineRt);
    }
}