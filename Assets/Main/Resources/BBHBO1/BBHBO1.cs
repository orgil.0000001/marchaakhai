using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO1 : Singleton<BBHBO1> {
    public static string path => nameof(BBHBO1) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(BBHBO1), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(BBHBO1), value ? 1 : 0); }
    }
    [HideInInspector] public bool isWin = false;
    [HideInInspector] public Vector3 mpScl;
    List<GameMb> lvls = new List<GameMb>();
    GameMb lvl => lvls[B.lvlIdx];
    List<int> lvlCnts = Lis(3, 1, 1);
    List<Song> songs;
    void Awake() { General.Crt(tf, GameTp.BBHBO1, Replay, NextLevel, SelectLevel); }
    [Range(1, 3)] public int level;
    void Start() {
        if (General.saveLvlIdx >= 0) B.lvlIdx = General.saveLvlIdx;
        else if (Application.isEditor) B.lvlIdx = level - 1;
        Rect r = go.Rt().rect;
        mpScl = V3.V(r.width / Screen.width, r.height / Screen.height, 1);
        songs = Lis(new Song(path, "1_1"), new Song(path, "1_2"), new Song(path, "1_3"), new Song(path, "2"), new Song(path, "3"), new Song(path, "3t"));
        for (int i = 0; i < lvlCnts.Count; i++) {
            lvls.Add(go.Child<GameMb>(0, i));
            lvls[i].Data();
            lvls[i].Init();
            lvls[i].data.go.Tls(V3.O);
        }
        B.Idle();
        if (IsAuto) {
            lvl.go.Tls(V3.I);
            lvl.CrtData();
        } else B.LevelSelectShow();
    }
    public void Fill(int i) { B.LevelBarFill(i, lvlCnts[B.lvlIdx]); }
    public IEnumerator WinCor(float tm = 0.5f) {
        isWin = true;
        if (B.lvlIdx < 2) {
            B.Correct(true);
            Fill(B.gameIdx + 1);
            yield return Wf.Sec(2);
        } else {
            B.PlaySong(songs[5]);
            yield return Wf.Sec(songs[5].len);
        }
        if (B.gameIdx + 1 == lvlCnts[B.lvlIdx]) {
            yield return B.LevelHideCor(lvl.data);
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            B.TxtCol(lvl.data, tm, true);
            yield return lvl.WinCor(false, tm);
            B.gameIdx++;
            lvl.CrtData();
            B.TxtCol(lvl.data, tm, false);
            yield return lvl.WinCor(true, tm);
        }
        isWin = false;
    }
    public void PlaySong() { B.PlaySong(songs[B.lvlIdx == 0 ? B.gameIdx : B.lvlIdx == 1 ? 3 : 4]); }
    public void Replay() { StaCor(B.WinHideCor(lvl.data, lvl.CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx == lvlCnts.Count - 1) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, lvls[B.lvlIdx].CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, lvls[idx].CrtData, idx)); }
}