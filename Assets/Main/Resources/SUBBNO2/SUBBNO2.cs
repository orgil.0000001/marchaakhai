using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBBNO2 : Singleton<SUBBNO2> {
    public static string path => nameof(SUBBNO2) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBBNO2), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBBNO2), value ? 1 : 0); }
    }
    List<Lvl> lvls = new List<Lvl>();
    Lvl lvl => lvls[B.lvlIdx];
    Game game => lvl.games[B.gameIdx];
    IEnumerator talkCor;
    bool isWin = false;
    List<Song> songs;
    void Awake() { General.Crt(tf, GameTp.SUBBNO2, Replay, NextLevel, SelectLevel); }
    void Start() {
        Rect r = go.Rt().rect;
        songs = Lis(new Song(path, ""), new Song(path, "1"), new Song(path, "2"));
        List<int> cnts = Lis(3, 2, 1);
        for (int i = 0; i < cnts.Count; i++)
            lvls.Add(new Lvl(go.ChildGo(0, i), cnts[i], Song.txts[0][i < 2 ? 1 : 2].txt, i == 2));
        B.Idle();
        if (IsAuto) {
            lvl.data.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    void CrtData() {
        isWin = false;
        game.Init();
        if (B.gameIdx == 0) {
            Fill(0);
            B.LevelBar();
            game.Scl(true);
            game.tf.Tls(game.scl);
        }
        PlaySong();
    }
    void Update() {
        if (!isWin && game.Upd())
            StaCor(WinCor());
    }
    void Fill(int i) { B.LevelBarFill(i, B.lvlIdx == 0 ? 3 : B.lvlIdx == 1 ? 2 : 1); }
    IEnumerator WinCor(float tm = 0.5f) {
        isWin = true;
        Fill(B.gameIdx + 1);
        if (B.gameIdx + 1 == lvl.games.Count) {
            if (B.lvlIdx == 0) {
                B.PlaySong(songs[1]);
                yield return Wf.Sec(songs[1].len / B.src.pitch);
            } else yield return Wf.Sec(B.lvlIdx == 1 ? 5 : 3);
            yield return B.LevelHideCor(lvl.data);
            game.tf.Tls(V3.O);
            game.rts.ForEach(rt => rt.Par(game.piecesTf));
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            B.Good();
            yield return Wf.Sec(3);
            yield return SclCor(game.tf.gameObject, game.scl, V3.O, tm);
            game.rts.ForEach(rt => rt.Par(game.piecesTf));
            game.Scl(false);
            B.gameIdx++;
            CrtData();
            StaCor(SclCor(game.tf.gameObject, V3.O, game.scl, tm));
            B.SclTm(game.rts, tm, true);
            yield return Wf.Sec(tm);
        }
    }
    public void PlaySong() { B.PlaySong(songs[B.lvlIdx < 2 ? 0 : 2]); }
    public void Replay() { StaCor(B.WinHideCor(lvls[B.lvlIdx].data, CrtData)); }
    public void NextLevel() {
        if (lvl.isLast) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, CrtData, idx)); }
    class Lvl {
        public LvlData data;
        public List<Game> games = new List<Game>();
        public bool isLast;
        public Lvl(GameObject go, int cnt, string txt, bool isLast) {
            this.isLast = isLast;
            data = new LvlData(go);
            data.txt.text = txt;
            for (int i = 0; i < cnt; i++)
                games.Add(new Game(this, i));
            data.go.Tls(V3.O);
        }
    }
    class Game {
        public Transform tf, piecesTf;
        public List<RectTransform> rts = new List<RectTransform>();
        public Vector3 scl;
        List<Vector3> pnts = new List<Vector3>();
        RectTransform rt, dayRt, nightRt;
        Vector3 mp, rtP;
        int idx, cnt, sibIdx;
        bool isClk => idx >= 0;
        Lvl lvl;
        public Game(Lvl lvl, int idx) {
            this.lvl = lvl;
            tf = lvl.data.go.Child(1, 1 + idx);
            piecesTf = lvl.data.go.Child(2, 0, idx);
            if (lvl.isLast) {
                dayRt = tf.Child(0).Rt();
                nightRt = tf.Child(1).Rt();
            }
            for (int i = 0; i < piecesTf.childCount; i++) {
                rts.Add(piecesTf.Child(i).Rt());
                rts[i].name = "" + (i + 1);
                if (piecesTf.childCount == tf.childCount)
                    pnts.Add(tf.Child(i).Tp());
            }
            Scl(false);
            scl = tf.Tls();
            tf.Tls(V3.O);
        }
        public void Init() {
            idx = -1;
            cnt = 0;
            var l = rts.Copy();
            for (int i = 0; i < rts.Count; i++)
                rts[i].name = "" + (i + 1);
            rts.Shuffle();
            for (int i = 0; i < rts.Count; i++)
                rts[i].SibIdx(i);
            rts = l;
        }
        public void Scl(bool isBig) { rts.ForEach(rt => rt.Tls(isBig ? V3.I : V3.zpp)); }
        public bool Upd() {
            bool res = false;
            if (IsMbD) {
                idx = rts.FindIndex(rt => rt.IsRt(Mp));
                if (isClk && rts[idx].name != "") {
                    rt = rts[idx];
                    sibIdx = rt.SibIdx();
                    rtP = rt.Tp();
                    mp = Mp;
                    rt.Par(rt.root);
                } else {
                    idx = -1;
                }
            }
            if (IsMb && isClk) {
                rt.Tp(rtP + Mp - mp);
                rt.Tls(V3.Lerp(rt.Tls(), scl, Dt * 20));
            }
            if (IsMbU && isClk) {
                rt.Par(tf);
                bool isPut = false;
                if (lvl.isLast) {
                    int day = IsEllipse(dayRt, rt, true) && IsEllipse(nightRt, rt, true) ? 1 :
                        IsEllipse(dayRt, rt, true) ? 0 : IsEllipse(nightRt, rt, true) ? 2 : -1;//false
                    isPut = Lis(0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 2)[rt.name.I() - 1] == day;
                } else {
                    isPut = V3.Dis(pnts[idx], rt.Tp()) < 50 * rt.root.Tls().x;
                }
                B.PlaySong(isPut);
                if (isPut) {
                    if (!lvl.isLast)
                        rt.Tp(pnts[idx]);
                    rt.Tls(V3.I);
                    rt.name = "";
                    cnt++;
                    if (cnt == rts.Count)
                        res = true;
                } else {
                    rt.Par(piecesTf);
                    rt.SibIdx(sibIdx);
                    rt.Tp(rtP);
                    rt.Tls(V3.I);
                }
                idx = -1;
            }
            return res;
        }
        bool IsEllipse(RectTransform cirRt, RectTransform rt, bool isCen) {
            Vector2 c = cirRt.Tlp(), r = cirRt.sizeDelta / 2, p = rt.Tlp(), sz = rt.sizeDelta / 2;
            return isCen ? IsEllipse(c, r, p) : IsEllipse(c, r, p - sz.Y(0)) && IsEllipse(c, r, p + sz.Y(0)) && IsEllipse(c, r, p - sz.X(0)) && IsEllipse(c, r, p + sz.X(0));
        }
        bool IsEllipse(Vector2 c, Vector2 r, Vector2 p) { return (p.x - c.x) * (p.x - c.x) / (r.x * r.x) + (p.y - c.y) * (p.y - c.y) / (r.y * r.y) <= 1; }
    }
}
