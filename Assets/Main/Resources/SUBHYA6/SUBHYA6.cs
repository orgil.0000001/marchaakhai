using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBHYA6 : Singleton<SUBHYA6> {
    public static string path => nameof(SUBHYA6) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBHYA6), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBHYA6), value ? 1 : 0); }
    }
    public RectTransform btnPf, linePf;
    public Sprite defSpr, correctSpr, wrongSpr;
    List<Lvl> lvls = new List<Lvl>();
    Lvl lvl => lvls[B.lvlIdx];
    GameSelect game = new GameSelect();
    Vector3 mpScl;
    float sz2X;
    void Awake() { General.Crt(tf, GameTp.SUBHYA6, Replay, NextLevel, SelectLevel); }
    void Start() {
        Rect r = go.Rt().rect;
        sz2X = V3.Scl((go.Rt().sizeDelta / 2).V3(), Tls).x;
        mpScl = V3.V(r.width / Screen.width, r.height / Screen.height, 1);
        List<Ans> lvl1 = new List<Ans>(), lvl2 = new List<Ans>(), lvl3 = new List<Ans>();
        for (int i = 1; i <= 6; i++)
            lvl1.Add(new Ans("e" + i));
        for (int i = 1; i <= 6; i++)
            lvl2.Add(new Ans("i" + i));
        for (int i = 7; i <= 12; i++)
            lvl3.Add(new Ans("e" + i));
        Song eSong = new Song(path, "e"), iSong = new Song(path, "i");
        lvls = Lis(new Lvl(go.ChildGo(0, 0), lvl1, 2, 3, 150, 130, 10, Song.txts[0][1].txt, eSong),
            new Lvl(go.ChildGo(0, 1), lvl2, 2, 3, 150, 130, 10, Song.txts[0][2].txt, iSong),
            new Lvl(go.ChildGo(0, 2), lvl3, 2, 3, 150, 130, 10, Song.txts[0][1].txt, eSong));
        B.Idle();
        if (IsAuto) {
            lvl.data.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    void CrtData() {
        if (B.gameIdx == 0) {
            Fill(0);
            lvl.Init();
            B.LevelBar();
        }
        PlaySong();
        game.CrtData();
    }
    void Update() { game.Upd(); }
    void Fill(int i) { B.LevelBarFill(i, lvl.gameCnt); }
    IEnumerator WinCor(float tm = 0.5f) {
        Fill(B.gameIdx + 1);
        if (B.gameIdx + 1 == lvl.gameCnt) {
            yield return B.LevelHideCor(lvl.data);
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            yield return game.WinCor(false, tm);
            B.gameIdx++;
            CrtData();
            yield return game.WinCor(true, tm);
        }
    }
    public void PlaySong() { B.PlaySong(lvl.song); }
    public void Replay() { StaCor(B.WinHideCor(lvls[B.lvlIdx].data, CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx == 2) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, CrtData, idx)); }
    class GameSelect : Mb2 {
        RectTransform rt, lineRt;
        List<RectTransform> lines = new List<RectTransform>(), sels = new List<RectTransform>();
        List<List<RectTransform>> rts = new List<List<RectTransform>>() { new List<RectTransform>(), new List<RectTransform>() };
        Dictionary<RectTransform, Song> songDic = new Dictionary<RectTransform, Song>();
        bool isAnim = false;
        public GameSelect() { }
        public void CrtData() {
            _.lvl.parTf.DstChilds();
            rts[0].Clear();
            rts[1].Clear();
            lines.Clear();
            sels.Clear();
            List<List<int>> idxs = new List<List<int>>() { new List<int>(), new List<int>() };
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < _.lvl.cnt; idxs[i].Add(j++)) ;
                idxs[i].Shuffle();
            }
            songDic.Clear();
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < _.lvl.cnt; j++) {
                    Ans ans = _.lvl.ans[B.gameIdx * _.lvl.cnt + idxs[i][j]];
                    RectTransform btn = Ins(_.btnPf, _.lvl.parTf);
                    btn.name = ans.name;
                    btn.Tlp(V3.Xy(i.X(2, _.lvl.btnSz.x, 250), -j.X(_.lvl.cnt, _.lvl.btnSz.y, _.lvl.spc)));
                    btn.sizeDelta = _.lvl.btnSz;
                    btn.Child(0, 0).Img().sprite = i == 0 ? ans.spr : ans.spr2;
                    btn.Child(0).Rt().sizeDelta = _.lvl.imgSz;
                    rts[i].Add(btn);
                    songDic.Add(btn, i == 0 ? ans.song : ans.song2);
                }
            CrtLine();
        }
        public void Upd() {
            if (IsMbD && !isAnim) {
                rt = Rt();
                if (rt && !sels.Contains(rt)) {
                    mp = RtP(rt);
                    UpdBtn(rt, SelTp.Clk);
                } else {
                    rt = null;
                }
            }
            if (IsMb && rt) {
                RectTransform rt2 = Rt();
                if (rt == rt2) {
                    lineRt.Hide();
                } else {
                    // Vector2 p;
                    // RectTransformUtility.ScreenPointToLocalPointInRectangle(_.tf as RectTransform, Mp, Cam, out p);
                    // Vector3 a = mp, b = !rt2 || rt.Tp().x < 0 == rt2.Tp().x < 0 || sels.Contains(rt2) ? p.V3() : RtP(rt2);
                    Vector3 a = mp, b = !rt2 || rt.Tp().x < _.sz2X == rt2.Tp().x < _.sz2X || sels.Contains(rt2) ? Mp : RtP(rt2);
                    lineRt.position = (a + b) / 2;
                    lineRt.right = b - a;
                    lineRt.sizeDelta = lineRt.sizeDelta.X(V3.Dis(a, b) * _.mpScl.y);
                    lineRt.Show();
                }
            }
            if (IsMbU && rt) {
                RectTransform rt2 = Rt();
                if (rt2 && rt != rt2 && rt.name == rt2.name) {
                    _.StaCor(CorrectCor(rt, rt2));
                } else if (rt2 && rt.Tp().x < _.sz2X != rt2.Tp().x < _.sz2X && !sels.Contains(rt2)) {
                    _.StaCor(WrongCor(rt, rt2));
                } else {
                    UpdBtn(rt, SelTp.None);
                    lineRt.Hide();
                }
                rt = null;
            }
        }
        Vector3 RtP(RectTransform rt) { return rt.TfPnt(V3.X(rt.sizeDelta.x / (rt.Tp().x < _.sz2X ? 2 : -2))); }
        RectTransform Rt() {
            int i0 = rts[0].FindIndex(rt => rt.IsRt(Mp)), i1 = rts[1].FindIndex(rt => rt.IsRt(Mp));
            return i0 >= 0 ? rts[0][i0] : i1 >= 0 ? rts[1][i1] : null;
        }
        IEnumerator CorrectCor(RectTransform rt, RectTransform rt2) {
            isAnim = true;
            UpdBtn(rt2, SelTp.Clk);
            UpdLine(SelTp.Clk);
            lineRt.Show();
            B.PlaySong(true);
            yield return Wf.Sec(1);
            yield return TalkCor(rt, rt2);
            sels.Add(rt, rt2);
            CrtLine();
            if (sels.Count == _.lvl.cnt * 2)
                _.StaCor(_.WinCor());
            isAnim = false;
        }
        IEnumerator WrongCor(RectTransform rt, RectTransform rt2) {
            isAnim = true;
            UpdBtn(rt, SelTp.Wrong);
            UpdBtn(rt2, SelTp.Wrong);
            UpdLine(SelTp.Wrong);
            lineRt.Show();
            B.PlaySong(false);
            yield return Wf.Sec(1);
            yield return TalkCor(rt, rt2);
            UpdBtn(rt, SelTp.None);
            UpdBtn(rt2, SelTp.None);
            UpdLine(SelTp.None);
            lineRt.Hide();
            isAnim = false;
        }
        IEnumerator TalkCor(RectTransform rt, RectTransform rt2, float tm = 0.5f) {
            for (int i = 0; i < 2; i++) {
                RectTransform r = (rt.Tp().x < _.sz2X ? i == 0 : i == 1) ? rt : rt2;
                yield return SclCor(r.gameObject, V3.I, V3.V(1.1f), 0.1f, EaseTp.InSine);
                B.PlaySong(songDic[r]);
                yield return Wf.Sec(songDic[r].len / B.src.pitch);
                yield return SclCor(r.gameObject, V3.V(1.1f), V3.I, 0.1f, EaseTp.OutSine);
                yield return Wf.Sec(tm);
            }
        }
        enum SelTp { None, Clk, Wrong }
        void UpdBtn(RectTransform rt, SelTp tp) { rt.Img().sprite = tp == SelTp.Clk ? _.correctSpr : tp == SelTp.Wrong ? _.wrongSpr : _.defSpr; }
        void UpdLine(SelTp tp) { lineRt.Img().color = tp == SelTp.Clk ? C.limeGreen : tp == SelTp.Wrong ? C.lightCoral : C.black; }
        void CrtLine() {
            lineRt = Ins(_.linePf, _.lvl.parTf);
            lineRt.Hide();
            lines.Add(lineRt);
        }
        public IEnumerator WinCor(bool isShow, float tm) {
            if (!isShow) {
                yield return Wf.Sec(1);
                foreach (var tfs in rts)
                    foreach (var b in tfs)
                        _.StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
                foreach (var b in lines)
                    _.StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
                yield return Wf.Sec(tm);
            } else {
                foreach (var tfs in rts)
                    foreach (var b in tfs)
                        _.StaCor(SclCor(b.gameObject, V3.O, V3.I, tm, EaseTp.OutSine));
                yield return Wf.Sec(tm);
            }
        }
    }
    class Ans {
        public string name;
        public Sprite spr, spr2;
        public Song song, song2;
        public Ans(string s) {
            name = s;
            spr = O.LoadSpr(path + "z" + s + "a");
            spr2 = O.LoadSpr(path + "z" + s + "b");
            song = new Song(path, s + "a");
            song2 = new Song(path, s + "b");
        }
    }
    class Lvl {
        public List<Ans> ans = new List<Ans>();
        public int gameCnt, cnt;
        public Vector2 btnSz, imgSz;
        public float spc;
        public LvlData data;
        public Transform parTf;
        public Song song;
        public Lvl(GameObject go, List<Ans> ans, int gameCnt, int cnt, float btnW, float imgW, float spc, string txt, Song song) {
            this.ans = ans;
            this.gameCnt = gameCnt;
            this.cnt = cnt;
            btnSz = V2.V(btnW);
            imgSz = V2.V(imgW);
            this.spc = spc;
            data = new LvlData(go);
            parTf = data.go.Child(1);
            data.go.Tls(V3.O);
            data.txt.text = txt;
            this.song = song;
        }
        public void Init() { ans.Shuffle(); }
    }
}