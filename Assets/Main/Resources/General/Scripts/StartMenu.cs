using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using Orgil;

public class StartMenu : Mb {
#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")] static extern void HideLoader();
#endif
    public GameTp tp;
    public List<Sprite> btnSprs, btnSelSprs;
    public Color imgCol, imgSelCol;
    VideoPlayer signVp;
    AudioSource src;
    AudioClip f1Clip;
    Text lessonTxt, titleTxt, gradeTxt;
    Text startTxt;
    List<Button> btns = new List<Button>();
    Song song;
    IEnumerator blindCor;
    GameObject logoParGo;
    bool useSign = true;
    void OnEnable() {
#if UNITY_WEBGL && !UNITY_EDITOR
        HideLoader();
#endif
        GameObject menuGo = go.ChildGo(0, 0);
        Song.tp = Song.tp;
        Song.game = tp;
        General.Sign(ref signVp, go.ChildGo(0, 1), Song.data.isSignCirc);
        src = go.Gc<AudioSource>();
        f1Clip = O.LoadClip("General/Data/start");
        logoParGo = menuGo.ChildGo(0, 0);
        titleTxt = menuGo.Child(0, 1).Txt();
        lessonTxt = menuGo.Child(0, 2).Txt();
        gradeTxt = menuGo.Child(0, 3).Txt();
        menuGo.Child(0, 4).Btn().onClick.AddListener(Sound);
        startTxt = menuGo.Child(2, 0).Txt();
        startTxt.Par().Btn().onClick.AddListener(Video);
        useSign = tp != GameTp.BBHBO3;
        List<int> btnXs = useSign ? Lis(-308, -162, -8, 143, 300) : Lis(-228, -82, 72, 10000, 236);
        for (int i = 0; i < 5; i++) {
            int tp = i;
            btns.Add(menuGo.Child(1, i).Btn());
            btns[i].onClick.AddListener(() => UpdBtns(tp));
            btns[i].TlpX(btnXs[i]);
        }
        UpdBtns();
    }
    void Video() { General.Video(); }
    void Update() {
        Dev.Check();
        if (General.Check(KeyCode.F1)) {
            if (blindCor.Null()) {
                src.clip = f1Clip;
                src.Play();
            }
        } else if (General.Check(KeyCode.Alpha1)) UpdBtns(0);
        else if (General.Check(KeyCode.Alpha2)) UpdBtns(1);
        else if (General.Check(KeyCode.Alpha3)) UpdBtns(2);
        else if (General.Check(KeyCode.Alpha4)) UpdBtns(useSign ? 3 : 4);
        else if (General.Check(KeyCode.Alpha5) && useSign) UpdBtns(4);
        else if (General.Check(KeyCode.Space)) {
            if (blindCor.NotNull()) StopCor(blindCor);
            StaCor(blindCor = BlindCor());
        } else if (General.Check(KeyCode.Return) || General.Check(KeyCode.BackQuote)) Video();
        else if (General.Check(KeyCode.Tab)) General.Game();
    }
    IEnumerator BlindCor() {
        Song.tp = 4;
        UpdBtns();
        yield return Wf.Sec(song.len);
        Video();
    }
    void UpdBtns(int i) {
        Song.tp = i;
        UpdBtns();
    }
    void UpdBtns() {
        song = new Song(tp + "/Data/", "_");
        for (int i = 0; i < 3; i++)
            logoParGo.ChildAct(i == Song.tp2, i);
        for (int i = 0; i < btns.Count; i++) {
            btns[i].Img().sprite = i == Song.tp ? btnSelSprs[i] : btnSprs[i];
            btns[i].Child(0).Txt().color = i == Song.tp ? imgSelCol : imgCol;
            if (i >= 3) btns[i].Child(1).Img().color = i == Song.tp ? imgSelCol : imgCol;
        }
        lessonTxt.text = Song.data.lesson.txt;
        titleTxt.text = Song.data.title;
        gradeTxt.text = Song.data.grade;
        startTxt.text = Name.ehleh.txt;
        Sound();
    }
    void Sound() {
        signVp.Act(Song.IsSign);
        if (Song.IsSign) {
            Vid(signVp, false, song.sign + ".mp4");
            signVp.loopPointReached += a => signVp.Hide();
        }
        src.clip = song.clip;
        src.Play();
    }
    // void VidCrop(ref string s, string name, int x = 420, int y = 0, int w = 1080, int h = 1080) {
    void VidCrop(ref string s, string name, int x = 30, int y = 0, int w = 240, int h = 300) {
        s += "ffmpeg -i sign/" + name + ".mp4 -filter:v \"crop=" +
            w + ":" + h + ":" + x + ":" + y + "\" sign2/" + name + ".mp4\n";
    }
    void VidResize(ref string s, string name, int w = 300, int h = 300) {
        s += "ffmpeg -i sign2/" + name + ".mp4 -s " + w + "x" + h + " -c:a copy sign3/" + name + ".mp4\n";
    }
    [ContextMenu("Video")]
    void PrintVidCrop() {
        string s = "";
        // var l = Lis("d1");
        // for (int i = 0; i < l.Count; i++)
        //     VidCrop(ref s, l[i]);
        // for (int i = 0; i < l.Count; i++)
        //     VidResize(ref s, l[i], 534, 300);
        // for (GameTp tp = GameTp.SUBBNO2; tp <= GameTp.BBHBO10; tp++) {
        //     string path = Application.dataPath + "/Main/Resources/StreamingAssets/" + tp + "/";
        //     if (System.IO.Directory.Exists(path)) {
        //         s += tp + "\n";
        //         string[] names = System.IO.Directory.GetFiles(path);
        //         foreach (string fName in names) {
        //             if (fName.IsE(".mp4")) {
        //                 string name = fName.Split('/').Last().Replace(".mp4", "");
        //                 s += "{\"" + name + "\", " + O.Load<VideoClip>("StreamingAssets/" + tp + "/" + name).length + "f}, ";
        //             }
        //         }
        //         s += "\n";
        //     }
        // }
        print(s);
    }
}
