using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Orgil;

public class EndMenu : Singleton<EndMenu> {
    VideoPlayer signVp;
    AudioSource src;
    GameObject bgGo, soundGo, homeGo, parGo;
    void Start() {
        bgGo = go.ChildGo(0);
        soundGo = bgGo.ChildNameGo("Sound");
        soundGo.Btn().onClick.AddListener(PlaySong);
        homeGo = bgGo.ChildNameGo("Home");
        homeGo.Btn().onClick.AddListener(General.Home);
        homeGo.Child(0).Txt().text = Name.nuurHuudas.txt;
        General.Sign(ref signVp, bgGo.ChildNameGo("Sign"), Song.data.isSignCirc);
        src = go.Gc<AudioSource>();
        PlaySong();
        if ((parGo = bgGo.ChildNameGo("TxtMv")).ActS()) StaCor(TxtMvCor());
        else if ((parGo = bgGo.ChildNameGo("TxtShow")).ActS()) StaCor(TxtShowCor());
        else if ((parGo = bgGo.ChildNameGo("Balloon")).ActS()) BalloonInit();
    }
    void Update() {
        Dev.Check();
        if (General.Check(KeyCode.Space) || General.Check(KeyCode.Return) || General.Check(KeyCode.Escape)) General.Home();
        else if (General.Check(KeyCode.BackQuote)) General.Video();
        else if (General.Check(KeyCode.Tab)) General.Game();
        BalloonUpd();
    }
    public void PlaySong() {
        src.clip = new Song("General/Data", "End").clip;
        src.Play();
        if (Song.IsSign) {
            signVp.Show();
            Vid(signVp, false, (Song.data.isSignCirc ? "sign" : "sign2") + "/dEnd.mp4");
            signVp.loopPointReached += a => signVp.Hide();
        }
    }
    IEnumerator TxtMvCor(float len = 800) {
        GameObject planeGo = parGo.ChildGo(3);
        RectTransform rt = parGo.Child(Song.tp2).Rt();
        parGo.ChildHide(4);
        for (int i = 0; i < 3; i++)
            parGo.ChildAct(i == Song.tp2, i);
        float x = 1400;
        planeGo.TlpX(rt.sizeDelta.x * rt.Tls().x / 2);
        homeGo.Tls(V3.O);
        yield return Cor(t => parGo.TlpX(M.Lerp(-x, 0, t)), x / len);
        parGo.ChildShow(4);
        float x2 = planeGo.Tlp().x;
        StaCor(Cor(t => planeGo.TlpX(M.Lerp(x2, x, t)), (x - x2) / len));
        yield return Wf.Sec(0.1f);
        StaCor(SclCor(homeGo, V3.O, V3.I, 0.2f, EaseTp.OutSine));
    }
    IEnumerator TxtShowCor(float len = 800) {
        GameObject planeGo = parGo.ChildGo(3);
        RectTransform rt = parGo.Child(Song.tp2).Rt();
        Image img = rt.Img();
        parGo.ChildHide(4);
        for (int i = 0; i < 3; i++)
            parGo.ChildAct(i == Song.tp2, i);
        homeGo.Tls(V3.O);
        bool isScl = false;
        for (float x = -1000; x <= 1000; x += Dt * len) {
            planeGo.TlpX(x);
            img.fillAmount = M.C01((x + rt.sizeDelta.x * rt.Tls().x / 2) / (rt.sizeDelta.x * rt.Tls().x));
            if (img.fillAmount > 0.5f && !isScl) {
                isScl = true;
                parGo.ChildShow(4);
                StaCor(SclCor(homeGo, V3.O, V3.I, 0.2f, EaseTp.OutSine));
            }
            yield return null;
        }
    }
    AudioSource txtSrc;
    GameObject effectPf;
    float txtY;
    List<List<GameObject>> balloonGos = new List<List<GameObject>> { new List<GameObject>(), new List<GameObject>() };
    float balloonRotSpc = 30;
    AudioClip clickClip;
    Vector3 mpScl, sz2;
    IEnumerator clkCor;
    void BalloonInit() {
        Rect r = go.Rt().rect;
        mpScl = V3.V(r.width / Screen.width, r.height / Screen.height, 1);
        sz2 = V3.Xy(Screen.width / 2, Screen.height / 2);
        txtSrc = parGo.Gc<AudioSource>();
        txtY = parGo.Tlp().y;
        for (int i = 0; i < 3; i++)
            parGo.ChildAct(i == Song.tp2, 2 + i);
        for (int i = 0; i < 6; i++) {
            int j = i / 3;
            Button btn = parGo.Child(j, i % 3, 0).Ac<Button>();
            btn.transition = Selectable.Transition.None;
            btn.onClick.AddListener(() => BalloonClk(j, btn.ParGo()));
            balloonGos[j].Add(btn.ParGo());
        }
        clickClip = O.LoadClip("General/Data/click");
        effectPf = O.LoadGo("General/Effect/Effect");
        StaCor(BalloonCor());
    }
    IEnumerator BalloonCor() {
        float homeY = homeGo.Tlp().y;
        homeGo.TlpY(-450);
        StaCor(SclCor(soundGo, V3.O, V3.I, 0.5f, EaseTp.OutSine));
        clkCor = Cor(t => parGo.TlpY(M.Lerp(-500, txtY, t)), 2, EaseTp.OutCubic);
        StaCor(clkCor);
        yield return Wf.Sec(1);
        yield return Cor(t => homeGo.TlpY(M.Lerp(-450, homeY, t)), 1, EaseTp.OutCubic);
    }
    void BalloonUpd() {
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < balloonGos[i].Count; j++)
                balloonGos[i][j].Tlr(Q.Lerp(balloonGos[i][j].Tlr(), Q.Z(-j.X(balloonGos[i].Count, 0, 30)), Dt * 10));
    }
    void BalloonClk(int i, GameObject bGo) {
        StopCor(clkCor);
        StaCor(clkCor = BalloonClkCor(i, bGo));
    }
    IEnumerator BalloonClkCor(int i, GameObject bGo, float dy = 20, float tm = 0.25f) {
        Dst(Ins(effectPf, V3.Scl(Mp, mpScl).Z(-100) - sz2, Q.O, tf), 1);
        txtSrc.clip = clickClip;
        txtSrc.Play();
        balloonGos[i].Rmv(bGo);
        Dst(bGo);
        float y = parGo.Tlp().y;
        yield return Cor(t => parGo.TlpY(M.Lerp(y, y - dy, t)), tm, EaseTp.OutSine);
        yield return Cor(t => parGo.TlpY(M.Lerp(y - dy, txtY, t)), (txtY - y + dy) / dy * tm, EaseTp.InSine);
        if (balloonGos[0].IsEmpty() && balloonGos[1].IsEmpty()) {
            parGo.ChildShow(0, 0);
            parGo.ChildShow(1, 0);
        }
    }
}
