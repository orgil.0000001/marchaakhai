using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameMb : Mb {
    public LvlData data;
    public void Data() { data = new LvlData(go); }
    public abstract void Init();
    public abstract void CrtData();
    public abstract IEnumerator WinCor(bool isShow, float tm);
}
