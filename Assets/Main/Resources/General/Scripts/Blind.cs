using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class Blind : Mb {
    AudioSource src;
    Text qusTxt;
    List<Button> btns = new List<Button>();
    List<int> idxs;
    int corIdx, qusIdx = 0;
    bool isSub;
    List<Qus> questions = new List<Qus>();
    Qus qus => questions[qusIdx];
    IEnumerator talkCor;
    List<AudioClip> qusClips = new List<AudioClip>(), ansClips = new List<AudioClip>();
    AudioClip correctClip, wrongClip, f1Clip;
    List<bool> corrects = new List<bool>();
    public GameTp tp;
    void Start() {
        if (Application.isEditor)
            Song.game = tp;
        for (int i = 1; i <= 6; i++)
            qusClips.Add(O.LoadClip("General/Data/qus" + i));
        for (int i = 1; i <= 4; i++)
            ansClips.Add(O.LoadClip("General/Data/" + i));
        correctClip = O.LoadClip("General/Bido/mCorrect");
        wrongClip = O.LoadClip("General/Bido/mWrong");
        f1Clip = O.LoadClip("General/Data/game");
        src = go.Gc<AudioSource>();
        isSub = Song.game.tS().IsS("SUB");
        go.Child(0).Img().sprite = O.LoadSpr(Song.game + "/Data/bg");
        List<QusData> data = GameData(Song.game);
        for (int i = 0; i < data.Count; i++) {
            questions.Add(new Qus(Song.game, i, data[i]));
            corrects.Add(false);
        }
        qusTxt = go.Child(0, 0, 0, 0).Txt();
        go.Child(0, 0, 0, 1).Btn().onClick.AddListener(PlaySong);
        Button pf = go.Child(0, 0, 1, 0).Btn();
        for (int i = 0; i < 4; i++) {
            int j = i;
            Button btn = j == 0 ? pf : Ins(pf, pf.Par());
            btn.onClick.AddListener(() => BtnClk(j));
            btns.Add(btn);
        }
        go.ChildGo(0, 0, 2, 0).Btn().onClick.AddListener(() => CrtQus(-1));
        go.Child(0, 0, 2, 1).Btn().onClick.AddListener(() => CrtQus(1));
        go.Child(0, 1).Btn().onClick.AddListener(General.Home);
        CrtQus();
    }
    void Update() {
        if (General.Check(KeyCode.F1)) {
            if (talkCor.NotNull())
                StopCor(talkCor);
            src.clip = f1Clip;
            src.Play();
        } else if (General.Check(KeyCode.Escape)) General.Home();
        else if (General.Check(KeyCode.BackQuote)) General.Video();
        else if (General.Check(KeyCode.Tab)) General.Game();
        else if (General.Check(KeyCode.Return)) General.Celebrate();
        else if (General.Check(KeyCode.Space)) PlaySong();
        else if (General.Check(KeyCode.LeftArrow)) CrtQus(-1);
        else if (General.Check(KeyCode.RightArrow)) CrtQus(1);
        else if (isSub) {
            if (General.Check(KeyCode.Comma)) BtnClk(0);
            else if (General.Check(KeyCode.Period)) BtnClk(1);
            else if (General.Check(KeyCode.Slash)) BtnClk(2);
        } else {
            if (General.Check(KeyCode.M)) BtnClk(0);
            else if (General.Check(KeyCode.Comma)) BtnClk(1);
            else if (General.Check(KeyCode.Period)) BtnClk(2);
            else if (General.Check(KeyCode.Slash)) BtnClk(3);
        }
    }
    void CrtQus(int d = 0) {
        int i = 0;
        bool isWin = false;
        if (d != 0) while (i < corrects.Count && corrects.RepIdx(qusIdx + ++i * d)) ;
        if (i < corrects.Count) qusIdx = M.RepIdx(qusIdx + i * d, corrects.Count);
        else if (!corrects.Contains(false)) isWin = true;
        if (isWin) {
            General.Celebrate();
        } else {
            qusTxt.text = "Дасгал " + (qusIdx + 1) + ": " + qus.data.qus;
            idxs = Lis(-1);
            corIdx = Rnd.Idx(qus.corrects.Count);
            List<int> l = new List<int>();
            for (i = 0; i < qus.wrongs.Count; i++) l.Add(i);
            l.RndShuffle();
            int wrongCnt = isSub ? 2 : qus.wrongs.Count;
            for (i = 0; i < wrongCnt; i++) idxs.Add(l[i]);
            idxs.RndShuffle();
            for (i = 0; i < btns.Count; i++) {
                btns[i].Act(i < idxs.Count);
                if (btns[i].ActS())
                    btns[i].Child(0).Txt().text = "Хариулт " + (i + 1) + ": " + (idxs[i] < 0 ? qus.data.corrects[corIdx] : qus.data.wrongs[idxs[i]]);
                btns[i].image.color = C.I;
            }
            PlaySong();
        }
    }
    void BtnClk(int i) {
        if (!corrects[qusIdx]) {
            btns.ForEach(btn => btn.image.color = C.I);
            StopCor(talkCor);
            StaCor(talkCor = BtnCor(i));
        }
    }
    IEnumerator BtnCor(int i) {
        bool isCorrect = idxs[i] < 0;
        if (isCorrect) corrects[qusIdx] = true;
        btns[i].image.color = isCorrect ? C.lime : C.lightCoral;
        yield return AnsSongCor(true, i);
        yield return SongCor(isCorrect ? correctClip : wrongClip, 0.1f);
        if (isCorrect) {
            if (Song.game == GameTp.SUBHEM3 && qusIdx == 1)
                yield return SongCor(O.LoadClip("SUBHEM3/Data/m2t"), 0);
            CrtQus(1);
        } else btns[i].image.color = C.I;
    }
    IEnumerator AnsSongCor(bool isAns, int i, AudioClip clip = null) {
        yield return SongCor(isAns ? ansClips[i] : qusClips[i], 0.3f);
        yield return SongCor(isAns ? idxs[i] >= 0 ? qus.wrongs[idxs[i]] : qus.corrects[corIdx] : clip, 0.1f);
    }
    IEnumerator SongCor(AudioClip clip, float tm) {
        src.clip = clip;
        src.Play();
        yield return Wf.Sec(clip ? clip.length : 0.1f);
        yield return Wf.Sec(tm);
    }
    IEnumerator TalkCor() {
        yield return AnsSongCor(false, qusIdx, qus.qus);
        for (int i = 0; i < idxs.Count; i++)
            yield return AnsSongCor(true, i);
    }
    public void PlaySong() {
        if (talkCor.NotNull())
            StopCor(talkCor);
        StaCor(talkCor = TalkCor());
    }
    class Qus {
        public QusData data;
        public AudioClip qus;
        public List<AudioClip> corrects = new List<AudioClip>(), wrongs = new List<AudioClip>();
        public Qus(GameTp tp, int idx, QusData data) {
            this.data = data;
            string path;
            if (tp == GameTp.SUBBNO5) {
                path = tp + "/Data/" + (idx < 3 ? "m" : "h") + (idx + 1);
            } else if (tp == GameTp.SUBHEM3) {
                path = tp + "/Data/m" + Lis(1, 2, 3, 4, 6, 7)[idx];
            } else if (tp == GameTp.SUBHEM6) {
                path = tp + "/Data/m" + Lis(1, 2, 3, 5, 8, 9)[idx];
            } else {
                path = tp + "/Data/h" + (idx + 1);
            }
            qus = O.LoadClip(path);
            for (int i = 1; i <= data.corrects.Count; i++) {
                AudioClip clip = O.LoadClip(path + "z" + i);
                if (!clip) print(path + "z" + i);
                corrects.Add(clip);
            }
            for (int i = 1; i <= data.wrongs.Count; i++) {
                AudioClip clip = O.LoadClip(path + "b" + i);
                if (!clip) print(path + "b" + i);
                wrongs.Add(clip);
            }
        }
    }
    class QusData {
        public string qus;
        public List<string> corrects, wrongs;
        public QusData(string qus, List<string> corrects, List<string> wrongs) {
            this.qus = qus;
            this.corrects = corrects;
            this.wrongs = wrongs;
        }
    }
    List<QusData> GameData(GameTp tp) {
        switch (tp) {
            case GameTp.SUBBNO2:
                return new List<QusData> {
                    new QusData("Өдөр хэзээ эхэлдэг вэ?",
                        Lis("Нар мандаж өглөө болоход"),
                        Lis("Нар жаргаж орой болоход", "Нар эгц дээрээс тусахад")
                    ), new QusData("Аль нь дэлхийн зөв байрлал вэ?",
                        Lis("Дэлхий нар, сарны дунд байрладаг"),
                        Lis("Дэлхий нарны дээр байрладаг", "Дэлхий сарны доор байрладаг")
                    ), new QusData("Дэлхий тэнхлэгээ тойрон нэг удаа бүтэн эргэхэд хэдэн цаг зарцуулах вэ?",
                        Lis("Нэг хоног буюу 24 цаг"),
                        Lis("Нэг жил буюу 365 хоног", "Хагас өдөр буюу 12 цаг")
                    ), new QusData("Дэлхийн сүүдэр талд хоногийн аль цаг үргэлжилдэг вэ?",
                        Lis("Шөнө"),
                        Lis("Өдөр", "Өглөө")
                    ), new QusData("Дэлхийн нар луу харсан талд хоногийн аль цаг үргэлжилдэг вэ?",
                        Lis("Өдөр"),
                        Lis("Шөнө", "Орой")
                    ),
                };
            case GameTp.SUBBNO5:
                return new List<QusData> {
                    new QusData("Сургуулийн гадна орчинд ямар ямар тэмдэг, тэмдэглэгээ байдаг вэ?",
                        Lis("Гарцын тэмдэг", "Анхаар, хүүхэд тэмдэг", "Сургууль орчмын бүс тэмдэг", "Автомашины зогсоол тэмдэг", "Орох тэмдэглэгээ"),
                        Lis("Хориглох тэмдэг", "Анхааруулах тэмдэг", "Төмөр замын гармын тэмдэг")
                    ), new QusData("Сургууль дотор ямар, ямар өрөө байдаг вэ?",
                        Lis("Номын сан", "Ариун цэврийн өрөө", "Өлгүүрийн хэсэг", "Эмчийн өрөө", "Урлаг, спорт заал", "Хоолны цайны газар"),
                        Lis("Цонх", "Аяга", "Самбар", "Шохой", "Алчуур")
                    ), new QusData("Сургуульд ямар ажил, мэргэжилтэй хүмүүс ажилладаг вэ?",
                        Lis("Захирал", "Сургалтын менежер", "Багш нар", "Эмч", "Тогооч", "Үйлчлэгч", "Манаач"),
                        Lis("Цагдаа", "Сэтгүүлч", "Малчин", "Барилгачин", "Гоо сайханч")
                    ), new QusData("Сургуулийн орчинд бусадтай хэрхэн харилцах вэ?",
                        Lis("Бусдыг хүндэтгэж харилцана"),
                        Lis("Бусадтай хүндэтгэлгүй харилцана", "Бусадтай харилцахгүй")
                    ),
                };
            case GameTp.SUBBNO6:
                return new List<QusData> {
                    new QusData("Гадаах ургамал, моддыг юу усалдаг вэ?",
                        Lis("Цас ба бороо"),
                        Lis("Нар ба үүл", "Салхи ба шуурга")
                    ), new QusData("Борооны дусал юунаас үүсдэг вэ?",
                        Lis("Ус чийгнээс"),
                        Lis("Өвс ургамлаас", "Хоол хүнснээс")
                    ), new QusData("Цаг агаар ямар үед цас ордог вэ?",
                        Lis("Хүйтэн"),
                        Lis("Халуун", "Дулаан")
                    ), new QusData("Цасан ширхэг хэрхэн үүсдэг вэ?",
                        Lis("Жижиг мөсөн талстууд нийлж"),
                        Lis("Өвс, ургамлууд нийлж", "Тоос, шороо нийлж")
                    ),
                };
            case GameTp.SUBHEM3:
                return new List<QusData> {
                    new QusData("Голын усанд эд зүйлээ унагавал яах вэ?",
                        Lis("Томчуудаас тусламж хүснэ", "Ээж аавдаа хэлнэ", "Аврагч дуудна"),
                        Lis("Найзыгаа дуудна", "Өөрөө авна")
                    ), new QusData("Тогтоол усанд тоглож болох уу?",
                        Lis("Болохгүй гэж найздаа сануулна", "Болохгүй"),
                        Lis("Болно", "Хамтдаа тоглоно", "Усны гутлаа өмсөөд тоглож болно")
                    ), new QusData("Үер болсон үед аль дугаар луу залгах вэ?",
                        Lis("105"),
                        Lis("101", "103", "108")
                    ), new QusData("Аянга цахилгаантай бороо орж байхад хаана байх нь зөв бэ?",
                        Lis("Гэртээ байна", "Сургууль дотроо байна", "Цэцэрлэг дотроо байна"),
                        Lis("Гэртээ харина", "Тоглоомын талбайд тоглоно", "Гадаа гарна")
                    ), new QusData("Үер болбол чи юу хийх вэ?",
                        Lis("Өндөрлөг газар гарах", "Замаар явах", "Уулан дээр гарах"),
                        Lis("Нам дор газар байх", "Байшингийн хажууд зогсох", "Газар доорх нүхэн гарцаар гарах")
                    ), new QusData("Үер усны аюулаас гэр бүлээрээ хэрхэн сэргийлэх вэ?",
                        Lis("Цаг агаарын сэрэмжлүүлэг мэдээ, дохиог тогтмол хүлээн авч, бусдад дамжуулах", "Хараа хяналтгүйгээр гол мөрөн, нуур цөөрмийн орчимд тоглохгүй, эрэг дээр ганцаараа үлдэхгүй байх", "Үерийн аюултай бүс нутгаас нүүх"),
                        Lis("Голын эрэг дээр тоглох", "Ширүүн, аадар бороотой үед гадагш гарах", "Үер болдог газарт амьдрах")
                    ),
                };
            case GameTp.SUBHEM6:
                return new List<QusData> {
                    new QusData("Гал гаргах хэрэгсэл аль нь вэ?",
                        Lis("Чүдэнз", "Асаагуур"),
                        Lis("Гар чийдэн", "Цаас", "Чулуу", "Мод")
                    ), new QusData("Гал унтраахад аль нь хэрэгтэй вэ?",
                        Lis("Галын хор", "Элс шороо"),
                        Lis("Чүдэнз", "Тос", "Мод", "Төмөр", "Цаас")
                    ), new QusData("Гал гарвал аль дугаарт залгах вэ?",
                        Lis("101"),
                        Lis("103", "102", "108", "105")
                    ), new QusData("Гал түймэр гарсан үед аль үйлдлийг хийж болохгүй вэ?",
                        Lis("Нуугдах", "Цонх онгойлгох"),
                        Lis("Утаатай орчинд доогуур явах", "101 дугаар луу залгаж дуудлага өгөх", "Галыг бүтээлгээр бүтээх", "Галын хор ашиглах", "Том хүнээс тусламж хүсэх", "Ус цацах")
                    ), new QusData("Гал гарсан үед ямар дүрэм баримтлах вэ?",
                        Lis("Гүйхгүй, түлхэхгүй, ярихгүй, эргэж буцахгүй"),
                        Lis("Гүйх", "Бие биеэ түлхэх", "Уйлаад зогсох", "Нуугдах", "Ус цацах")
                    ), new QusData("Хэрвээ хувцас чинь галд өртвөл ямар дүрэм баримтлах вэ?",
                        Lis("Зогс, хэвт, өнхөр"),
                        Lis("Сандарч гүйх", "Суух", "Хувцсаа тайлахыг оролдох", "Дороо үсрэх")
                    ),
                };
            case GameTp.SUBHYA2:
                return new List<QusData> {
                    new QusData("Улаан, ногоон, сонгино гэсэн үгсийн аль нь өөр утгатай вэ?",
                        Lis("Сонгино"),
                        Lis("Улаан", "Ногоон")
                    ), new QusData("Явах, цагаан, сэлэх гэсэн үгсийн аль нь өөр утгатай вэ?",
                        Lis("Цагаан"),
                        Lis("Явах", "Сэлэх")
                    ), new QusData("Идэх, зайрмаг, чихэр гэсэн үгсийн аль нь өөр утгатай вэ?",
                        Lis("Идэх"),
                        Lis("Зайрмаг", "Чихэр")
                    ), new QusData("Улбар шар, ном, ягаан гэсэн үгсийн аль нь өөр утгатай вэ?",
                        Lis("Ном"),
                        Lis("Улбар шар", "Ягаан")
                    ), new QusData("Төмс, лууван, гүйх гэсэн үгсийн аль нь өөр утгатай вэ?",
                        Lis("Гүйх"),
                        Lis("Төмс", "Лууван")
                    ),
                };
            case GameTp.SUBHYA3:
                return new List<QusData> {
                    new QusData("\"Мидо .......... олов\" гэсэн өгүүлбэрийг гүйцээнэ үү?",
                        Lis("Өрөө"),
                        Lis("Машин", "Бөмбөг")
                    ), new QusData("Кидо .......... идэв гэсэн өгүүлбэрийг гүйцээнэ үү?",
                        Lis("Улаан алим"),
                        Lis("Хар шороо", "Цагаан цаас")
                    ), new QusData("\"Хүүхдүүд шар ном уншив\" гэсэн өгүүлбэрийг хоёр үгтэй өгүүлбэр болгон зөв бичсэнийг олоорой.",
                        Lis("Хүүхдүүд уншив."),
                        Lis("Хүүхдүүд шар", "Хүүхдүүд ном")
                    ), new QusData("Цахим аялагчид онцгой өрөөний хаалганы түлхүүрийг хэрхэн олсон бэ?",
                        Lis("Зургийг ашиглан өгүүлбэр зохиосон"),
                        Lis("Хаалганы хажуугаас түлхүүрийг олсон", "Нууц үгээр хаалгыг онгойлгосон")
                    ),
                };
            case GameTp.SUBHYA4:
                return new List<QusData> {
                    new QusData("Томоо, Жижгээ хоёрт эмээ нь авдраасаа юу гаргаж өгсөн бэ?",
                        Lis("Зургийн цомог, чихэр"),
                        Lis("Дээл, малгай", "Ном, дэвтэр")
                    ), new QusData("Томоо, Жижгээ хоёрын зургийн цомгийн хамгийн сүүлд ямар зураг байсан бэ?",
                        Lis("Нохойтой болсон зураг"),
                        Lis("Цэцэрлэгт орсон зураг", "Элсэн дээр тоглож буй зураг")
                    ), new QusData("Эмээ нь Томоо, Жижгээ хоёрыг юу гэж магтсан бэ?",
                        Lis("Өвөө эмээдээ тусалдаг сайн хүүхдүүд шүү."),
                        Lis("Та хоёр онц сурдаг байсан даа.", "Бусдыг дээрэлхдэггүй сайн хүүхдүүд.")
                    ),
                };
            case GameTp.SUBHYA6:
                return new List<QusData> {
                    new QusData("Могой урт бол могойн зулзага ямар байсан бэ?",
                        Lis("Богино"),
                        Lis("Урт", "Бөөрөнхий")
                    ), new QusData("Тэмээ намхан харин анааш ямар байсан бэ?",
                        Lis("Өндөр"),
                        Lis("Удаан", "Тайван")
                    ), new QusData("Заан ямар байсан бэ?",
                        Lis("Тарган бүдүүн"),
                        Lis("Намхан жижиг", "Урт өндөр")
                    ), new QusData("Жижгээ жижиг амтай байсан бол матар ямар амтай байсан бэ?",
                        Lis("Том"),
                        Lis("Жижиг", "Өндөр")
                    ), new QusData("Туулай ямар нүдтэй байсан бэ?",
                        Lis("Дүрлэгэр бүлтгэр"),
                        Lis("Өндөр урт", "Намхан богино")
                    ),
                };
            case GameTp.SUBHYA8:
                return new List<QusData> {
                    new QusData("Тэмдэг тэмдэглэгээг хэд ангилдаг вэ?",
                        Lis("Дөрөв"),
                        Lis("Гурав", "Тав")
                    ), new QusData("Дугуй хүрээтэй, улаан өнгөтэй тэмдэглэгээ юуны тэмдэглэгээ вэ?",
                        Lis("Хориглох"),
                        Lis("Заах", "Анхааруулах")
                    ), new QusData("Гурвалжин хүрээтэй, улаан өнгөтэй тэмдэглэгээ юуны тэмдэглэгээ вэ?",
                        Lis("Анхааруулах"),
                        Lis("Заах", "Хориглох")
                    ), new QusData("Дөрвөлжин хүрээтэй, цэнхэр өнгөтэй тэмдэглэгээ юуны тэмдэглэгээ вэ?",
                        Lis("Мэдээлэх"),
                        Lis("Анхааруулах", "Заах")
                    ), new QusData("Дугуй хүрээтэй, цэнхэр өнгөтэй тэмдэглэгээ юуны тэмдэглэгээ вэ?",
                        Lis("Заах"),
                        Lis("Мэдээлэх", "Анхааруулах")
                    ),
                };
            case GameTp.BBHBO1:
                return new List<QusData> {
                    new QusData("Өдрийн дэглэмийн хуваарь гэж юу вэ?",
                        Lis("Өглөө, өдөр, орой тогтмол хийдэг зүйлс"),
                        Lis("Өчигдөр хийсэн зүйлс", "Хийх дуртай зүйлс", "Хиймээр санагддаг зүйлс")
                    ), new QusData("Кидо өглөө босоод юу хийнэ гэж өдрийн дэглэмдээ бичсэн бэ?",
                        Lis("Нүүр, гар, шүдээ угаана"),
                        Lis("Унтана", "Дуу дуулна", "Дугуй унана")
                    ), new QusData("Мидо өглөөний цай уусныхаа дараа юу хийнэ гэж өдрийн дэглэмдээ бичсэн бэ?",
                        Lis("Тэргэнцрээ илүү супер болгох арга судална"),
                        Lis("Гадаа гарч сууна", "Аялж зугаална", "Ном уншина")
                    ), new QusData("Айдо зураг зурсныхаа дараа юу хийнэ гэж өдрийн дэглэмдээ бичсэн бэ?",
                        Lis("Цэцгээ усална"),
                        Lis("Гадаа гарна", "Хөлгөө харна", "Үүл зурна")
                    ), new QusData("Айдо унтах цагтаа унталгүй зураг зурчихсан чинь юу болсон бэ?",
                        Lis("Өдөр нь ядраад цахим хичээлээ хийж чадахгүй байсан"),
                        Lis("Унтаж чадахгүй байсан", "Хичээлээ давтмаар санагдсан", "Дахиад зураг зурмаар санагдсан")
                    ),
                };
            case GameTp.BBHBO2:
                return new List<QusData> {
                    new QusData("Дараах өгүүлбэрийн цэгийн оронд тохирох үгийг олоорой. Цахилгаан хэрэгслээ хэрэглэж дууссан даруйдаа .......... байгаарай.",
                        Lis("Унтрааж"),
                        Lis("Зөөж", "Асааж", "Хэрэглэж")
                    ), new QusData("Залгуурыг цахилгаанаас салгахдаа хэрхэн татаж салгах вэ?",
                        Lis("Залгуурын толгойн ар хэсгээс барьж, зөөлөн татаж салгах"),
                        Lis("Шууд чанга татах", "Нойтон гараар татах", "Утаснаас нь татах")
                    ), new QusData("Залгуурын утаснаас татаж чангаавал ямар аюултай вэ?",
                        Lis("Утас тасарч гэмтэн, гал гарах аюултай"),
                        Lis("Аюул бага", "Их хүч шаардана", "Ямар ч аюулгүй")
                    ), new QusData("Хэрвээ шалбарч гэмтсэн цахилгааны утас байвал яах хэрэгтэй вэ?",
                        Lis("Холдоод, хүрэхгүй байх"),
                        Lis("Барьж үзэх", "Татаж үзэх", "Дэргэд нь очих")
                    ), new QusData("Залгаатай цахилгаан хэрэгслийг усанд ойртуулбал юу болох вэ?",
                        Lis("Тог дамжих учир тогонд цохиулах аюултай"),
                        Lis("Залгуур сална", "Хатаахад хэцүү", "Дуу чимээтэй болно")
                    ),
                };
            case GameTp.BBHBO3:
                return new List<QusData> {
                    new QusData("Захирлын өрөөнөөс ямар чимээ гарсан бэ?",
                        Lis("Ярилцах"),
                        Lis("Дуу дуулах", "Бүжиглэх", "Бөмбөг залах")
                    ), new QusData("Хаана шувуу жиргэх чимээ сонсогдсон бэ?",
                        Lis("Сургуулийн цэцэрлэгт хүрээлэнд"),
                        Lis("Биеийн тамирын зааланд", "Цайны газар", "Хөгжмийн өрөөнд")
                    ), new QusData("Дараах чимээг сонсоод намайг хаана байгааг таагаарай.",
                        Lis("Биеийн тамирын заал"),
                        Lis("Сургуулийн цэцэрлэгт хүрээлэн", "Захирлын өрөө", "Цайны газар")
                    ), new QusData("Томоо Бадралд дуртай хөгжмөө сонсгохоор чихэвч зүүлгэхэд Бадрал яагаад цочсон бэ?",
                        Lis("Хөгжмөө хэтэрхий чанга тавьсан учраас"),
                        Lis("Дуу сонсогдохгүй байсан болохоор", "Чихэвч зүүдэггүй болохоор", "Чихэвч ажиллахгүй байсан болохоор")
                    ), new QusData("Хэт чанга чимээ эрүүл мэндэд сайн уу, муу юу?",
                        Lis("Муу"),
                        Lis("Сайн", "Маш сайн")
                    ),
                };
            case GameTp.BBHBO4:
                return new List<QusData> {
                    new QusData("Аль нь тэжээмэл амьтан бэ?",
                        Lis("Нохой"),
                        Lis("Үнэг", "Чоно", "Баавгай")
                    ), new QusData("Аль нь зэрлэг амьтан бэ?",
                        Lis("Баавгай"),
                        Lis("Хонь", "Ямаа", "Үхэр")
                    ), new QusData("Тахиа ямар ашиг шимтэй вэ?",
                        Lis("Өндөг"),
                        Lis("Ноос", "Сүү", "Ноолуур")
                    ), new QusData("Үнээний сүүгээр юу хийдэг вэ?",
                        Lis("Ааруул"),
                        Lis("Жимсний чанамал", "Өргөст хэмх", "Гадил")
                    ), new QusData("Загас тэжээхэд юу хэрэгтэй вэ?",
                        Lis("Устай аквариум, аквариум доторх ургамал, загасны хоол"),
                        Lis("Чийдэн, түлхүүр", "Аяга, сандал", "Хүрз, сав")
                    ),
                };
            case GameTp.BBHBO5:
                return new List<QusData> {
                    new QusData("Томоог өдрийн хоолоо идэж байхад нар хаанаас туссан бэ?",
                        Lis("Эгц дээрээс"),
                        Lis("Урдаас", "Хойноос", "Зүүнээс")
                    ), new QusData("Чи алийг нь шөнө харах боломжтой вэ?",
                        Lis("Одод түгэх"),
                        Lis("Нар мандах", "Солонго татах", "Нартай бороо орох")
                    ), new QusData("Дараах үйлүүдээс алийг нь өдөр хийж болох вэ?",
                        Lis("Хичээлдээ явах"),
                        Lis("Галын наадам хийх", "Одод түгэхийг харах", "Сар гарахыг харах")
                    ), new QusData("Аль нь гэрэл, дулаан ялгаруулдаг вэ?",
                        Lis("Нар"),
                        Lis("Сар", "Од", "Үүл")
                    ), new QusData("Дэлхий нарыг тойрон эргэж байх үед сүүдэр талд хоногийн аль хэсэг болдог вэ?",
                        Lis("Шөнө"),
                        Lis("Өглөө", "Өдөр", "Орой")
                    ),
                };
            case GameTp.BBHBO6:
                return new List<QusData> {
                    new QusData("Нүд ямар үүрэгтэй вэ?",
                        Lis("Харах"),
                        Lis("Сонсох", "Идэх", "Тулах")
                    ), new QusData("Аль нь үнэрлэх үүрэгтэй эрхтэн бэ?",
                        Lis("Хамар"),
                        Lis("Хөл", "Гар", "Нуруу")
                    ), new QusData("Аль нь алхах, гишгэх, тулах үүрэгтэй эрхтэн бэ?",
                        Lis("Хөл"),
                        Lis("Хүзүү", "Бугуй", "Тохой")
                    ), new QusData("Аль нь зөв бэ?",
                        Lis("Хамгаалалтын хэрэгсэлтэй дугуй унах"),
                        Lis("Хамгаалалтын хэрэгсэлгүй дугуй унах", "Хамгаалалтын хэрэгслээ өмсөхгүй дугуй унах")
                    ), new QusData("Аль нь зөв бэ?",
                        Lis("Өвлийн хүйтэнд зузаан хувцас өмсөх"),
                        Lis("Өвлийн хүйтэнд нимгэн хувцас өмсөх")
                    ),
                };
            case GameTp.BBHBO7:
                return new List<QusData> {
                    new QusData("Жижгээгийн сургууль гэрээс нь аль зүгт байсан бэ?",
                        Lis("Баруун хойд"),
                        Lis("Урд", "Хойно", "Зүүн")
                    ), new QusData("Барилгын ажил явагдаж байгаа замаар явах нь аюултай юу?",
                        Lis("Тийм"),
                        Lis("Үгүй", "Огт үгүй")
                    ), new QusData("Жижгээ аавтайгаа хамт машины замаар гарахдаа юу ашигласан бэ?",
                        Lis("Шар туг"),
                        Lis("Бөмбөг", "Шугам", "Дэвтэр")
                    ), new QusData("Харанхуй замаар явахаар болбол юу ашиглан замаа гэрэлтүүлж болох вэ?",
                        Lis("Гар чийдэн"),
                        Lis("Шугам", "Цүнх", "Дэвтэр")
                    ), new QusData("Гар чийдэн юугаар ажилладаг вэ?",
                        Lis("Зайгаар"),
                        Lis("Тосоор", "Галаар", "Дулаанаар")
                    ),
                };
            case GameTp.BBHBO8:
                return new List<QusData> {
                    new QusData("Тасалгааны цэцгийг хэд ангилдаг вэ?",
                        Lis("4"),
                        Lis("1", "2", "3")
                    ), new QusData("Кидогийн цэцэг яагаад хатаж хорчийсон бэ?",
                        Lis("Цэцгээ буруу арчилсан"),
                        Lis("Цэцгээ арчлахаа мартсан", "Цэцгээ арчлаагүй")
                    ), new QusData("Аль нь гоёмсог навчтай ургамал вэ?",
                        Lis("Мөнхрөө"),
                        Lis("Алаг цэцэг", "Зуун наст", "Нил цэцэг")
                    ), new QusData("Аль нь гоёмсог цэцэглэдэг ургамал вэ?",
                        Lis("Нил цэцэг"),
                        Lis("Зуун наст", "Мөнхрөө", "Багцхан")
                    ), new QusData("Цэцэг бүрийн арчилгаа адилхан уу?",
                        Lis("Үгүй, өөр"),
                        Lis("Тийм, адилхан", "Бүгд адил")
                    ),
                };
            case GameTp.BBHBO9:
                return new List<QusData> {
                    new QusData("Хотгор газраар налууг даган урсаж байгаа усыг юу гэдэг вэ?",
                        Lis("Гол"),
                        Lis("Ус", "Нуур", "Худаг")
                    ), new QusData("Томоохон хонхорт тогтсон их усыг юу гэдэг вэ?",
                        Lis("Нуур"),
                        Lis("Ус", "Гол", "Худаг")
                    ), new QusData("Аль нь уул, усыг хамгаалж буй зөв үйлдэл вэ?",
                        Lis("Мод тарих"),
                        Lis("Мод тайрах", "Голд хог хаях", "Цэцэг ногоог үндсээр нь зулгаах")
                    ), new QusData("Аль нь уул, усыг сүйтгэж буй буруу үйлдэл вэ?",
                        Lis("Цэцэг ногоог үндсээр нь зулгаах"),
                        Lis("Голын хог цэвэрлэх", "Мод тарих", "Хогоо байгальд ил хаяхгүй байх")
                    ), new QusData("Томоо, Жижгээ хоёр юу ойлгосон бэ?",
                        Lis("Хогоо ил хаяхгүй, өвс ургамал гэмтээхгүй байх хэрэгтэйг"),
                        Lis("Хөдөө явахдаа дуу дуулахыг", "Нуур хаана байдгийг", "Уул руу яаж гарахыг")
                    ),
                };
            case GameTp.BBHBO10:
                return new List<QusData> {
                    new QusData("Хэрвээ нартай, халуун өдөр байвал чи ямар хувцас өмсөх вэ?",
                        Lis("Богино өмд, нимгэн цамц, саравчтай малгай, нарны шил"),
                        Lis("Зузаан өмд, цамц, малгай", "Цасны өмд, ноосон цамц, ороолт", "Усны гутал, ороолт, бээлий")
                    ), new QusData("Хэрвээ бороотой, сэрүүн өдөр байвал чи ямар хувцас өмсөх вэ?",
                        Lis("Усны гутал, цув, шүхэр"),
                        Lis("Ноосон өмд, цамц, малгай", "Цасны өмд, зузаан цамц", "Углааш, бээлий")
                    ), new QusData("Хэрвээ цастай, хүйтэн өдөр байвал чи ямар хувцас өмсөх вэ?",
                        Lis("Ноосон өмд, цамц, малгай, ороолт, бээлий, дулаан гутал"),
                        Lis("Богино өмд, нимгэн цамц, саравчтай малгай, нарны шил", "Усны гутал, цув, шүхэр", "Углааш, даашинз, нарны малгай")
                    ), new QusData("Жижгээ яагаад нимгэн хувцсаа сольж өмссөн бэ?",
                        Lis("Гадаа хүйтэн байсан"),
                        Lis("Гадаа нартай байсан", "Гадаа дулаахан байсан", "Гадаа халуун байсан")
                    ), new QusData("Аялал зугаалгаар явахдаа юуг зайлшгүй анхаарах хэрэгтэй вэ?",
                        Lis("Цаг агаарын мэдээ сонсож, тохируулан хувцаслах"),
                        Lis("Дуртай юмнуудаа авах", "Өдөржин тоглох", "Олон тоглоом мэддэг байх")
                    ),
                };
        }
        return new List<QusData> { new QusData("Qus", Lis("1"), Lis("-1", "-2", "-3")) };
    }
}
