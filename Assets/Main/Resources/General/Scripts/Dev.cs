using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Dev : Mb {
    bool inGame = false;
    void Start() { StaCor(Cor()); }
    IEnumerator Cor(float tm = 0.1f, float waitTm = 5) {
        int idx = Codes.IndexOf(Code);
        GameObject cGo = go.ChildGo(idx);
        for (int i = 0; i < tf.childCount; i++)
            go.ChildAct(i == idx, i);
        Code = "";
        yield return SclCor(cGo, V3.O, V3.I, tm, EaseTp.OutSine);
        yield return Wf.Sec(waitTm);
        yield return SclCor(cGo, V3.I, V3.O, tm, EaseTp.InSine);
        Dst(go);
    }
    static List<string> Codes = Lis("ulziiundralorgil");
    static string Code = "";
    public static void Check() {
        if (Input.anyKeyDown) {
            foreach (string str in Codes)
                if (Code.Length < str.Length && Input.GetKeyDown(KeyCode.A + str[Code.Length] - 'a')) {
                    string s = Code.Insert(Code.Length, "" + str[Code.Length]);
                    if (str.IsS(s)) {
                        Code = s;
                        if (Code == str) Ins(O.Load<Dev>("General/Dev/Dev"), ((Canvas)FindObjectOfType(typeof(Canvas))).transform);
                        return;
                    }
                }
            Code = "";
        }
    }
}
