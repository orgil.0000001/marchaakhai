using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Orgil;

public class VideoMenu : Mb {
#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")] static extern void ShowVideo(string url);
    [DllImport("__Internal")] static extern void HideVideo();
#endif
    static void VidAct(bool isShow) {
#if UNITY_WEBGL && !UNITY_EDITOR
        if (isShow) ShowVideo(Song.data.video);
        else HideVideo();
#endif
    }
    AudioSource src;
    AudioClip f1Clip;
    void Start() {
        src = go.Gc<AudioSource>();
        f1Clip = O.LoadClip("General/Data/video");
        VidAct(true);
        GameObject pGo = tf.root.gameObject;
        pGo.Child(0, 0).Btn().onClick.AddListener(Home);
        pGo.Child(0, 1).Btn().onClick.AddListener(Game);
        StaCor(ShowCor(pGo.ChildGo(0, 1)));
    }
    void Update() {
        Dev.Check();
        if (General.Check(KeyCode.F1)) {
            src.clip = f1Clip;
            src.Play();
        } else if (General.Check(KeyCode.Escape)) Home();
        else if (General.Check(KeyCode.Tab) || General.Check(KeyCode.Return)) Game();
    }
    void Home() {
        VidAct(false);
        General.Home();
    }
    void Game() {
        VidAct(false);
        General.Game();
    }
    IEnumerator ShowCor(GameObject go, float waitTm = 10, float tm = 0.1f) {
        go.Tls(V3.O);
        yield return Wf.Sec(waitTm);
        yield return SclCor(go, V3.O, V3.I, tm, EaseTp.OutSine);
    }
}