using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO7 : Mb {
    public static string path => nameof(BBHBO7) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(BBHBO7), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(BBHBO7), value ? 1 : 0); }
    }
    List<Song> songs;
    List<List<int>> paths = new List<List<int>> {
        Lis(0, 1, 2), Lis(0, 1, 5),
        Lis(5, 10, 9), Lis(5, 10, 11), Lis(5, 1, 0), Lis(5, 1, 2),
        Lis(11, 10, 9), Lis(11, 10, 5), Lis(11, 12, 13), Lis(11, 12, 6, 7),
        Lis(7, 6, 12, 11), Lis(7, 6, 12, 13), Lis(7, 8, 14), Lis(7, 8, 4), Lis(7, 3),
    };
    List<Button> btns = new List<Button>();
    List<int> nbrs = new List<int>();
    public int staMoney = 5000;
    public Sprite curSpr, nxtSpr, btnCorrectSpr, btnWrongSpr;
    GameObject personGo, flashlightGo, shopGo, homeGo;
    Image lightImg;
    Text moneyTxt;
    int cur, money;
    bool isShop = false;
    void Awake() { General.Crt(tf, GameTp.BBHBO7, Replay, NextLevel, (int i) => { }); }
    void Start() {
        Rect r = go.Rt().rect;
        songs = Lis(new Song(path, "1"));
        new LvlData(go.ChildGo(0)).txt.text = Song.txts[0][1].txt;
        B.Idle();
        GameObject parGo = go.ChildGo(0), pntsGo = parGo.ChildNameGo("Pnts"), txtsGo = parGo.ChildNameGo("Texts");
        personGo = parGo.ChildNameGo("Person");
        shopGo = parGo.ChildNameGo("Shop");
        for (int i = 0; i < 15; i++) {
            Button btn = pntsGo.Child(i).Btn();
            int j = i;
            btn.onClick.AddListener(() => StaCor(BtnClkCor(j)));
            btns.Add(btn);
        }
        flashlightGo = personGo.ChildGo(1);
        lightImg = flashlightGo.Child(1).Img();
        homeGo = txtsGo.ChildGo(3);
        homeGo.Child(0).Txt().text = Song.txts[0][3].txtU; // ger
        txtsGo.Child(0).Txt().text = Song.txts[0][2].txtU;// surguuli
        moneyTxt = txtsGo.Child(2, 0).Txt();
        txtsGo.Child(1).Txt().text = shopGo.Child(0).Txt().text = Song.txts[0][4].txtU; // delguur
        shopGo.Child(1, 0, 1, 0).Txt().text = Song.txts[0][5].txtU; // chiher
        shopGo.Child(1, 1, 1, 0).Txt().text = Song.txts[0][6].txtU; // zairmag
        shopGo.Child(1, 2, 1, 0).Txt().text = Song.txts[0][7].txtU; // gar chiiden
        shopGo.Tls(V3.O);
        CrtData();
    }
    void ShowPath(int idx) {
        cur = idx;
        nbrs.Clear();
        for (int i = 0; i < paths.Count; i++)
            if (paths[i][0] == idx)
                nbrs.Add(paths[i].Last());
        for (int i = 0; i < btns.Count; i++) {
            if (i == idx) {
                btns[i].Show();
                btns[i].Img().sprite = curSpr;
            } else if (nbrs.Contains(i)) {
                btns[i].Show();
                btns[i].Img().sprite = nxtSpr;
            } else {
                btns[i].Hide();
            }
        }
    }
    bool isClk = false;
    bool IsPath(int idx, int a, int b) { return cur == a && idx == b || cur == b && idx == a; }
    IEnumerator BtnClkCor(int idx) {
        if (!isClk && !isShop) {
            if (nbrs.Contains(idx)) {
                B.PlaySong(true);
                isClk = true;
                int i;
                for (i = 0; i < paths.Count && !(paths[i][0] == cur && paths[i].Last() == idx); i++) ;
                bool isDark = IsPath(idx, 11, 7);
                if (isDark && flashlightGo.activeSelf)
                    yield return Cor(t => lightImg.color = lightImg.color.A(t), 0.1f);
                for (int j = 1; j < paths[i].Count; j++) {
                    Vector3 a = btns[paths[i][j - 1]].Tp(), b = btns[paths[i][j]].Tp();
                    yield return Cor(t => personGo.Tp(V3.Lerp(a, b, t)), V3.Dis(a, b) / (Screen.height * 0.42f));
                }
                if (isDark && flashlightGo.activeSelf)
                    yield return Cor(t => lightImg.color = lightImg.color.A(1 - t), 0.1f);
                ShowPath(idx);
                if (idx == 0) {
                    print("surguuli");
                } else if (idx == 2) {
                    yield return WrongCor(new Song(path, "Barilga"));
                } else if (idx == 3) {
                    yield return WrongCor(new Song(path, "Zam"));
                } else if (idx == 4) {
                    yield return WrongCor(new Song(path, "Horiglono"));
                } else if (idx == 5) {
                    ShowShop();
                } else if (isDark) {
                    if (!flashlightGo.activeSelf)
                        yield return WrongCor(new Song(path, "Haranhui"));
                } else if (idx == 9) {
                    yield return WrongCor(new Song(path, "Muhar"));
                } else if (idx == 13) {
                    yield return WrongCor(new Song(path, "Nuh"));
                } else if (idx == 14) {
                    Song song = new Song(path, "Home");
                    B.PlaySong(song);
                    yield return Wf.Sec(song.len);
                    StaCor(B.WinShowCor());
                } else {
                    print(idx);
                }
                isClk = false;
            } else {
                B.PlaySong(false);
            }
        }
    }
    IEnumerator WrongCor(Song song) {
        B.PlaySong(song);
        yield return Wf.Sec(song.len);
        yield return Wf.Sec(1);
        B.PlaySong(false);
        StaCor(ResetCor());
    }
    IEnumerator ResetCor(float tm = 0.5f, float tm2 = 1f) {
        money = staMoney;
        moneyTxt.text = "" + money;
        personGo.Tp(btns[0].Tp());
        homeGo.Tls(V3.O);
        flashlightGo.Hide();
        ShowPath(0);
        yield return SclCor(personGo, V3.O, V3.I, tm, EaseTp.OutSine);
        yield return Wf.Sec(tm2);
        yield return SclCor(homeGo, V3.O, V3.I, tm, EaseTp.OutSine);
    }
    void CrtData() {
        StaCor(ResetCor());
        PlaySong();
    }
    List<int> prices = Lis(1500, 2000, 4000);
    void ShowShop() {
        isShop = true;
        ShopBtns();
        StaCor(Cor(t => shopGo.Tls(V3.Lerp(V3.O, V3.I, t)), 0.2f, EaseTp.OutSine));
    }
    public void ShopExit() {
        StaCor(Cor(t => shopGo.Tls(V3.Lerp(V3.I, V3.O, t)), 0.2f, EaseTp.InSine));
        isShop = false;
    }
    public void ShopSelect(int tp) {
        if (prices[tp] <= money) {
            B.PlaySong(true);
            money -= prices[tp];
            moneyTxt.text = "" + money;
            if (tp == 2)
                flashlightGo.Show();
            ShopBtns();
        } else {
            B.PlaySong(false);
        }
    }
    void ShopBtns() {
        for (int i = 0; i < 3; i++)
            shopGo.Child(1, i, 1).Img().sprite = prices[i] <= money ? btnCorrectSpr : btnWrongSpr;
    }
    public void PlaySong() { B.PlaySong(songs[0]); }
    public void Replay() { StaCor(B.WinHideCor(null, CrtData)); }
    public void NextLevel() { General.Celebrate(); }
}