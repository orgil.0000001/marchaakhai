using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO4_3 : GameMb {
    BBHBO4 main => BBHBO4._;
    List<(RectTransform, Vector3)> rts = new List<(RectTransform, Vector3)>();
    Transform pTf;
    RectTransform parRt;
    List<int> cnts = Lis(5, 3, 2, 3, 3, 4, 2, 2), idxs = new List<int>();
    Vector2 sz1, sz2;
    public override void Init() {
        data.txt.text = Song.txts[0][3].txt;
        pTf = go.Child(0, 0);
        parRt = go.Child(0, 1, 0).Rt();
        for (int i = 0; i < pTf.childCount; i++) {
            var rt = pTf.Child(i).Rt();
            rts.Add((rt, rt.Tlp()));
        }
        sz1 = rts[0].Item1.sizeDelta;
        sz2 = V2.V(80);
    }
    public override void CrtData() {
        List<int> l = new List<int>();
        for (int i = 0; i < cnts.Count; i++) l.Add(i);
        if (B.gameIdx == 0) {
            l.RndShuffle();
            idxs = l.Rng(0, 3);
            main.Fill(0);
            B.LevelBar();
        }
        l.Rmv(idxs[B.gameIdx]);
        l.RndShuffle();
        l = Lis(idxs[B.gameIdx]).Add(l.Rng(0, 2));
        rts.RndShuffle();
        int k = 0;
        parRt.Img().sprite = O.LoadSpr(BBHBO4.path + "z3_" + (l[0] + 1));
        for (int i = 0; i < l.Count; i++)
            for (int j = 0; j < cnts[l[i]]; j++, k++) {
                rts[k].Item1.Show();
                rts[k].Item1.Par(pTf);
                rts[k].Item1.name = "" + i;
                rts[k].Item1.Img().sprite = O.LoadSpr(BBHBO4.path + "z3_" + (l[i] + 1) + "_" + (j + 1));
                rts[k].Item1.Tlp(rts[k].Item2 + V3.Xy(Rnd.Rng(-50, 50), Rnd.Rng(-30, 30)));
                rts[k].Item1.Tls(V3.I);
                rts[k].Item1.sizeDelta = sz1;
            }
        for (; k < rts.Count; k++) {
            rts[k].Item1.Hide();
            rts[k].Item1.Par(pTf);
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(Lis((parRt, parRt.Tlp())).Add(rts), tm, isShow, b => b.Item1.gameObject);
        yield return Wf.Sec(tm);
    }
    RectTransform rt;
    Vector3 rtP;
    void Update() {
        if (B.lvlIdx == 2) {
            if (IsMbD) {
                rt = rts.Find(btn => btn.Item1.IsRt(Mp)).Item1;
                if (rt && rt.parent == pTf) {
                    rtP = rt.Tp();
                    mp = Mp;
                    rt.Par(pTf.parent);
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + Mp - mp);
            }
            if (IsMbU && rt) {
                if (parRt.IsRt(Mp) && rt.name == "0") {
                    B.PlaySong(true);
                    rt.Par(parRt.Child(0));
                    rt.sizeDelta = sz2;
                    if (rt.parent.childCount == cnts[idxs[B.gameIdx]])
                        StaCor(Cor());
                } else {
                    B.PlaySong(false);
                    rt.Par(pTf);
                    rt.Tp(rtP);
                }
            }
        }
    }
    IEnumerator Cor() {
        B.Good();
        yield return Wf.Sec(2);
        StaCor(main.WinCor());
    }
}