using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO4_1 : GameMb {
    BBHBO4 main => BBHBO4._;
    List<Transform> tfs = new List<Transform>();
    List<RectTransform> rts = new List<RectTransform>();
    List<List<(RectTransform, Vector3, Vector2)>> rts2 = new List<List<(RectTransform, Vector3, Vector2)>> { new List<(RectTransform, Vector3, Vector2)>(), new List<(RectTransform, Vector3, Vector2)>() };
    public override void Init() {
        data.txt.text = Song.txts[0][1].txt;
        tfs = Lis(go.Child(0, 0, 0), go.Child(0, 0, 1), go.Child(1, 0));
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < tfs[i].childCount; j++) {
                var rt = tfs[i].Child(j).Rt();
                rts.Add(rt);
                rts2[i].Add((rt, rt.Tlp(), rt.sizeDelta));
            }
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            rts.RndShuffle();
            rts.ForEach(rt => {
                rt.sizeDelta = V2.V(150);
                rt.Par(tfs[2]);
                rt.Tls(V3.I);
            });
            main.Fill(0);
            B.LevelBar();
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    RectTransform rt;
    Vector3 rtP;
    int idx;
    void Update() {
        if (B.lvlIdx == 0) {
            if (IsMbD) {
                rt = rts.Find(btn => btn.IsRt(Mp));
                if (rt && rt.parent == tfs[2]) {
                    rtP = rt.Tp();
                    mp = Mp;
                    idx = rt.SibIdx();
                    rt.Par(tfs[2].parent);
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + (Mp - mp).Z(0));
            }
            if (IsMbU && rt) {
                int i = -1;
                if (tfs[0].parent.Rt().IsRt(Mp) && !tfs[2].Rt().IsRt(Mp)) {
                    i = rt.Tlp().x < 0 ? 0 : 1;
                    if (!rts2[i].Any(btn => btn.Item1 == rt)) i = -1;
                }
                if (i >= 0) {
                    B.PlaySong(true);
                    var e = rts2[i].Find(btn => btn.Item1 == rt);
                    rt.Par(tfs[i]);
                    rt.Tlp(e.Item2);
                    rt.sizeDelta = e.Item3;
                    main.Fill(rts.Count - tfs[2].childCount, rts.Count);
                    if (tfs[2].childCount == 0)
                        StaCor(Cor());
                } else {
                    B.PlaySong(false);
                    rt.Par(tfs[2]);
                    rt.SibIdx(idx);
                }
            }
        }
    }
    IEnumerator Cor() {
        B.Good();
        yield return Wf.Sec(2);
        StaCor(main.WinCor());
    }
}