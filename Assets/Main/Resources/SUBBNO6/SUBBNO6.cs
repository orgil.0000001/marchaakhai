using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBBNO6 : Singleton<SUBBNO6> {
    public static string path => nameof(SUBBNO6) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBBNO6), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBBNO6), value ? 1 : 0); }
    }
    List<Game1> lvl1 = new List<Game1>();
    Game1 game1 => lvl1[B.gameIdx];
    List<Game2> lvl2 = new List<Game2>();
    Game2 game2 => lvl2[B.gameIdx];
    List<Game3> lvl3 = new List<Game3>();
    Game3 game3 => lvl3[B.gameIdx];
    List<LvlData> lvlDatas = new List<LvlData>();
    LvlData lvlData => lvlDatas[B.lvlIdx];
    bool isWin = false;
    List<Song> songs;
    void Awake() { General.Crt(tf, GameTp.SUBBNO6, Replay, NextLevel, SelectLevel); }
    void Start() {
        Rect r = go.Rt().rect;
        songs = Lis(new Song(path, "1_1"), new Song(path, "1_2"), new Song(path, "2"), new Song(path, "3"));
        for (int i = 0; i < 3; i++)
            lvlDatas.Add(new LvlData(go.ChildGo(0, i)));
        Transform tf1 = lvlDatas[0].go.Child(1), tf2 = lvlDatas[1].go.Child(1), tf3 = lvlDatas[2].go.Child(1);
        for (int i = 0; i < tf1.childCount; i++)
            lvl1.Add(new Game1(lvlDatas[0], tf1.Child(i)));
        for (int i = 0; i < tf2.childCount; i++)
            lvl2.Add(new Game2(lvlDatas[1], tf2.Child(i)));
        for (int i = 0; i < tf3.childCount - 1; i++)
            lvl3.Add(new Game3(lvlDatas[2], i));
        lvlDatas.ForEach(data => data.go.Tls(V3.O));
        B.Idle();
        if (IsAuto) {
            lvlData.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    int dataIdx => B.lvlIdx == 0 ? B.gameIdx : B.lvlIdx + 1;
    void CrtData() {
        isWin = false;
        lvlData.txt.text = Song.txts[0][dataIdx + 1].txt;
        if (B.lvlIdx == 0) game1.Init();
        else if (B.lvlIdx == 1) game2.Init();
        else game3.Init();
        if (B.gameIdx == 0) {
            Fill(0);
            B.LevelBar();
            if (B.lvlIdx == 0) {
                lvl1[0].Scl(V3.I);
                lvl1[1].Scl(V3.O);
            } else if (B.lvlIdx == 1) {
                game2.Scl(V3.I);
            } else {
                game3.Scl(true);
                game3.tf.Tls(game3.scl);
            }
        }
        PlaySong();
    }
    void Update() {
        if (B.lvlIdx == 0) {
            game1.Upd(isWin);
        } else if (!isWin) {
            if (B.lvlIdx == 1) game2.Upd();
            else game3.Upd();
        }
    }
    void Fill(int i) { B.LevelBarFill(i, B.lvlIdx == 0 ? 2 : 1); }
    IEnumerator WinCor(float tm = 0.5f) {
        isWin = true;
        B.Correct(true);
        Fill(B.gameIdx + 1);
        yield return Wf.Sec(2);
        if (B.gameIdx + 1 == (B.lvlIdx == 0 ? lvl1.Count : B.lvlIdx == 1 ? lvl2.Count : lvl3.Count)) {
            yield return B.LevelHideCor(lvlData);
            if (B.lvlIdx == 2) {
                game3.tf.Tls(V3.O);
                game3.rts.ForEach(rt => rt.Par(game3.piecesTf));
            }
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            if (B.lvlIdx == 0) {
                B.SclTm(game1.rts, tm, false);
            } else if (B.lvlIdx == 1) {
                B.SclTm(game2.imgs, tm, false);
            } else {
                yield return SclCor(game3.tf.gameObject, game3.scl, V3.O, tm);
                game3.rts.ForEach(rt => rt.Par(game3.piecesTf));
                game3.Scl(false);
            }
            B.TxtCol(lvlData, tm, true);
            yield return Wf.Sec(tm);
            B.gameIdx++;
            CrtData();
            if (B.lvlIdx == 0) {
                B.SclTm(game1.rts, tm, true);
            } else if (B.lvlIdx == 1) {
                B.SclTm(game2.rts, tm, true);
                B.SclTm(game2.imgs, tm, true);
            } else {
                StaCor(SclCor(game3.tf.gameObject, V3.O, game3.scl, tm));
                B.SclTm(game3.rts, tm, true);
                yield return Wf.Sec(tm);
            }
            B.TxtCol(lvlData, tm, false);
            yield return Wf.Sec(tm);
        }
    }
    public void PlaySong() { B.PlaySong(songs[dataIdx]); }
    public void Replay() { StaCor(B.WinHideCor(lvlData, CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx == 2) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvlDatas[++B.lvlIdx], CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvlDatas[idx], CrtData, idx)); }
    class Game1 : Mb2 {
        public List<RectTransform> rts = new List<RectTransform>();
        public List<Vector3> pnts = new List<Vector3>();
        public List<int> idxs = new List<int>();
        float dy = 0, y = 0;
        int idx;
        RectTransform rt;
        Transform parTf;
        Vector3 rtP;
        LvlData data;
        bool isNear = false;
        public Game1(LvlData data, Transform tf) {
            this.data = data;
            for (int i = 0; i < tf.childCount; i++) {
                rts.Add(tf.Child(i).Rt());
                pnts.Add(rts[i].Tp());
            }
            idxs = 0.Fill(tf.childCount);
            Scl(V3.zpp);
        }
        public void Scl(Vector3 scl) {
            rts.ForEach(rt => rt.Tls(scl));
        }
        public void Init() {
            isNear = false;
            Vector3 lvlScl = data.go.Tls(), lvlP = data.go.Tp();
            data.go.Tls(V3.I);
            data.go.Tp(data.staP);
            List<Vector3> l = pnts.Copy();
            l.Shuffle();
            for (int i = 0; i < l.Count; i++) {
                rts[i].Tp(l[i]);
                rts[i].TlpY(0);
            }
            data.go.Tls(lvlScl);
            data.go.Tp(lvlP);
        }
        public void Upd(bool isWin) {
            if (IsMbD && !isWin) {
                isNear = true;
                rt = rts.Find(btn => btn.Rt().IsRt(Mp));
                if (rt) {
                    rtP = rt.Tp().Y(pnts[0].y);
                    mp = Mp;
                    parTf = rt.Par();
                    rt.Par(parTf.Par());
                    y = 50;
                }
            }
            if (IsMb && !isWin && rt) {
                rt.Tp(rtP + V3.X(Mp.x - mp.x));
                int i0 = idx = pnts.NearIdx(rt.Tp());
                for (int i = 0; i < idxs.Count; i++)
                    idxs[i] = rts[i] == rt ? -1 : pnts.NearIdx(rts[i].Tp());
                if (idxs.Contains(idx))
                    for (i0 = 0; i0 < idxs.Count && idxs.Contains(i0); i0++) ;
                if (idx > i0) {
                    for (int i = 0; i < idxs.Count; i++)
                        if (i0 < idxs[i] && idxs[i] <= idx)
                            idxs[i]--;
                } else if (idx < i0) {
                    for (int i = 0; i < idxs.Count; i++)
                        if (idx <= idxs[i] && idxs[i] < i0)
                            idxs[i]++;
                }
            }
            if (IsMbU && !isWin && rt) {
                rt.Par(parTf);
                y = 0;
                idxs[idxs.IndexOf(-1)] = idx;
                int i;
                for (i = 0; i < idxs.Count && i == idxs[i]; i++) ;
                if (i == idxs.Count)
                    _.StaCor(_.WinCor());
            }
            if (rt && isNear) {
                int i;
                for (i = 0; i < idxs.Count; i++)
                    if (idxs[i] >= 0)
                        rts[i].Tp(V3.Lerp(rts[i].Tp(), pnts[idxs[i]], Dt * 20));
                if (!idxs.Contains(-1)) {
                    for (i = 0; i < idxs.Count; i++)
                        if (V3.Dis(rts[i].Tp(), pnts[idxs[i]]) > 10 * rt.root.Tls().x)
                            break;
                    if (i == idxs.Count) {
                        dy = 0;
                        isNear = false;
                    }
                }
                dy = M.Lerp(dy, y, Dt * 15);
                rt.TpY(rtP.y + dy);
            }
        }
    }
    class Game2 : Mb2 {
        public List<RectTransform> rts = new List<RectTransform>(), corrects = new List<RectTransform>();
        public List<Vector3> pnts = new List<Vector3>();
        RectTransform rt;
        Transform parTf, btnsTf;
        Vector3 rtP;
        LvlData data;
        List<List<Transform>> tfs = new List<List<Transform>>();
        public List<Transform> imgs = new List<Transform>(), imgs2 = new List<Transform>();
        List<Transform> btns = new List<Transform>();
        List<Vector3> btnPnts = new List<Vector3>();
        public Game2(LvlData data, Transform tf) {
            this.data = data;
            Transform bgTf = tf.Child(0);
            for (int i = 0; i < bgTf.childCount; i++) {
                List<Transform> l = new List<Transform>();
                Transform imgTf = bgTf.Child(i, 0);
                for (int j = 0; j < imgTf.childCount; j++)
                    l.Add(imgTf.Child(j));
                tfs.Add(l);
                imgs.Add(imgTf);
            }
            btnsTf = tf.Child(1);
            btns = btnsTf.Childs();
            btnPnts = btns.Parse(tf => tf.Tlp());
            Scl(V3.zpp);
        }
        public void Scl(Vector3 scl) {
            btns.ForEach(rt => rt.Tls(scl));
            imgs.ForEach(rt => rt.Tls(scl));
        }
        public void Init() {
            Vector3 lvlScl = data.go.Tls(), lvlP = data.go.Tp();
            data.go.Tls(V3.I);
            data.go.Tp(data.staP);
            rts.Clear();
            pnts.Clear();
            corrects.Clear();
            Vector3 imgScl = imgs[0].Tls();
            imgs.ForEach(tf => tf.Tls(V3.I));
            for (int i = 0; i < btns.Count; i++) {
                btns[i].Par(btnsTf);
                btns[i].Tlp(btnPnts[i]);
            }
            List<int> idxs = new List<int>();
            for (int i = 0; i < imgs.Count; idxs.Add(i++)) ;
            idxs.Shuffle();
            imgs2.Clear();
            for (int i = 0; i < imgs.Count; i++) {
                imgs[idxs[i]].Par().SibIdx(i);
                imgs[idxs[i]].ParAct(i < 3);
                if (i < 3) imgs2.Add(imgs[idxs[i]]);
            }
            imgs[0].Par(1).UpdLayout();
            List<Vector3> l = new List<Vector3>();
            for (int i = 0, n = 2; i < 3; i++) {
                List<Transform> tfs2 = tfs[idxs[i]];
                tfs2.Shuffle();
                for (int j = 0; j < tfs2.Count; j++) {
                    tfs2[j].Act(j < n);
                    if (j < n) {
                        Transform cTf = btns[i * n + j].Child(0);
                        cTf.Img().sprite = tfs2[j].parent.Img().sprite;
                        cTf.Rt().anchoredPosition = -tfs2[j].Rt().anchoredPosition;
                        cTf.Rt().sizeDelta = tfs2[j].Par(1).Rt().sizeDelta;
                        pnts.Add(tfs2[j].Tp());
                        rts.Add(cTf.parent.Rt());
                        l.Add(cTf.parent.Tp());
                    }
                }
            }
            l.Shuffle();
            for (int i = 0; i < l.Count; i++)
                rts[i].Tp(l[i]);
            imgs.ForEach(tf => tf.Tls(imgScl));
            data.go.Tls(lvlScl);
            data.go.Tp(lvlP);
        }
        public void Upd() {
            if (IsMbD) {
                rt = rts.Find(btn => btn.Rt().IsRt(Mp));
                if (corrects.Contains(rt))
                    rt = null;
                if (rt) {
                    rtP = rt.Tp();
                    mp = Mp;
                    parTf = rt.Par();
                    rt.Par(rt.root);
                }
            }
            if (IsMb && rt)
                rt.Tp(rtP + Mp - mp);
            if (IsMbU && rt) {
                int i = rts.IndexOf(rt);
                if (V3.Dis(rt.Tp(), pnts[i]) < 30 * rt.root.Tls().x) {
                    B.PlaySong(true);
                    rt.Tp(pnts[i]);
                    rt.Par(imgs2.PosNear(rt.Tp()));
                    corrects.Add(rt);
                    if (corrects.Count == rts.Count)
                        _.StaCor(_.WinCor());
                } else {
                    B.PlaySong(false);
                    rt.Tp(rtP);
                    rt.Par(parTf);
                }
                rt = null;
            }
        }
    }
    class Game3 {
        public Transform tf, piecesTf;
        public List<RectTransform> rts = new List<RectTransform>();
        public Vector3 scl;
        List<Vector3> pnts = new List<Vector3>(), poss = new List<Vector3>();
        List<Quaternion> rots = new List<Quaternion>();
        Quaternion rot;
        RectTransform rt;
        Vector3 mp, rtP;
        int idx, cnt, sibIdx;
        bool isClk => idx >= 0;
        LvlData lvlData;
        public Game3(LvlData lvlData, int idx) {
            this.lvlData = lvlData;
            tf = lvlData.go.Child(1, 1 + idx);
            piecesTf = lvlData.go.Child(2, 0, idx);
            for (int i = 0; i < piecesTf.childCount; i++) {
                rts.Add(piecesTf.Child(i).Rt());
                rts[i].name = "" + (i + 1);
                poss.Add(rts[i].Tp());
                rots.Add(rts[i].Tr());
                if (piecesTf.childCount == tf.childCount)
                    pnts.Add(tf.Child(i).Tp());
            }
            Scl(false);
            scl = tf.Tls();
            tf.Tls(V3.O);
        }
        public void Init() {
            idx = -1;
            cnt = 0;
            Vector3 scl = lvlData.go.Tls();
            lvlData.go.Tls(V3.I);
            var l = rts.Copy();
            for (int i = 0; i < rts.Count; i++)
                rts[i].name = "" + (i + 1);
            rts.Shuffle();
            for (int i = 0; i < rts.Count; i++) {
                rts[i].SibIdx(i);
                rts[i].Tp(poss[i]);
                rts[i].Tr(rots[i]);
            }
            rts = l;
            lvlData.go.Tls(scl);
        }
        public void Scl(bool isBig) { rts.ForEach(rt => rt.Tls(isBig ? V3.I : V3.zpp)); }
        public void Upd() {
            if (IsMbD) {
                idx = rts.FindIndex(rt => rt.IsRt(Mp));
                if (isClk && rts[idx].name != "") {
                    rt = rts[idx];
                    sibIdx = rt.SibIdx();
                    rtP = rt.Tp();
                    rot = rt.Tr();
                    mp = Mp;
                    rt.Par(rt.root);
                } else {
                    idx = -1;
                }
            }
            if (IsMb && isClk) {
                rt.Tp(rtP + Mp - mp);
                rt.Tls(V3.Lerp(rt.Tls(), scl, Dt * 20));
                rt.Tr(Q.Lerp(rt.Tr(), Q.O, Dt * 20));
            }
            if (IsMbU && isClk && rt) {
                rt.Par(tf);
                if (V3.Dis(pnts[idx], rt.Tp()) < 50 * rt.root.Tls().x) {
                    B.PlaySong(true);
                    rt.Tp(pnts[idx]);
                    rt.Tr(Q.O);
                    rt.Tls(V3.I);
                    rt.name = "";
                    cnt++;
                    _.Fill(cnt);
                    if (cnt == rts.Count)
                        _.StaCor(_.WinCor());
                } else {
                    B.PlaySong(false);
                    rt.Par(piecesTf);
                    rt.SibIdx(sibIdx);
                    rt.Tp(rtP);
                    rt.Tr(rot);
                    rt.Tls(V3.I);
                }
                idx = -1;
            }
        }
    }
}