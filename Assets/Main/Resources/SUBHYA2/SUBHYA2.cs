using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBHYA2 : Mb {
    public static string path => nameof(SUBHYA2) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBHYA2), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBHYA2), value ? 1 : 0); }
    }
    class Ans {
        public string name;
        public Sprite spr;
        public Song song;
        public Ans(string s) {
            name = s;
            spr = O.LoadSpr(path + "z" + s);
            song = new Song(path, s);
        }
    }
    class Lvl {
        public int lvl, gameCnt;
        public Vector2Int n;
        public int cnt => (n.x * n.y) / 2;
        public Vector2 btnSz, imgSz;
        public Vector2 spc;
        public LvlData data;
        public Transform parTf;
        public List<Ans> ans = new List<Ans>();
        public Lvl(GameObject go, int lvl, int gameCnt, int ansCnt, int nx, int ny, Vector2 btnSz, float imgW, Vector2 spc, string txt) {
            this.lvl = lvl;
            this.gameCnt = gameCnt;
            n = V2I.V(nx, ny);
            this.btnSz = btnSz;
            imgSz = V2.V(imgW);
            this.spc = spc;
            data = new LvlData(go.ChildGo(0, lvl - 1));
            data.txt.text = txt;
            parTf = data.go.Child(1);
            data.go.Tls(V3.O);
            for (int i = 1; i <= ansCnt; i++)
                ans.Add(new Ans(lvl + "_" + i));
        }
        public void Init() { ans.Shuffle(); }
    }
    public Sprite faceSpr;
    public List<Sprite> backSprs;
    public Button btnPf, btn2Pf;
    List<Lvl> lvls = new List<Lvl>();
    Lvl lvl => lvls[B.lvlIdx];
    List<Button> btns = new List<Button>();
    Button clkBtn;
    float btnTm = 0.2f;
    int cnt = 0;
    bool isClk = false, isSound = false;
    IEnumerator talkCor;
    Song song;
    void Awake() { General.Crt(tf, GameTp.SUBHYA2, Replay, NextLevel, SelectLevel); }
    void Start() {
        song = new Song(path, "");
        string txt = Song.txts[0][1].txt;
        lvls = Lis(new Lvl(go, 1, 1, 3, 3, 2, V2.V(210, 170), 140, V2.V(35, 35), txt),
            new Lvl(go, 2, 1, 5, 5, 2, V2.V(156, 126), 106, V2.V(5, 30), txt),
            new Lvl(go, 3, 1, 6, 4, 3, V2.V(156, 126), 106, V2.V(5, 15), txt));
        B.Idle();
        if (IsAuto) {
            lvl.data.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    void CrtData() {
        if (B.gameIdx == 0) {
            lvl.Init();
            Fill(0);
            B.LevelBar();
        }
        StaCor(talkCor = TalkCor());
        List<int> l = new List<int>();
        for (int i = 0; i < lvl.cnt * 2; i++)
            l.Add(i);
        l.Shuffle();
        lvl.parTf.DstChilds();
        btns.Clear();
        for (int i = 0; i < l.Count; i++) {
            Button btn = Ins(B.lvlIdx == 1 ? btn2Pf : btnPf, lvl.parTf);
            Ans ans = lvl.ans[B.gameIdx * lvl.cnt + l[i] % lvl.cnt];
            btn.name = ans.name;
            btn.Tlp(V3.Xy((i % lvl.n.x).X(lvl.n.x, lvl.btnSz.x, lvl.spc.x), -(i / lvl.n.x).X(lvl.n.y, lvl.btnSz.y, lvl.spc.y)));
            btn.Rt().sizeDelta = lvl.btnSz;
            btn.Img().sprite = backSprs[B.lvlIdx];
            btn.Child(B.lvlIdx == 1 ? "0 0" : "0").Img().sprite = ans.spr;
            btn.Child(0).Rt().sizeDelta = lvl.imgSz;
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn, ans)));
            btns.Add(btn);
        }
    }
    IEnumerator TalkCor() {
        isSound = true;
        B.PlaySong(song);
        yield return Wf.Sec(song.len / B.src.pitch);
        isSound = false;
    }
    IEnumerator BtnClkCor(Button btn, Ans ans) {
        if (!isClk && !isSound) {
            isClk = true;
            B.PlaySong(ans.song);
            if (!clkBtn) {
                yield return BtnCor(btn, true);
                clkBtn = btn;
            } else if (btn != clkBtn) {
                yield return BtnCor(btn, true);
                yield return Wf.Sec(1);
                if (clkBtn.name == btn.name) {
                    B.PlaySong(true);
                    StaCor(SclCor(btn.gameObject, V3.I, V3.O, btnTm, EaseTp.InSine));
                    StaCor(SclCor(clkBtn.gameObject, V3.I, V3.O, btnTm, EaseTp.InSine));
                    yield return Wf.Sec(btnTm);
                    cnt++;
                    Fill(cnt);
                } else {
                    B.PlaySong(false);
                    StaCor(BtnCor(btn, false));
                    StaCor(BtnCor(clkBtn, false));
                    yield return Wf.Sec(btnTm * 2);
                }
                clkBtn = null;
            }
            if (cnt == lvl.cnt) {
                cnt = 0;
                StaCor(WinCor());
            }
            isClk = false;
        }
    }
    IEnumerator BtnCor(Button btn, bool isShow) {
        yield return SclCor(btn.gameObject, V3.I, V3.zpp, btnTm, EaseTp.InSine);
        btn.Img().sprite = isShow ? faceSpr : backSprs[B.lvlIdx];
        btn.ChildAct(isShow, 0);
        yield return SclCor(btn.gameObject, V3.zpp, V3.I, btnTm, EaseTp.OutSine);
    }
    void Fill(int i) { B.LevelBarFill(i, btns.IsEmpty() ? 1 : btns.Count / 2); }
    IEnumerator WinCor() {
        if (B.gameIdx + 1 == lvl.gameCnt) {
            yield return B.LevelHideCor(lvl.data);
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            yield return Wf.Sec(1);
            B.gameIdx++;
            CrtData();
            foreach (var b in btns)
                StaCor(SclCor(b.gameObject, V3.O, V3.I, btnTm, EaseTp.OutSine));
            yield return Wf.Sec(btnTm);
        }
    }
    public void PlaySong() {
        StopCor(talkCor);
        StaCor(talkCor = TalkCor());
    }
    public void Replay() { StaCor(B.WinHideCor(lvls[B.lvlIdx].data, CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx + 1 == lvls.Count) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, CrtData, idx)); }
}