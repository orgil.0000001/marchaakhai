using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO3_1 : GameMb {
    BBHBO3 main => BBHBO3._;
    List<Button> btns = new List<Button>();
    List<(Sprite, Song, AudioClip)> datas = new List<(Sprite, Song, AudioClip)>();
    List<int> idxs = new List<int>();
    public Sprite defSpr, correctSpr, wrongSpr;
    public override void Init() {
        data.txt.text = Song.txts[0][1].txt;
        go.Child(1).Btn().onClick.AddListener(PlaySong);
        Transform pTf = go.Child(2);
        for (int i = 0; i < pTf.childCount; i++) {
            Button btn = pTf.Child(i).Btn();
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn)));
            btns.Add(btn);
        }
        for (int i = 1; i <= 8; i++)
            datas.Add((O.LoadSpr(BBHBO3.path + "z1_" + i), new Song(BBHBO3.path, "1_" + i), O.LoadClip(BBHBO3.path + "1_" + i)));
    }
    public override void CrtData() {
        List<int> l = new List<int>();
        for (int i = 0; i < datas.Count; i++) l.Add(i);
        if (B.gameIdx == 0) {
            l.RndShuffle();
            idxs = l.Rng(0, 3);
            main.Fill(0);
            B.LevelBar();
        }
        l.Rmv(idxs[B.gameIdx]);
        l.RndShuffle();
        l = Lis(idxs[B.gameIdx]).Add(l.Rng(0, btns.Count - 1));
        l.RndShuffle();
        for (int i = 0; i < btns.Count; i++) {
            // btns[i].name = idxs[B.gameIdx] == l[i] ? "1" : "0";
            btns[i].name = "" + l[i];
            btns[i].Child(0, 0).Img().sprite = datas[l[i]].Item1;
            btns[i].Img().sprite = defSpr;
        }
        StaCor(SongCor());
    }
    IEnumerator SongCor() {
        main.PlaySong();
        yield return Wf.Sec(main.songs[0].len);
        PlaySong();
    }
    bool isClk = false;
    IEnumerator BtnClkCor(Button btn) {
        if (!isClk) {
            isClk = true;
            // bool isCorrect = btn.name == "1";
            bool isCorrect = idxs[B.gameIdx] == btn.name.I();
            btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
            B.PlaySong(datas[btn.name.I()].Item2);
            yield return Wf.Sec(2);
            B.Correct(isCorrect);
            yield return Wf.Sec(2);
            if (!isCorrect) B.gameIdx--;
            else main.Fill(B.gameIdx + 1);
            StaCor(main.WinCor());
            isClk = false;
        }
    }
    void PlaySong() { B.PlaySong(datas[idxs[B.gameIdx]].Item3); }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(btns, tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
}