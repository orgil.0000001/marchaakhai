using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO3_3 : GameMb {
    BBHBO3 main => BBHBO3._;
    List<Button> btns = new List<Button>();
    List<((Song, string), List<(Song, string)>)> datas = new List<((Song, string), List<(Song, string)>)>();
    public Sprite defSpr, correctSpr, wrongSpr;
    public override void Init() {
        for (int i = 0; i < 2; i++) {
            Button btn = go.Child(i + 1).Btn();
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn)));
            btns.Add(btn);
        }
        for (int i = 1; i <= 4; i++)
            datas.Add(((new Song(BBHBO3.path, "3_" + i), Song.txts[i][0].txt), Lis(
                (new Song(BBHBO3.path, "3_" + i + "z"), Song.txts[i][1].txt),
                (new Song(BBHBO3.path, "3_" + i + "b"), Song.txts[i][2].txt)
            )));
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            datas.RndShuffle();
            main.Fill(0);
            B.LevelBar();
        }
        data.txt.text = datas[B.gameIdx].Item1.Item2;
        for (int i = 0, j = Rnd.B ? 1 : 0; i < 2; i++) {
            btns[i].Img().sprite = defSpr;
            btns[i].Child(0).Txt().text = datas[B.gameIdx].Item2[i == j ? 0 : 1].Item2;
            btns[i].name = i == j ? "1" : "0";
        }
        main.PlaySong();
    }
    public void PlaySong() { B.PlaySong(datas[B.gameIdx].Item1.Item1); }
    bool isClk = false;
    IEnumerator BtnClkCor(Button btn, float tm = 0.2f) {
        if (!isClk) {
            isClk = true;
            bool isCorrect = btn.name == "1";
            btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
            Song song = datas[B.gameIdx].Item2[isCorrect ? 0 : 1].Item1;
            B.PlaySong(song);
            yield return Wf.Sec(song.len);
            B.Correct(isCorrect);
            yield return Wf.Sec(2);
            if (!isCorrect) B.gameIdx--;
            else main.Fill(B.gameIdx + 1);
            StaCor(main.WinCor());
            isClk = false;
        }
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(btns.Rng(0, datas[B.gameIdx].Item2.Count), tm, isShow, V3.pzp, V3.I, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
}