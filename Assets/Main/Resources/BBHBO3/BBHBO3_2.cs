using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO3_2 : GameMb {
    BBHBO3 main => BBHBO3._;
    List<(Sprite, Song, AudioClip)> corrects = new List<(Sprite, Song, AudioClip)>(), wrongs = new List<(Sprite, Song, AudioClip)>(), datas = new List<(Sprite, Song, AudioClip)>();
    List<RectTransform> rts = new List<RectTransform>(), rts2 = new List<RectTransform>();
    List<Vector3> pnts = new List<Vector3>();
    Transform pTf;
    RectTransform rt;
    Vector3 rtP;
    public override void Init() {
        data.txt.text = Song.txts[0][2].txt;
        for (int i = 1; i <= 4; i++) {
            corrects.Add((O.LoadSpr(BBHBO3.path + "z2z" + i), new Song(BBHBO3.path, "2z" + i), O.LoadClip(BBHBO3.path + "2z" + i)));
            wrongs.Add((O.LoadSpr(BBHBO3.path + "z2b" + i), new Song(BBHBO3.path, "2b" + i), O.LoadClip(BBHBO3.path + "2b" + i)));
        }
        pTf = go.Child(2);
        for (int i = 0; i < 4; i++) {
            rts.Add(pTf.Child(i).Rt());
            pnts.Add(rts[i].Tlp());
        }
        rts2 = Lis(go.Child(1, 0).Rt(), go.Child(1, 1).Rt());
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            corrects.RndShuffle();
            wrongs.RndShuffle();
            main.Fill(0);
            B.LevelBar();
        }
        int j = B.gameIdx * 2;
        datas = Lis(corrects[j], corrects[j + 1], wrongs[j], wrongs[j + 1]);
        datas.RndShuffle();
        for (int i = 0; i < datas.Count; i++) {
            rts[i].name = "" + i;
            rts[i].Par(pTf);
            rts[i].Tlp(pnts[i]);
            rts[i].Tls(V3.I);
            rts[i].Child(0, 0).Img().sprite = datas[i].Item1;
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(rts, tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
    void Update() {
        if (B.lvlIdx == 1) {
            if (IsMbD) {
                rt = rts.Find(btn => btn.IsRt(Mp));
                if (rt && rt.parent == pTf) {
                    rtP = rt.Tp();
                    mp = Mp;
                    rt.Par(pTf.Par());
                    B.PlaySong(datas[rt.name.I()].Item3);
                } else {
                    rt = null;
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + Mp - mp);
            }
            if (IsMbU && rt) {
                var pTf2 = rts2.Find(par => par.IsRt(Mp));
                if (pTf2 && (pTf2.name == "1") == corrects.Contains(datas[rt.name.I()])) {
                    // B.PlaySong(true);
                    B.PlaySong(datas[rt.name.I()].Item2);
                    rt.Par(pTf2.Child(0));
                    main.Fill(B.gameIdx * 4 + 4 - pTf.childCount);
                    if (pTf.childCount == 0)
                        StaCor(Cor());
                } else {
                    B.PlaySong(false);
                    rt.Par(pTf);
                    rt.Tp(rtP);
                }
            }
        }
    }
    IEnumerator Cor() {
        yield return Wf.Sec(2);
        B.Good();
        yield return Wf.Sec(2);
        StaCor(main.WinCor());
    }
}