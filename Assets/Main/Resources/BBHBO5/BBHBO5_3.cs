using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO5_3 : GameMb {
    BBHBO5 main => BBHBO5._;
    List<Button> btns = new List<Button>();
    public Sprite defSpr, correctSpr, wrongSpr;
    List<(Song, string, List<(Song, string)>)> datas = new List<(Song, string, List<(Song, string)>)>();
    public override void Init() {
        for (int i = 0; i < 3; i++) {
            Button btn = go.Child(i + 1).Btn();
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn)));
            btns.Add(btn);
        }
        for (int i = 0; i < 7; i++) {
            List<(Song, string)> l = new List<(Song, string)>();
            for (int j = 1; j < Song.txts[5 + i].Count; j++)
                l.Add((new Song(BBHBO5.path, "3_" + (i + 1) + "_" + j), Song.txts[5 + i][j].txt));
            datas.Add((new Song(BBHBO5.path, "3_" + (i + 1)), Song.txts[5 + i][0].txt, l));
        }
    }
    public void PlaySong() { B.PlaySong(datas[B.gameIdx].Item1); }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            datas.RndShuffle();
            main.Fill(0);
            B.LevelBar();
            btns[2].Tls(datas[0].Item3.Count == 2 ? V3.pzp : V3.I);
        }
        data.txt.text = datas[B.gameIdx].Item2;
        List<int> l = datas[B.gameIdx].Item3.Count == 2 ? Lis(0, 1) : Lis(0, 1, 2);
        l.RndShuffle();
        for (int i = 0; i < datas[B.gameIdx].Item3.Count; i++) {
            btns[i].Img().sprite = defSpr;
            btns[i].Child(0).Txt().text = datas[B.gameIdx].Item3[l[i]].Item2;
            btns[i].name = "" + l[i];
        }
        main.PlaySong();
    }
    bool isClk = false;
    IEnumerator BtnClkCor(Button btn, float tm = 0.2f) {
        if (!isClk) {
            isClk = true;
            bool isCorrect = btn.name == "0";
            btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
            Song song = datas[B.gameIdx].Item3[btn.name.I()].Item1;
            B.PlaySong(song, false);
            yield return Wf.Sec(song.clipLen);
            B.Correct(isCorrect);
            yield return Wf.Sec(2);
            if (isCorrect) main.Fill(B.gameIdx + 1);
            StaCor(main.WinCor(isCorrect));
            isClk = false;
        }
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(btns.Rng(0, datas[B.gameIdx].Item3.Count), tm, isShow, V3.pzp, V3.I, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
}