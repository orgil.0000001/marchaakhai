using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO5 : Singleton<BBHBO5> {
    public static string path => nameof(BBHBO5) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(BBHBO5), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(BBHBO5), value ? 1 : 0); }
    }
    [HideInInspector] public bool isWin = false;
    [HideInInspector] public Vector3 mpScl;
    List<GameMb> lvls = new List<GameMb>();
    GameMb lvl => lvls[B.lvlIdx];
    List<int> lvlCnts = Lis(1, 2, 7);
    public List<Song> songs;
    void Awake() { General.Crt(tf, GameTp.BBHBO5, Replay, NextLevel, SelectLevel); }
    [Range(1, 3)] public int level;
    void Start() {
        if (General.saveLvlIdx >= 0) B.lvlIdx = General.saveLvlIdx;
        else if (Application.isEditor) B.lvlIdx = level - 1;
        Rect r = go.Rt().rect;
        mpScl = V3.V(r.width / Screen.width, r.height / Screen.height, 1);
        songs = Lis(new Song(path, "1"), new Song(path, "2_1"), new Song(path, "2_2"));
        for (int i = 0; i < lvlCnts.Count; i++) {
            lvls.Add(go.Child<GameMb>(0, i));
            lvls[i].Data();
            lvls[i].Init();
            lvls[i].data.go.Tls(V3.O);
        }
        B.Idle();
        if (IsAuto) {
            lvl.go.Tls(V3.I);
            lvl.CrtData();
        } else B.LevelSelectShow();
    }
    public void Fill(int i) { B.LevelBarFill(i, B.lvlIdx == 0 ? 1 : 12); }
    public IEnumerator WinCor(bool isCorrect, float tm = 0.5f) {
        isWin = true;
        if (isCorrect && B.gameIdx + 1 == lvlCnts[B.lvlIdx]) {
            yield return B.LevelHideCor(lvl.data);
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            B.TxtCol(lvl.data, tm, true);
            yield return lvl.WinCor(false, tm);
            if (isCorrect) B.gameIdx++;
            lvl.CrtData();
            B.TxtCol(lvl.data, tm, false);
            yield return lvl.WinCor(true, tm);
        }
        isWin = false;
    }
    public void PlaySong() {
        if (B.lvlIdx == 2) ((BBHBO5_3)lvl).PlaySong();
        else B.PlaySong(songs[B.lvlIdx == 0 ? 0 : 1 + B.gameIdx]);
    }
    public void Replay() { StaCor(B.WinHideCor(lvl.data, lvl.CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx == lvlCnts.Count - 1) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, lvls[B.lvlIdx].CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, lvls[idx].CrtData, idx)); }
}