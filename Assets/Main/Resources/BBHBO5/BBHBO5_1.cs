using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO5_1 : GameMb {
    BBHBO5 main => BBHBO5._;
    List<RectTransform> rts = new List<RectTransform>();
    List<Vector3> pnts = new List<Vector3>();
    List<int> idxs = new List<int>();
    float dy = 0, y = 0;
    int idx;
    RectTransform rt;
    Transform parTf;
    Vector3 rtP;
    bool isNear = false;
    public override void Init() {
        Transform pTf = go.Child(1);
        for (int i = 0; i < pTf.childCount; i++) {
            rts.Add(pTf.Child(i).Rt());
            pnts.Add(rts[i].Tp());
        }
        idxs = 0.Fill(pTf.childCount);
        rts.ForEach(rt => rt.Tls(V3.zpp));
    }
    public override void CrtData() {
        isNear = false;
        data.txt.text = Song.txts[0][1].txt;
        Vector3 lvlScl = go.Tls(), lvlP = go.Tp();
        go.Tls(V3.I);
        go.Tp(data.staP);
        List<int> l = new List<int>();
        for (int i = 0; i < rts.Count; i++) l.Add(i);
        l.Shuffle();
        for (int i = 0; i < l.Count; i++) {
            rts[i].Tp(pnts[l[i]]);
            rts[i].TlpY(0);
        }
        go.Tls(lvlScl);
        go.Tp(lvlP);
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
            rts.ForEach(rt => rt.Tls(V3.I));
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    void Update() {
        if (B.lvlIdx == 0) {
            if (IsMbD && !main.isWin) {
                isNear = true;
                rt = rts.Find(btn => btn.IsRt(Mp));
                if (rt) {
                    rtP = rt.Tp().Y(pnts[0].y);
                    mp = Mp;
                    parTf = rt.Par();
                    rt.Par(parTf.Par());
                    y = 50;
                }
            }
            if (IsMb && !main.isWin && rt) {
                rt.Tp(rtP + V3.X(Mp.x - mp.x));
                int i0 = idx = pnts.NearIdx(rt.Tp());
                for (int i = 0; i < idxs.Count; i++)
                    idxs[i] = rts[i] == rt ? -1 : pnts.NearIdx(rts[i].Tp());
                if (idxs.Contains(idx))
                    for (i0 = 0; i0 < idxs.Count && idxs.Contains(i0); i0++) ;
                if (idx > i0) {
                    for (int i = 0; i < idxs.Count; i++)
                        if (i0 < idxs[i] && idxs[i] <= idx)
                            idxs[i]--;
                } else if (idx < i0) {
                    for (int i = 0; i < idxs.Count; i++)
                        if (idx <= idxs[i] && idxs[i] < i0)
                            idxs[i]++;
                }
            }
            if (IsMbU && !main.isWin && rt) {
                rt.Par(parTf);
                y = 0;
                idxs[idxs.IndexOf(-1)] = idx;
                int i;
                for (i = 0; i < idxs.Count && i == idxs[i]; i++) ;
                if (i == idxs.Count)
                    StaCor(Cor());
            }
            if (rt && isNear) {
                int i;
                for (i = 0; i < idxs.Count; i++)
                    if (idxs[i] >= 0)
                        rts[i].Tp(V3.Lerp(rts[i].Tp(), pnts[idxs[i]], Dt * 20));
                if (!idxs.Contains(-1)) {
                    for (i = 0; i < idxs.Count; i++)
                        if (V3.Dis(rts[i].Tp(), pnts[idxs[i]]) > 10 * rt.root.Tls().x)
                            break;
                    if (i == idxs.Count) {
                        dy = 0;
                        isNear = false;
                    }
                }
                dy = M.Lerp(dy, y, Dt * 15);
                rt.TpY(rtP.y + dy);
            }
        }
    }
    IEnumerator Cor() {
        B.Good();
        yield return Wf.Sec(2);
        StaCor(main.WinCor(true));
    }
}