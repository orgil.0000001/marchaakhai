using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO5_2 : GameMb {
    BBHBO5 main => BBHBO5._;
    List<List<(Sprite, string, Song)>> corrects = new List<List<(Sprite, string, Song)>>(), wrongs = new List<List<(Sprite, string, Song)>>();
    List<(Sprite, string, Song)> datas = new List<(Sprite, string, Song)>();
    List<RectTransform> rts = new List<RectTransform>(), rts2 = new List<RectTransform>();
    List<Vector3> pnts = new List<Vector3>();
    Transform pTf;
    RectTransform rt;
    Vector3 rtP;
    Dictionary<RectTransform, Song> songDic = new Dictionary<RectTransform, Song>();
    public override void Init() {
        for (int i = 0; i < 2; i++) {
            corrects.Add(new List<(Sprite, string, Song)>());
            wrongs.Add(new List<(Sprite, string, Song)>());
            for (int j = 0, n = i == 0 ? 3 : 4; j < n; j++) {
                corrects[i].Add((O.LoadSpr(BBHBO5.path + "z2_" + (i + 1) + "u" + (j + 1)), Song.txts[1 + i * 2][j].txt, new Song(BBHBO5.path, "2_" + (i + 1) + "u" + (j + 1))));
                wrongs[i].Add((O.LoadSpr(BBHBO5.path + "z2_" + (i + 1) + "s" + (j + 1)), Song.txts[2 + i * 2][j].txt, new Song(BBHBO5.path, "2_" + (i + 1) + "s" + (j + 1))));
            }
        }
        pTf = go.Child(2);
        for (int i = 0; i < pTf.childCount; i++) {
            rts.Add(pTf.Child(i).Rt());
            pnts.Add(rts[i].Tlp());
        }
        rts2 = Lis(go.Child(1, 2).Rt(), go.Child(1, 3).Rt());
        go.Child(1, 0, 0).Txt().text = Song.txts[0][4].txt;
        go.Child(1, 1, 0).Txt().text = Song.txts[0][5].txt;
    }
    public override void CrtData() {
        data.txt.text = Song.txts[0][2 + B.gameIdx].txt;
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
        }
        corrects[B.gameIdx].RndShuffle();
        wrongs[B.gameIdx].RndShuffle();
        int j = B.gameIdx * 2;
        datas = corrects[B.gameIdx].Rng(0, 3).Add(wrongs[B.gameIdx].Rng(0, 3));
        datas.RndShuffle();
        songDic.Clear();
        for (int i = 0; i < datas.Count; i++) {
            rts[i].name = "" + i;
            rts[i].Par(pTf);
            rts[i].Tlp(pnts[i]);
            rts[i].Tls(V3.I);
            rts[i].Child(0, 0, 0).Img().sprite = datas[i].Item1;
            rts[i].Child(1).Txt().text = datas[i].Item2;
            songDic.Add(rts[i], datas[i].Item3);
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(rts, tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
    int idx;
    void Update() {
        if (B.lvlIdx == 1) {
            if (IsMbD) {
                rt = rts.Find(btn => btn.IsRt(Mp));
                if (rt && rt.parent == pTf) {
                    rtP = rt.Tp();
                    mp = Mp;
                    idx = rt.SibIdx();
                    rt.Par(pTf.Par());
                    B.PlaySong(songDic[rt], false);
                } else {
                    rt = null;
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + Mp - mp);
            }
            if (IsMbU && rt) {
                var pTf2 = rts2.Find(par => par.IsRt(Mp));
                if (pTf2 && (pTf2.name == "1") == corrects[B.gameIdx].Contains(datas[rt.name.I()])) {
                    B.PlaySong(true);
                    rt.Par(pTf2);
                    main.Fill((B.gameIdx + 1) * 6 - pTf.childCount);
                    if (pTf.childCount == 0)
                        StaCor(Cor());
                } else {
                    B.PlaySong(false);
                    rt.Par(pTf);
                    rt.SibIdx(idx);
                    rt.Tp(rtP);
                }
            }
        }
    }
    IEnumerator Cor() {
        B.Good();
        yield return Wf.Sec(2);
        StaCor(main.WinCor(true));
    }
}