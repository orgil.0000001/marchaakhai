using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBHEM6 : Mb {
    public static string path => nameof(SUBHEM6) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBHEM6), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBHEM6), value ? 1 : 0); }
    }
    public Button btnPf;
    public GameObject personGo;
    public Sprite correctSpr, wrongSpr;
    bool isSound = false, isClk = false, isComlete = false;
    List<Lvl> lvls = new List<Lvl>();
    Lvl lvl => lvls[B.lvlIdx];
    List<Game> games = new List<Game>();
    Game game => lvl.isFire ? games.Last() : lvl.games[B.gameIdx];
    List<Ans> ans = new List<Ans>();
    List<Button> btns = new List<Button>();
    IEnumerator talkCor;
    Vector2Int fireCnt;
    float fireEm;
    void Awake() { General.Crt(tf, GameTp.SUBHEM6, Replay, NextLevel, SelectLevel); }
    void Start() {
        List<string> strs = Lis("", "", "", "");
        List<(int, int)> datas = Lis((2, 4), (2, 5), (1, 3), (3, 4), (2, 6), (2, 6), (3, 2), (1, 5), (1, 4), (6, 4));
        for (int i = 0; i < datas.Count; i++)
            games.Add(new Game(Song.txts[0][i + 1], i, datas[i].Item1, datas[i].Item2, ref strs));
        print("ЗУРАГ: " + strs[0] + "\nМОНГОЛ: " + strs[1] + "\nКАЗАК: " + strs[2] + "\nТУВА: " + strs[3]);
        lvls = Lis(new Lvl(go.ChildGo(0, 0), games, false, 290, 180, 54, 1, 2, 3, 4),
            new Lvl(go.ChildGo(0, 1), games, false, 216, 176, 28, 5, 6, 7, 8, 9),
            new Lvl(go.ChildGo(0, 2), games, true, 110, 96, 10, 10));
        var em = personGo.Child(2).PsEm();
        fireEm = em.rateOverTime.constant;
        B.Idle();
        if (IsAuto) {
            lvl.data.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    void CrtData() {
        if (B.gameIdx == 0) {
            B.LevelBar();
            B.LvlBar(lvl.stars, -1, true);
            B.LevelBarFill(B.gameIdx, lvl.stars.Count);
            if (B.lvlIdx == 2) {
                fireCnt = V2I.V(0, -1);
                FireEm(false);
            }
        }
        ans = lvl.isFire ? new List<Ans>(game.corrects).Add(game.wrongs) : Lis(game.corrects.Rnd()).Add(game.wrongs.Rnd(1 + B.lvlIdx));
        ans.Shuffle();
        lvl.data.txt.text = game.name.txt;
        StaCor(talkCor = TalkCor());
        lvl.btnsGo.DstChilds();
        btns.Clear();
        for (int i = 0; i < ans.Count; i++) {
            Button btn = Ins(btnPf, lvl.btnsGo.transform);
            btn.Tlp(lvl.isFire ? V3.Xy((i % (i < 6 ? 3 : 4)).X(i < 6 ? 3 : 4, lvl.btnSz.x, lvl.spc), -(i < 3 ? 0 : i < 6 ? 1 : 2).X(3, lvl.btnSz.y, lvl.spc)) : V3.X(i.X(ans.Count, lvl.btnSz.x, lvl.spc)));
            btn.Rt().sizeDelta = lvl.btnSz;
            btn.Child(0).Img().sprite = ans[i].spr;
            btn.Child(0).Rt().sizeDelta = lvl.imgSz;
            int j = i;
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn, j)));
            btns.Add(btn);
        }
    }
    IEnumerator TalkCor() {
        isSound = true;
        B.PlaySong(game.song);
        yield return Wf.Sec(game.song.len / B.src.pitch);
        if (!lvl.isFire)
            for (int i = 0; i < ans.Count; i++)
                yield return AnsCor(i);
        isSound = false;
    }
    IEnumerator AnsCor(int i) {
        yield return SclCor(btns[i].gameObject, V3.I, V3.V(1.1f), 0.1f, EaseTp.InQuad);
        B.PlaySong(ans[i].song);
        yield return Wf.Sec(ans[i].song.len / B.src.pitch);
        yield return SclCor(btns[i].gameObject, V3.V(1.1f), V3.I, 0.1f, EaseTp.OutQuad);
    }
    IEnumerator BtnClkCor(Button btn, int idx) {
        if (!isSound && !isClk && !isComlete) {
            bool isCorrect = ans[idx].isCorrect;
            btn.enabled = false;
            isClk = true;
            if (isCorrect) {
                if (lvl.isFire) {
                    yield return AnsCor(idx);
                    B.gameIdx++;
                    B.LvlBar(lvl.stars, true);
                    B.LevelBarFill(B.gameIdx, lvl.stars.Count);
                    FireEm(true);
                    if (B.gameIdx == game.corrects.Count) yield return LvlCor(btn);
                    else Correct(btn, isCorrect);
                } else {
                    B.gameIdx++;
                    B.LvlBar(lvl.stars, true);
                    B.LevelBarFill(B.gameIdx, lvl.stars.Count);
                    if (B.gameIdx == lvl.games.Count) yield return LvlCor(btn);
                    else yield return BtnCor(btn, isCorrect);
                }
            } else {
                if (lvl.isFire) {
                    yield return AnsCor(idx);
                    FireEm(false);
                    Correct(btn, isCorrect);
                } else {
                    if (B.gameIdx > 0) {
                        B.gameIdx--;
                        B.LvlBar(lvl.stars, false);
                        B.LevelBarFill(B.gameIdx, lvl.stars.Count);
                    }
                    yield return BtnCor(btn, isCorrect);
                }
            }
            if (!isComlete)
                isClk = false;
        }
    }
    void FireEm(bool isCorrect) {
        if (isCorrect) fireCnt.x++;
        else fireCnt.y++;
        float t = fireEm * (1 + fireCnt.y / game.corrects.Count.F()) * (1 - fireCnt.x / game.corrects.Count.F());
        personGo.ChildAct(isCorrect, 0);
        personGo.ChildAct(!isCorrect, 1);
        personGo.Child(2).PsEmRot(t);
        personGo.Child(2, 0).PsEmRot(t);
        personGo.Child(2, 1).PsEmRot(t);
    }
    IEnumerator BtnCor(Button btn, bool isCorrect, float tm = 0.5f) {
        Correct(btn, isCorrect);
        yield return Wf.Sec(2);
        B.SclTm(btns, tm, false, b => b.gameObject);
        B.TxtCol(lvl.data, tm, true);
        yield return Wf.Sec(tm);
        CrtData();
        B.SclTm(btns, tm, true, b => b.gameObject);
        B.TxtCol(lvl.data, tm, false);
    }
    void Correct(Button btn, bool isCorrect) {
        btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
        B.Correct(isCorrect);
    }
    public void PlaySong() {
        if (!isComlete) {
            StopCor(talkCor);
            for (int i = 0; i < ans.Count; i++)
                btns[i].Tls(V3.I);
            StaCor(talkCor = TalkCor());
        }
    }
    IEnumerator LvlCor(Button btn) {
        Correct(btn, true);
        yield return Wf.Sec(2);
        yield return B.LevelHideCor(lvl.data);
        B.gameIdx = 0;
        if (IsAuto) StaCor(B.WinShowCor());
        else B.LevelSelectShow();
    }
    public void Replay() { StaCor(B.WinHideCor(lvls[B.lvlIdx].data, CrtData)); }
    public void NextLevel() {
        if (lvl.isFire) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, CrtData, idx)); }
    class Ans {
        public bool isCorrect;
        public Sprite spr;
        public Song song;
        public Ans(bool isCorrect, string path, string s) {
            this.isCorrect = isCorrect;
            spr = O.LoadSpr(path + "z" + s);
            song = new Song(path, s);
        }
    }
    class Game {
        public Name name;
        public Song song;
        public List<Ans> corrects = new List<Ans>(), wrongs = new List<Ans>();
        public Game(Name name, int i, int correctCnt, int wrongCnt, ref List<string> strs) {
            string s = "" + (i + 1);
            this.name = name;
            song = new Song(path, s);
            Song.Strs(path, s, ref strs);
            for (int j = 0; j < correctCnt; j++)
                corrects.Add(CrtAns(path, i, j, true, ref strs));
            for (int j = 0; j < wrongCnt; j++)
                wrongs.Add(CrtAns(path, i, j, false, ref strs));
        }
        Ans CrtAns(string path, int i, int j, bool isCorrect, ref List<string> strs) {
            string s = (i + 1) + (isCorrect ? "z" : "b") + (j + 1);
            Ans ans = new Ans(isCorrect, path, s);
            if (!ans.spr) strs[0] += s + ", ";
            Song.Strs(path, s, ref strs);
            return ans;
        }
    }
    class Lvl {
        public bool isFire;
        public Vector2 btnSz, imgSz;
        public float spc;
        public LvlData data;
        public GameObject btnsGo;
        public List<Game> games = new List<Game>();
        public List<GameObject> stars = new List<GameObject>();
        public Lvl(GameObject go, List<Game> questions, bool isFire, float btnW, float imgW, float spc, params int[] idxs) {
            this.isFire = isFire;
            btnSz = V2.V(btnW);
            imgSz = V2.V(imgW);
            this.spc = spc;
            List<int> qusIdxs = new List<int>();
            for (int i = 0; i < idxs.Length; i++)
                qusIdxs.Add(idxs[i] - 1);
            qusIdxs.Shuffle();
            for (int j = 0; j < qusIdxs.Count; j++)
                games.Add(questions[qusIdxs[j]]);
            data = new LvlData(go);
            btnsGo = data.go.ChildGo(1);
            GameObject starPf = data.go.ChildGo(2, 0);
            int n = isFire ? games[0].corrects.Count : games.Count;
            for (int j = 0; j < n; j++)
                stars.Add((j == 0 ? starPf : Ins(starPf, starPf.Par())).ChildGo(0));
            data.go.Tls(V3.O);
        }
    }
}