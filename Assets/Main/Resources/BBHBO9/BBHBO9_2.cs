using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO9_2 : GameMb {
    BBHBO9 main => BBHBO9._;
    List<List<RectTransform>> rts = Lis(new List<RectTransform>(), new List<RectTransform>());
    Transform pTf;
    Vector3 scl;
    public override void Init() {
        data.txt.text = Song.txts[0][2].txt;
        pTf = go.Child(1, 1);
        for (int i = 0; i < 4; i++)
            rts[0].Add(go.Child(1, 0, 0, i).Rt());
        for (int i = 0; i < 4; i++) {
            rts[1].Add(go.Child(1, 1, i).Rt());
            rts[1][i].Child(0).Txt().text = Song.txts[1][i].txt;
        }
        scl = rts[1][0].Tls();
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            rts[1].RndShuffle();
            for (int i = 0; i < rts[1].Count; i++) {
                rts[1][i].Par(pTf);
                rts[1][i].SibIdx(i);
                rts[1][i].Tls(scl);
            }
            pTf.UpdLayout();
            main.Fill(0);
            B.LevelBar();
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    RectTransform rt;
    Vector3 rtP;
    void Update() {
        if (B.lvlIdx == 1) {
            if (IsMbD) {
                rt = rts[1].Find(btn => btn.IsRt(Mp));
                if (rt && rt.parent == pTf) {
                    rtP = rt.Tp();
                    mp = Mp;
                    rt.Par(pTf.parent);
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + Mp - mp);
                rt.Tls(V3.Lerp(rt.Tls(), V3.I, Dt * 10));
            }
            if (IsMbU && rt) {
                var rt2 = rts[0][rt.name.I() - 1];
                if (V3.Dis(rt.Tp(), rt2.Tp()) < 30 * rt.root.Tls().x) {
                    B.PlaySong(true);
                    rt.Tp(rt2.Tp());
                    rt.Tls(V3.I);
                    rt.Par(rt2.Par());
                    main.Fill(4 - pTf.childCount);
                    if (pTf.childCount == 0)
                        StaCor(Cor());
                } else {
                    B.PlaySong(false);
                    rt.Tp(rtP);
                    rt.Tls(scl);
                    rt.Par(pTf);
                }
            }
        }
    }
    IEnumerator Cor() {
        B.Good();
        yield return Wf.Sec(2);
        StaCor(main.WinCor());
    }
}