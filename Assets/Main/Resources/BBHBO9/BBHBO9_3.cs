using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO9_3 : GameMb {
    BBHBO9 main => BBHBO9._;
    List<Button> btns = new List<Button>();
    public Sprite defSpr, correctSpr, wrongSpr;
    // List<(string, List<(Song, string)>)> datas = new List<(string, List<(Song, string)>)>();
    List<(string, List<(Song, Sprite)>)> datas = new List<(string, List<(Song, Sprite)>)>();
    public override void Init() {
        for (int i = 0; i < 2; i++) {
            Button btn = go.Child(i + 1).Btn();
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn)));
            btns.Add(btn);
        }
        // for (int i = 0; i < 4; i++)
        //     datas.Add((Song.txts[0][3].txt, Lis(
        //         (new Song(BBHBO9.path, "3_" + (i + 1) + "z"), Song.txts[2][i * 2].txt),
        //         (new Song(BBHBO9.path, "3_" + (i + 1) + "b"), Song.txts[2][i * 2 + 1].txt)
        //     )));
        for (int i = 0; i < 4; i++)
            datas.Add((Song.txts[0][3].txt, Lis(
                (new Song(BBHBO9.path, "3_" + (i + 1) + "z"), O.LoadSpr(BBHBO9.path + "z3_" + (i + 1) + "z")),
                (new Song(BBHBO9.path, "3_" + (i + 1) + "b"), O.LoadSpr(BBHBO9.path + "z3_" + (i + 1) + "b"))
            )));
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            datas.RndShuffle();
            main.Fill(0);
            B.LevelBar();
        }
        data.txt.text = datas[B.gameIdx].Item1;
        for (int i = 0, j = Rnd.B ? 1 : 0; i < 2; i++) {
            // btns[i].Img().color = C.I;
            // btns[i].Child(0).Txt().text = (i == 0 ? "A: " : "B: ") + datas[B.gameIdx].Item2[i == j ? 0 : 1].Item2;
            btns[i].Img().sprite = defSpr;
            btns[i].Child(0, 0).Img().sprite = datas[B.gameIdx].Item2[i == j ? 0 : 1].Item2;
            btns[i].name = i == j ? "1" : "0";
        }
        main.PlaySong();
    }
    bool isClk = false;
    IEnumerator BtnClkCor(Button btn, float tm = 0.2f) {
        if (!isClk) {
            isClk = true;
            bool isCorrect = btn.name == "1";
            // btn.Img().color = isCorrect ? C.lime : C.lightCoral;
            btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
            Song song = datas[B.gameIdx].Item2[isCorrect ? 0 : 1].Item1;
            B.PlaySong(song);
            yield return Wf.Sec(song.len);
            B.Correct(isCorrect);
            yield return Wf.Sec(2);
            if (!isCorrect) B.gameIdx--;
            else main.Fill(B.gameIdx + 1);
            StaCor(main.WinCor());
            isClk = false;
        }
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(btns, tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
}