using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO9 : Singleton<BBHBO9> {
    public static string path => nameof(BBHBO9) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(BBHBO9), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(BBHBO9), value ? 1 : 0); }
    }
    [HideInInspector] public bool isWin = false;
    [HideInInspector] public Vector3 mpScl;
    List<GameMb> lvls = new List<GameMb>();
    GameMb lvl => lvls[B.lvlIdx];
    List<int> lvlCnts = Lis(1, 1, 4);
    List<Song> songs;
    void Awake() { General.Crt(tf, GameTp.BBHBO9, Replay, NextLevel, SelectLevel); }
    [Range(1, 3)] public int level;
    void Start() {
        if (General.saveLvlIdx >= 0) B.lvlIdx = General.saveLvlIdx;
        else if (Application.isEditor) B.lvlIdx = level - 1;
        Rect r = go.Rt().rect;
        mpScl = V3.V(r.width / Screen.width, r.height / Screen.height, 1);
        songs = Lis(new Song(path, "1"), new Song(path, "2"), new Song(path, "3"));
        for (int i = 0; i < lvlCnts.Count; i++) {
            lvls.Add(go.Child<GameMb>(0, i));
            lvls[i].Data();
            lvls[i].Init();
            lvls[i].data.go.Tls(V3.O);
        }
        B.Idle();
        if (IsAuto) {
            lvl.go.Tls(V3.I);
            lvl.CrtData();
        } else B.LevelSelectShow();
    }
    public void Fill(int i) { B.LevelBarFill(i, 4); }
    public IEnumerator WinCor(float tm = 0.5f) {
        isWin = true;
        if (B.gameIdx + 1 == lvlCnts[B.lvlIdx]) {
            yield return B.LevelHideCor(lvl.data);
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            B.TxtCol(lvl.data, tm, true);
            yield return lvl.WinCor(false, tm);
            B.gameIdx++;
            lvl.CrtData();
            B.TxtCol(lvl.data, tm, false);
            yield return lvl.WinCor(true, tm);
        }
        isWin = false;
    }
    public void PlaySong() { B.PlaySong(songs[B.lvlIdx]); }
    public void Replay() { StaCor(B.WinHideCor(lvl.data, lvl.CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx == lvlCnts.Count - 1) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, lvls[B.lvlIdx].CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, lvls[idx].CrtData, idx)); }
}