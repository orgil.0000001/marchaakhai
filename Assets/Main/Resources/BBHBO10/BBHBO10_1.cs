using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO10_1 : GameMb {
    BBHBO10 main => BBHBO10._;
    public RectTransform btnTxtPf, btnImgPf, linePf;
    public int ansCnt = 4;
    RectTransform rt, lineRt;
    List<RectTransform> lines = new List<RectTransform>(), sels = new List<RectTransform>();
    List<List<RectTransform>> rts = new List<List<RectTransform>>() { new List<RectTransform>(), new List<RectTransform>() };
    bool isAnim = false;
    Transform parTf;
    class Ans {
        public string name, txt;
        public Sprite spr;
        public Ans(int num) {
            name = "" + num;
            txt = Song.txts[1][num - 1].txt;
            spr = O.LoadSpr(BBHBO10.path + "z1_" + num);
        }
    }
    List<Ans> answers = new List<Ans>();
    Vector3 sz2;
    public override void Init() {
        sz2 = V3.Scl((main.Rt().sizeDelta / 2).V3(), main.Tls);
        data.txt.text = Song.txts[0][1].txt;
        for (int i = 1; i <= 5; i++) answers.Add(new Ans(i));
        parTf = go.Child(1);
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
            answers.RndShuffle();
        }
        parTf.DstChilds();
        rts[0].Clear();
        rts[1].Clear();
        lines.Clear();
        sels.Clear();
        List<List<int>> idxs = new List<List<int>>() { new List<int>(), new List<int>() };
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < ansCnt; idxs[i].Add(j++)) ;
            idxs[i].RndShuffle();
        }
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < ansCnt; j++) {
                Ans ans = answers[B.gameIdx * ansCnt + idxs[i][j]];
                RectTransform btn = Ins(i == 1 ? btnTxtPf : btnImgPf, parTf);
                btn.name = ans.name;
                btn.Tlp(V3.Xy(-400 + j * 200, i == 1 ? -122 : 122));
                if (i == 1) btn.Child(0).Txt().text = ans.txt;
                else btn.Child(0).Img().sprite = ans.spr;
                rts[i].Add(btn);
            }
        CrtLine();
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        if (!isShow) {
            yield return Wf.Sec(1);
            foreach (var tfs in rts)
                foreach (var b in tfs)
                    StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
            foreach (var b in lines)
                StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
            yield return Wf.Sec(tm);
        } else {
            foreach (var tfs in rts)
                foreach (var b in tfs)
                    StaCor(SclCor(b.gameObject, V3.O, V3.I, tm, EaseTp.OutSine));
            yield return Wf.Sec(tm);
        }
    }
    void Update() {
        if (B.lvlIdx == 0) {
            if (IsMbD && !isAnim) {
                rt = Rt();
                if (rt && !sels.Contains(rt)) {
                    mp = RtP(rt);
                    UpdBtn(rt, SelTp.Clk);
                } else {
                    rt = null;
                }
            }
            if (IsMb && rt) {
                RectTransform rt2 = Rt();
                if (rt == rt2) {
                    lineRt.Hide();
                } else {
                    Vector3 a = mp, b = !rt2 || rt.Tp().y > sz2.y == rt2.Tp().y > sz2.y || sels.Contains(rt2) ? Mp : RtP(rt2);
                    lineRt.position = (a + b) / 2;
                    lineRt.right = b - a;
                    lineRt.sizeDelta = lineRt.sizeDelta.X(V3.Dis(a, b) * main.mpScl.y);
                    lineRt.Show();
                }
            }
            if (IsMbU && rt) {
                RectTransform rt2 = Rt();
                if (rt2 && rt != rt2 && rt.name == rt2.name) {
                    B.PlaySong(true);
                    StaCor(CorrectCor(rt, rt2));
                } else if (rt2 && rt.Tp().y > sz2.y != rt2.Tp().y > sz2.y && !sels.Contains(rt2)) {
                    B.PlaySong(false);
                    StaCor(WrongCor(rt, rt2));
                } else {
                    B.PlaySong(false);
                    UpdBtn(rt, SelTp.None);
                    lineRt.Hide();
                }
                rt = null;
            }
        }
    }
    Vector3 RtP(RectTransform rt) { return rt.TfPnt(V3.Y(rt.sizeDelta.y / (rt.Tp().y > sz2.y ? -2 : 2))); }
    RectTransform Rt() {
        int i0 = rts[0].FindIndex(rt => rt.IsRt(Mp)), i1 = rts[1].FindIndex(rt => rt.IsRt(Mp));
        return i0 >= 0 ? rts[0][i0] : i1 >= 0 ? rts[1][i1] : null;
    }
    IEnumerator CorrectCor(RectTransform rt, RectTransform rt2) {
        isAnim = true;
        UpdBtn(rt2, SelTp.Clk);
        UpdLine(SelTp.Clk);
        lineRt.Show();
        yield return null;
        sels.Add(rt, rt2);
        CrtLine();
        main.Fill(B.gameIdx * ansCnt + sels.Count / 2);
        if (sels.Count == ansCnt * 2) {
            B.Good();
            yield return Wf.Sec(2);
            StaCor(main.WinCor());
        }
        isAnim = false;
    }
    IEnumerator WrongCor(RectTransform rt, RectTransform rt2) {
        isAnim = true;
        UpdBtn(rt, SelTp.Wrong);
        UpdBtn(rt2, SelTp.Wrong);
        UpdLine(SelTp.Wrong);
        lineRt.Show();
        yield return Wf.Sec(1);
        UpdBtn(rt, SelTp.None);
        UpdBtn(rt2, SelTp.None);
        UpdLine(SelTp.None);
        lineRt.Hide();
        isAnim = false;
    }
    enum SelTp { None, Clk, Wrong }
    void UpdBtn(RectTransform rt, SelTp tp) { rt.Img().color = tp == SelTp.Clk ? C.lime : tp == SelTp.Wrong ? C.lightCoral : C.I; }
    void UpdLine(SelTp tp) { lineRt.Img().color = tp == SelTp.Clk ? C.limeGreen : tp == SelTp.Wrong ? C.lightCoral : C.black; }
    void CrtLine() {
        lineRt = Ins(linePf, parTf);
        lineRt.Hide();
        lines.Add(lineRt);
    }
}