using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO10_2 : GameMb {
    BBHBO10 main => BBHBO10._;
    List<Transform> tfs;
    List<List<Transform>> tfs2 = new List<List<Transform>>();
    Image barImg;
    public override void Init() {
        tfs = Lis(go.Child(1, 0, 4), go.Child(1, 0, 3), go.Child(1, 0, 1), go.Child(1, 0, 2));
        barImg = go.Child(1, 0, 5, 0).Img();
        for (int i = 0; i < 4; i++) {
            Transform pTf = go.Child(1, i + 1);
            pTf.name = "" + i;
            List<Transform> l = new List<Transform>();
            for (int j = 0; j < pTf.childCount; j++) {
                Transform pTf2 = pTf.Child(j);
                pTf2.name = "" + j;
                l.Add(pTf2);
                for (int k = 0; k < pTf2.childCount; k++) {
                    Button btn = pTf2.Child(k).Ac<Button>();
                    btn.transition = Selectable.Transition.None;
                    btn.onClick.AddListener(() => StaCor(BtnClkCor(pTf2)));
                }
            }
            tfs2.Add(l);
        }
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
        }
        data.txt.text = Song.txts[2][B.gameIdx].txt;
        barImg.fillAmount = 0;
        datas = Lis(0, 0, 0, 0);
        for (int i = 0; i < tfs2.Count; i++)
            for (int j = 0; j < tfs2[i].Count; j++)
                tfs2[i][j].SibIdx(Rnd.Idx(tfs2[i].Count));
        tfs.ForEach(tf => tf.Childs().ForEach(tf => tf.Hide()));
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(Lis(tfs[0].parent).Add(tfs2[0]).Add(tfs2[1]).Add(tfs2[2]).Add(tfs2[3]), tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
    List<int> datas;
    IEnumerator BtnClkCor(Transform pTf) {
        int tp = pTf.parent.name.I(), tp2 = pTf.name.I();
        for (int i = 0; i < tfs[tp].childCount; i++)
            tfs[tp].ChildAct(i == tp2, i);
        bool isCorrect = tp2 == B.gameIdx;
        datas[tp] = isCorrect ? 1 : 0;
        int sum = datas[0] + datas[1] + datas[2] + datas[3];
        Bar(barImg.fillAmount, sum.F() / datas.Count);
        main.Fill(B.gameIdx * 4 + sum);
        B.PlaySong(isCorrect);
        if (sum == datas.Count) {
            yield return Wf.Sec(1);
            B.Good();
            yield return Wf.Sec(2);
            StaCor(main.WinCor());
        }
    }
    void Bar(float a, float b) { StaCor(Cor(t => barImg.fillAmount = M.Lerp(a, b, t), 0.2f)); }
}