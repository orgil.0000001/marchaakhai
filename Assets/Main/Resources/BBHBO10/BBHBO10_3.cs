using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO10_3 : GameMb {
    BBHBO10 main => BBHBO10._;
    public Sprite defSpr, correctSpr, wrongSpr;
    List<List<Sprite>> sprs = new List<List<Sprite>>();
    List<RectTransform> rts = new List<RectTransform>();
    List<Button> btns = new List<Button>();
    List<int> corrects = new List<int>();
    public override void Init() {
        for (int i = 1; i <= 5; i++)
            sprs.Add(Lis(O.LoadSpr(BBHBO10.path + "z1_" + i)));
        sprs[0].Add(O.LoadSpr(BBHBO10.path + "z1_1_2"));
        for (int i = 0; i < 7; i++) {
            int j = i;
            Button btn = go.Child(1, 0, i).Btn();
            btn.onClick.AddListener(() => StaCor(ClkBtnCor(btn, j)));
            btns.Add(btn);
        }
        for (int i = 0; i < 7; i++)
            rts.Add(go.Child(1, 1, 3, i).Rt());
    }
    IEnumerator ClkBtnCor(Button btn, int i) {
        bool isCorrect = B.gameIdx == 2 ? corrects.Contains(i) : i + 1 == corrects.Count;
        B.Correct(isCorrect);
        btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
        yield return Wf.Sec(2);
        if (!isCorrect) B.gameIdx--;
        StaCor(main.WinCor());
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            main.Fill(0);
            B.LevelBar();
        }
        data.txt.text = Song.txts[3][B.gameIdx].txt;
        List<int> l = new List<int>();
        for (int i = 0, cnt = B.gameIdx == 2 ? 1 : Rnd.RngIn(2, 4); i < 7; i++)
            l.Add(i < cnt ? 0 : Rnd.RngIn(1, 4));
        l.RndShuffle();
        corrects.Clear();
        for (int i = 0; i < 7; i++) {
            btns[i].Img().sprite = defSpr;
            rts[i].Child(0).Img().sprite = sprs[l[i]].Rnd();
            if (B.gameIdx == 1 ? l[i] != 0 : l[i] == 0)
                corrects.Add(i);
        }
        main.PlaySong();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(btns.Parse(btn => btn.Rt()).Add(rts), tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
}