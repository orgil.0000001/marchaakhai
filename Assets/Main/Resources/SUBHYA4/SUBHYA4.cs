using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBHYA4 : Singleton<SUBHYA4> {
    public static string path => nameof(SUBHYA4) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBHYA4), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBHYA4), value ? 1 : 0); }
    }
    List<Lvl> lvls = new List<Lvl>();
    Lvl lvl => lvls[B.lvlIdx];
    Game game => lvl.games[B.gameIdx];
    IEnumerator talkCor;
    bool isWin = false;
    List<Song> songs;
    void Awake() { General.Crt(tf, GameTp.SUBHYA4, Replay, NextLevel, SelectLevel); }
    void Start() {
        Rect r = go.Rt().rect;
        songs = Lis(new Song(path, "1"), new Song(path, "2"), new Song(path, "3"));
        List<int> cnts = Lis(1, 1, 1);
        for (int i = 0; i < cnts.Count; i++)
            lvls.Add(new Lvl(go.ChildGo(0, i), cnts[i], Song.txts[0][i + 1].txt));
        B.Idle();
        if (IsAuto) {
            lvl.data.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    void CrtData() {
        isWin = false;
        game.Init();
        if (B.gameIdx == 0) {
            Fill(0);
            B.LevelBar();
            game.Scl(true);
            game.tf.Tls(game.scl);
        }
        PlaySong();
    }
    void Update() {
        if (!isWin && !B.isSelect) game.Upd();
    }
    void Fill(int i) { B.LevelBarFill(i, game.rts.Count); }
    IEnumerator WinCor(float tm = 0.5f) {
        isWin = true;
        if (B.gameIdx + 1 == lvl.games.Count) {
            yield return Wf.Sec(B.lvlIdx == 2 ? 5 : 2);
            yield return B.LevelHideCor(lvl.data);
            game.tf.Tls(V3.O);
            game.rts.ForEach(rt => rt.Par(game.piecesTf));
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            yield return SclCor(game.tf.gameObject, game.scl, V3.O, tm);
            game.rts.ForEach(rt => rt.Par(game.piecesTf));
            game.Scl(false);
            B.gameIdx++;
            CrtData();
            StaCor(SclCor(game.tf.gameObject, V3.O, game.scl, tm));
            B.SclTm(game.rts, tm, true);
            yield return Wf.Sec(tm);
        }
    }
    public void PlaySong() { B.PlaySong(songs[B.lvlIdx]); }
    public void Replay() { StaCor(B.WinHideCor(lvls[B.lvlIdx].data, CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx + 1 == lvls.Count) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvls[++B.lvlIdx].data, CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvls[idx].data, CrtData, idx)); }
    class Game {
        public Transform tf, piecesTf;
        public List<RectTransform> rts = new List<RectTransform>();
        public Vector3 scl;
        List<Vector3> pnts = new List<Vector3>(), poss = new List<Vector3>();
        List<Quaternion> rots = new List<Quaternion>();
        Quaternion rot;
        RectTransform rt;
        Vector3 mp, rtP;
        int idx, cnt, sibIdx;
        bool isClk => idx >= 0;
        Lvl lvl;
        public Game(Lvl lvl, int idx) {
            this.lvl = lvl;
            tf = lvl.data.go.Child(1, 1 + idx);
            piecesTf = lvl.data.go.Child(2, 0, idx);
            for (int i = 0; i < piecesTf.childCount; i++) {
                rts.Add(piecesTf.Child(i).Rt());
                rts[i].name = "" + (i + 1);
                poss.Add(rts[i].Tp());
                rots.Add(rts[i].Tr());
                if (piecesTf.childCount == tf.childCount)
                    pnts.Add(tf.Child(i).Tp());
            }
            Scl(false);
            scl = tf.Tls();
            tf.Tls(V3.O);
        }
        public void Init() {
            idx = -1;
            cnt = 0;
            Vector3 scl = lvl.data.go.Tls();
            lvl.data.go.Tls(V3.I);
            var l = rts.Copy();
            for (int i = 0; i < rts.Count; i++)
                rts[i].name = "" + (i + 1);
            rts.Shuffle();
            for (int i = 0; i < rts.Count; i++) {
                rts[i].SibIdx(i);
                rts[i].Tp(poss[i]);
                rts[i].Tr(rots[i]);
            }
            rts = l;
            lvl.data.go.Tls(scl);
        }
        public void Scl(bool isBig) { rts.ForEach(rt => rt.Tls(isBig ? V3.I : V3.zpp)); }
        public void Upd() {
            if (IsMbD) {
                idx = rts.FindIndex(rt => rt.IsRt(Mp));
                if (isClk && rts[idx].name != "") {
                    rt = rts[idx];
                    sibIdx = rt.SibIdx();
                    rtP = rt.Tp();
                    rot = rt.Tr();
                    mp = Mp;
                    rt.Par(rt.root);
                } else {
                    idx = -1;
                }
            }
            if (IsMb && isClk) {
                rt.Tp(rtP + Mp - mp);
                rt.Tls(V3.Lerp(rt.Tls(), scl, Dt * 20));
                rt.Tr(Q.Lerp(rt.Tr(), Q.O, Dt * 20));
            }
            if (IsMbU && isClk) {
                rt.Par(tf);
                if (V3.Dis(pnts[idx], rt.Tp()) < 50 * rt.root.Tls().x) {
                    B.PlaySong(true);
                    rt.Tp(pnts[idx]);
                    rt.Tr(Q.O);
                    rt.Tls(V3.I);
                    rt.name = "";
                    cnt++;
                    _.Fill(cnt);
                    if (cnt == rts.Count)
                        _.StaCor(_.WinCor());
                } else {
                    B.PlaySong(false);
                    rt.Par(piecesTf);
                    rt.SibIdx(sibIdx);
                    rt.Tp(rtP);
                    rt.Tr(rot);
                    rt.Tls(V3.I);
                }
                idx = -1;
            }
        }
    }
    class Lvl {
        public LvlData data;
        public List<Game> games = new List<Game>();
        public Lvl(GameObject go, int cnt, string txt) {
            data = new LvlData(go);
            data.txt.text = txt;
            for (int i = 0; i < cnt; i++)
                games.Add(new Game(this, i));
            data.go.Tls(V3.O);
        }
    }
}
