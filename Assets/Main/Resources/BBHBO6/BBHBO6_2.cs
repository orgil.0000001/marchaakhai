using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO6_2 : GameMb {
    BBHBO6 main => BBHBO6._;
    List<(string, Song, string)> datas = new List<(string, Song, string)>();
    int idx = 0, cnt = 4;
    public override void Init() {
        Transform pTf = go.Child(1);
        List<string> l = Lis("Eye", "Ear", "Nose", "Mouth", "Arm", "Leg");
        for (int i = 0; i < l.Count; i++) {
            Transform pTf2 = pTf.ChildName(l[i]);
            for (int j = 0; j < pTf2.childCount; j++)
                pTf2.Child(j).Btn().onClick.AddListener(() => StaCor(ClkBtnCor(pTf2)));
            datas.Add((Song.txts[2][i].txt, new Song(BBHBO6.path, "2_" + (i + 1)), pTf2.name));
        }
    }
    bool isClk = false;
    IEnumerator ClkBtnCor(Transform pTf, float tm = 0.2f, float tm2 = 1) {
        if (!isClk && idx < cnt) {
            isClk = true;
            var l = pTf.Childs().Parse(e => e.Img());
            if (pTf.name == datas[idx].Item3) {
                B.PlaySong(true);
                yield return ChangeCor(l, C.I, C.lime, tm);
                yield return Wf.Sec(tm2);
                yield return ChangeCor(l, C.lime, C.I, tm);
                idx++;
                main.Fill(idx);
                if (idx == cnt) {
                    B.Good();
                    yield return Wf.Sec(2);
                    StaCor(main.WinCor());
                } else {
                    StaCor(Cor());
                }
            } else {
                B.PlaySong(false);
                yield return ChangeCor(l, C.I, C.lightCoral, tm);
                yield return Wf.Sec(tm2);
                yield return ChangeCor(l, C.lightCoral, C.I, tm);
            }
            isClk = false;
        }
    }
    IEnumerator ChangeCor(List<Image> l, Color a, Color b, float tm) {
        l.ForEach(e => StaCor(Cor(t => e.color = C.Lerp(a, b, t), tm)));
        yield return Wf.Sec(tm);
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            datas.RndShuffle();
            idx = 0;
            StaCor(Cor());
            main.Fill(0);
            B.LevelBar();
        }
    }
    public void PlaySong() { B.PlaySong(datas[idx].Item2); }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    IEnumerator Cor(float tm = 0.5f) {
        if (idx > 0) {
            B.TxtCol(data, tm, true);
            yield return Wf.Sec(tm);
        }
        data.txt.text = datas[idx].Item1;
        B.PlaySong(datas[idx].Item2);
        if (idx > 0) {
            B.TxtCol(data, tm, false);
            yield return Wf.Sec(tm);
        }
    }
}