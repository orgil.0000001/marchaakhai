using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO6_3 : GameMb {
    BBHBO6 main => BBHBO6._;
    List<Button> btns = new List<Button>();
    List<((string, Song), List<(Song, Sprite)>)> datas = new List<((string, Song), List<(Song, Sprite)>)>();
    public override void Init() {
        for (int i = 0; i < 2; i++) {
            Button btn = go.Child(i + 1).Btn();
            btn.onClick.AddListener(() => StaCor(BtnClkCor(btn)));
            btns.Add(btn);
        }
        for (int i = 0; i < 3; i++)
            datas.Add(((Song.txts[0][2 + i].txt, new Song(BBHBO6.path, "3_" + (i + 1))), Lis(
                (new Song(BBHBO6.path, "3_" + (i + 1) + "z"), O.LoadSpr(BBHBO6.path + "z3_" + (i + 1) + "z")),
                (new Song(BBHBO6.path, "3_" + (i + 1) + "b"), O.LoadSpr(BBHBO6.path + "z3_" + (i + 1) + "b"))
            )));
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            datas.RndShuffle();
            main.Fill(0);
            B.LevelBar();
        }
        data.txt.text = datas[B.gameIdx].Item1.Item1;
        for (int i = 0, j = Rnd.B ? 1 : 0; i < 2; i++) {
            btns[i].Img().color = C.I;
            btns[i].Child(0, 0).Img().sprite = datas[B.gameIdx].Item2[i == j ? 0 : 1].Item2;
            btns[i].name = i == j ? "1" : "0";
        }
        PlaySong();
    }
    public void PlaySong() { B.PlaySong(datas[B.gameIdx].Item1.Item2); }
    bool isClk = false;
    IEnumerator BtnClkCor(Button btn, float tm = 0.2f) {
        if (!isClk) {
            isClk = true;
            bool isCorrect = btn.name == "1";
            btn.Img().color = isCorrect ? C.lime : C.lightCoral;
            Song song = datas[B.gameIdx].Item2[isCorrect ? 0 : 1].Item1;
            B.PlaySong(song);
            yield return Wf.Sec(song.len);
            B.Correct(isCorrect);
            yield return Wf.Sec(2);
            if (!isCorrect) B.gameIdx--;
            else main.Fill(B.gameIdx + 1);
            StaCor(main.WinCor());
            isClk = false;
        }
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        B.SclTm(btns, tm, isShow, b => b.gameObject);
        yield return Wf.Sec(tm);
    }
}