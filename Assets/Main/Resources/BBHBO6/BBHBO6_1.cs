using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class BBHBO6_1 : GameMb {
    BBHBO6 main => BBHBO6._;
    List<(Transform, Song)> btns = new List<(Transform, Song)>();
    int idx = 0;
    public override void Init() {
        data.txt.text = Song.txts[0][1].txt;
        for (int i = 0; i < 8; i++) {
            var e = (go.Child(2, i), new Song(BBHBO6.path, "1_" + (i + 1)));
            e.Item1.Child(0).Txt().text = Song.txts[1][i].txt;
            e.Item1.Child(1).Btn().onClick.AddListener(() => B.PlaySong(e.Item2, false));
            btns.Add(e);
        }
        Transform pTf = go.Child(1);
        for (int i = 1; i < pTf.childCount; i++) {
            Transform pTf2 = pTf.Child(i);
            for (int j = 0; j < pTf2.childCount; j++)
                pTf2.Child(j).Btn().onClick.AddListener(() => StaCor(ClkBtnCor(pTf2)));
        }
    }
    bool isClk = false;
    IEnumerator ClkBtnCor(Transform pTf, float tm = 0.2f, float tm2 = 1) {
        if (!isClk && idx < btns.Count) {
            isClk = true;
            var l = pTf.Childs().Parse(e => e.Img());
            if (pTf.name == btns[idx].Item1.name) {
                B.PlaySong(true);
                yield return ChangeCor(l, C.lime, tm, tm2);
                idx++;
                main.Fill(idx);
                ShowBtn();
                if (idx == btns.Count) {
                    B.Good();
                    yield return Wf.Sec(2);
                    StaCor(main.WinCor());
                }
            } else {
                B.PlaySong(false);
                yield return ChangeCor(l, C.lightCoral, tm, tm2);
            }
            isClk = false;
        }
    }
    IEnumerator ChangeCor(List<Image> l, Color c, float tm, float tm2) {
        if (l[0].Par().name == "Leg") {
            List<Image> l2 = l[0].Par().SibName("Knee").Childs().Parse(e => e.Img());
            StaCor(ChangeCor(l, C.I, c, tm));
            StaCor(ChangeCor(l2, C.I, c, tm));
            yield return Wf.Sec(tm + tm2);
            StaCor(ChangeCor(l, c, C.I, tm));
            StaCor(ChangeCor(l2, c, C.I, tm));
            yield return Wf.Sec(tm);
        } else {
            yield return ChangeCor(l, C.I, c, tm);
            yield return Wf.Sec(tm2);
            yield return ChangeCor(l, c, C.I, tm);
        }
    }
    IEnumerator ChangeCor(List<Image> l, Color a, Color b, float tm) {
        l.ForEach(e => StaCor(Cor(t => e.color = C.Lerp(a, b, t), tm)));
        yield return Wf.Sec(tm);
    }
    public override void CrtData() {
        if (B.gameIdx == 0) {
            btns.RndShuffle();
            for (int i = 0; i < btns.Count; i++) {
                btns[i].Item1.SibIdx(i);
                btns[i].Item1.Tls(V3.O);
                btns[i].Item1.Child(1).Tls(V3.O);
            }
            idx = 0;
            StaCor(Cor());
            main.Fill(0);
            B.LevelBar();
        } else main.PlaySong();
    }
    IEnumerator Cor() {
        main.PlaySong();
        yield return Wf.Sec(main.songs[0].len);
        ShowBtn();
    }
    public override IEnumerator WinCor(bool isShow, float tm) {
        yield return null;
    }
    void ShowBtn(float tm = 0.5f) {
        if (idx > 0) {
            StaCor(SclCor(btns[idx - 1].Item1.gameObject, V3.V(1.2f), V3.I, tm, EaseTp.InSine));
            StaCor(SclCor(btns[idx - 1].Item1.ChildGo(1), V3.I, V3.O, tm, EaseTp.InSine));
        }
        if (idx < btns.Count) {
            B.PlaySong(btns[idx].Item2, false);
            StaCor(SclCor(btns[idx].Item1.gameObject, V3.O, V3.V(1.2f), tm, EaseTp.OutSine));
            StaCor(SclCor(btns[idx].Item1.ChildGo(1), V3.O, V3.I, tm, EaseTp.InSine));
        }
    }
}