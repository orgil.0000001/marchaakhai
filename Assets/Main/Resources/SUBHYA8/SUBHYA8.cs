using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBHYA8 : Mb {
    public static string path => nameof(SUBHYA8) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBHYA8), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBHYA8), value ? 1 : 0); }
    }
    public Sprite defSpr, correctSpr, wrongSpr;
    bool isWin = false;
    List<LvlData> lvlDatas = new List<LvlData>();
    LvlData lvlData => lvlDatas[B.lvlIdx];
    List<int> cnts = Lis(1, 1, 3);
    // Level 1
    List<Transform> tfs1 = new List<Transform>();
    List<RectTransform> rts1 = new List<RectTransform>();
    List<Vector3> pnts1 = new List<Vector3>(), scl1 = new List<Vector3>();
    RectTransform rt, carRt;
    Image personImg;
    GameObject boardGo;
    List<bool> clks;
    // Level 2
    List<Sprite> data2 = new List<Sprite>();
    List<List<Button>> btns2 = new List<List<Button>>();
    // Level 3
    List<Transform> tfs3 = new List<Transform>();
    List<RectTransform> rts3 = new List<RectTransform>();
    List<int> data3 = new List<int>();
    Transform parTf;
    List<Song> songs;
    Vector3 rtP, scl;
    int idx = -1;
    bool isClk => idx >= 0;
    List<Sprite> sprs = new List<Sprite>();
    void Awake() { General.Crt(tf, GameTp.SUBHYA8, Replay, NextLevel, SelectLevel); }
    void Start() {
        sprs = Resources.LoadAll<Sprite>(nameof(SUBHYA8) + "/Sprites/Jijgee").Lis();
        Rect r = go.Rt().rect;
        songs = Lis(new Song(path, "1"), new Song(path, "2"), new Song(path, "3"));
        for (int i = 0; i < 4; i++)
            data2.Add(O.LoadSpr(path + "z2_" + (i + 1)));
        for (int i = 0; i < 3; i++) {
            lvlDatas.Add(new LvlData(go.ChildGo(0, i)));
            lvlDatas[i].txt.text = Song.txts[0][i + 1].txt;
        }
        Transform lvl1Tf = lvlDatas[0].go.Child(1), lvl2Tf = lvlDatas[1].go.Child(1);
        lvl1Tf.ChildHide(2);
        personImg = lvl1Tf.Child(3).Img();
        carRt = lvl1Tf.Child(4).Rt();
        tfs1 = Lis(lvl1Tf.Child(0), lvl1Tf.Child(1));
        rts1 = Lis(lvl1Tf.Child(6).Rt(), lvl1Tf.Child(7).Rt());
        pnts1 = Lis(personImg.Tlp(), lvl1Tf.Child(2).Tlp());
        scl1 = Lis(rts1[0].Tls(), rts1[1].Tls());
        for (int i = 0; i < 10; i++) {
            tfs3.Add(lvlDatas[2].go.Child(1, i, 0));
            rts3.Add(lvlDatas[2].go.Child(2, i).Rt());
        }
        // boardGo = lvl1Tf.ChildGo(5);
        // StaCor(BoardCor());
        for (int i = 0; i < 4; i++) {
            List<Button> l = Lis(lvl2Tf.Child(i, 0).Btn());
            for (int j = 0; j < 4; j++)
                l.Add(lvl2Tf.Child(i, 1, j).Btn());
            btns2.Add(l);
        }
        lvlDatas.ForEach(data => data.go.Tls(V3.O));
        B.Idle();
        if (IsAuto) {
            lvlData.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    void CrtData() {
        if (B.lvlIdx == 0) {
            StaCor(CarCor());
        } else if (B.lvlIdx == 1) {
            Game2(Lis(1, 2, 3, 4));
        } else {
            List<int> l = Lis(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            l.Shuffle();
            if (B.gameIdx == 0) {
                data3.Clear();
                for (int i = 0; i < cnts[B.lvlIdx]; i++)
                    data3.Add(l[i]);
            }
            List<int> l2 = Lis(data3[B.gameIdx]);
            l.Shuffle();
            for (int i = 0; l2.Count < 4; i++)
                if (!l2.Contains(l[i]) && (l2[0] == 0 ? l[i] != 8 : l2[0] == 8 ? l[i] != 0 : true))
                    l2.Add(l[i]);
            for (int i = 0; i < l.Count; i++) {
                rts3[i].Act(l2.Contains(i));
                tfs3[i].ParAct(l2[0] == i);
                rts3[i].SetSiblingIndex(Rnd.Idx(l.Count));
            }
            Vector3 s = lvlDatas[2].go.Tls();
            lvlDatas[2].go.Tls(V3.I);
            rts3[0].Par().UpdLayout();
            lvlDatas[2].go.Tls(s);
        }
        if (B.gameIdx == 0) {
            Fill(0);
            B.LevelBar();
        }
        PlaySong();
    }
    IEnumerator SclCor(int i, float scl = 1.1f, float sclTm = 0.5f, float tm = 0.1f) {
        if (!clks[i]) {
            yield return SclCor(rts1[i].gameObject, scl1[i], scl1[i] * scl, sclTm, EaseTp.OutSine);
            yield return Wf.Sec(tm);
            yield return SclCor(rts1[i].gameObject, scl1[i] * scl, scl1[i], sclTm, EaseTp.InSine);
        }
    }
    IEnumerator SclsCor() {
        while (clks.Contains(false)) {
            StaCor(SclCor(0));
            StaCor(SclCor(1));
            yield return Wf.Sec(1.1f);
        }
    }
    IEnumerator CarCor(float tm = 5) {
        clks = Lis(false, false);
        StaCor(SclsCor());
        personImg.SibIdx(3);
        tfs1.ForEach(tf => tf.Hide());
        rts1.ForEach(tf => tf.Show());
        personImg.Tlp(pnts1[0]);
        rts1[0].name = rts1[1].name = "1";
        while (!isWin) {
            for (int i = 0; i < 2 && !isWin; i++) {
                carRt.TleY(i * 180);
                float t;
                for (t = 0; t < tm && !isWin; t += Dt) {
                    carRt.Tlp(i == 0 ? V3.Xy(M.Lerp(900, -900, t / tm), -20) : V3.Xy(M.Lerp(-900, 900, t / tm), -140));
                    yield return null;
                }
                if (isWin) {
                    float x = carRt.localPosition.x;
                    if (i == 0) {
                        if (x < 190) yield return Cor(t => carRt.TlpX(M.Lerp(x, -900, t)), tm - t);
                        else yield return Cor(t => carRt.TlpX(M.Lerp(x, 290, t)), 0.2f);
                    } else {
                        if (x > -50) yield return Cor(t => carRt.TlpX(M.Lerp(x, 900, t)), tm - t);
                        else yield return Cor(t => carRt.TlpX(M.Lerp(x, -150, t)), 0.2f);
                    }
                }
            }
        }
    }
    void Update() {
        if (B.lvlIdx == 0) {
            if (IsMbD) {
                idx = rts1.FindIndex(rt => rt.IsRt(Mp));
                if (isClk && rts1[idx].name != "") {
                    rt = rts1[idx];
                    rtP = rt.Tp();
                    rt.Tls(scl1[idx]);
                    clks[idx] = true;
                    scl = rt.Tls();
                    mp = Mp;
                } else {
                    idx = -1;
                }
            }
            if (IsMb && isClk) {
                rt.Tp(rtP + Mp - mp);
                rt.Tls(V3.Lerp(rt.Tls(), V3.I, Dt * 20));
            }
            if (IsMbU && isClk) {
                if (V3.Dis(tfs1[idx].Tp(), rt.Tp()) < 50 * Tls.x) {
                    B.PlaySong(true);
                    tfs1[idx].Show();
                    rts1[idx].Hide();
                    rt.name = "";
                    Fill((rts1[0].name == "" ? 1 : 0) + (rts1[1].name == "" ? 1 : 0));
                    if (rts1[0].name == "" && rts1[1].name == "")
                        StaCor(WinCor());
                } else B.PlaySong(false);
                rt.Tp(rtP);
                rt.Tls(scl);
                idx = -1;
                rt = null;
            }
        } else if (B.lvlIdx == 2) {
            if (IsMbD) {
                if (rt = rts3.Find(rt => rt.IsRt(Mp) && rt.ActS())) {
                    rtP = rt.Tp();
                    scl = rt.Tls();
                    parTf = rt.parent;
                    rt.Par(tf);
                    mp = Mp;
                }
            }
            if (IsMb && rt) {
                rt.Tp(rtP + Mp - mp);
                rt.Tls(V3.Lerp(rt.Tls(), tfs3[data3[B.gameIdx]].Tls(), Dt * 20));
            }
            if (IsMbU && rt) {
                if (rt.name == tfs3[data3[B.gameIdx]].name && V3.Dis(tfs3[data3[B.gameIdx]].Tp(), rt.Tp()) < 50 * Tls.x) {
                    B.PlaySong(true);
                    tfs3[data3[B.gameIdx]].Show();
                    rt.Hide();
                    rt.Tp(rtP);
                    rt.Tls(scl);
                    StaCor(WinCor());
                } else B.PlaySong(false);
                rt.Tp(rtP);
                rt.Tls(scl);
                rt.Par(parTf);
                idx = -1;
                rt = null;
            }
        }
    }
    void Fill(int i) { B.LevelBarFill(i, B.lvlIdx == 0 ? rts1.Count : btns2.Count); }
    IEnumerator WinCor(float tm = 0.5f) {
        isWin = true;
        if (B.gameIdx + 1 == cnts[B.lvlIdx]) {
            if (B.lvlIdx == 0) {
                personImg.SibIdx(4);
                yield return Cor(t => {
                    personImg.Tlp(V3.Lerp(pnts1[0], pnts1[1], t));
                    personImg.sprite = sprs[M.RoundI(t * (sprs.Count - 1))];
                }, 3);
                yield return Wf.Sec(2);
            }
            yield return B.LevelHideCor(lvlData);
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            yield return Wf.Sec(1);
            if (B.lvlIdx == 1) {
                foreach (var btns in btns2)
                    foreach (var b in btns)
                        StaCor(SclCor(b.gameObject, V3.I, V3.zpp, tm, EaseTp.InSine));
            } else if (B.lvlIdx == 2) {
                StaCor(SclCor(tfs3[0].ParGo(1), V3.I, V3.O, tm, EaseTp.InSine));
                StaCor(SclCor(rts3[0].ParGo(), V3.I, V3.zpp, tm, EaseTp.InSine));
            }
            yield return Wf.Sec(tm);
            B.gameIdx++;
            CrtData();
            if (B.lvlIdx == 1) {
                foreach (var btns in btns2)
                    foreach (var b in btns)
                        StaCor(SclCor(b.gameObject, V3.zpp, V3.I, tm, EaseTp.OutSine));
            } else if (B.lvlIdx == 2) {
                StaCor(SclCor(tfs3[0].ParGo(1), V3.O, V3.I, tm, EaseTp.OutSine));
                StaCor(SclCor(rts3[0].ParGo(), V3.zpp, V3.I, tm, EaseTp.OutSine));
            }
            yield return Wf.Sec(tm);
        }
        isWin = false;
    }
    public void PlaySong() { B.PlaySong(songs[B.lvlIdx]); }
    public void Replay() { StaCor(B.WinHideCor(lvlDatas[B.lvlIdx], CrtData)); }
    public void NextLevel() {
        if (B.lvlIdx == 2) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvlDatas[++B.lvlIdx], CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvlDatas[idx], CrtData, idx)); }
    List<int> idxs2 = new List<int>();
    void Game2(List<int> l) {
        idxs2.Clear();
        l.Shuffle();
        for (int i = 0; i < btns2.Count; i++) {
            btns2[i][0].Img().sprite = data2[l[i] - 1];
            List<int> l2 = Lis(0, 1, 2, 3);
            l2.Shuffle();
            for (int j = 0; j < 4; j++) {
                Button btn = btns2[i][j + 1];
                btn.Img().sprite = defSpr;
                btn.Child(0).Img().sprite = data2[l[i] - 1];
                btn.Child(0).Tle(V3.Z(l2[j] * 90));
                bool isCorrect = l2[j] == 0;
                int i2 = i;
                btn.onClick.RemoveAllListeners();
                btn.onClick.AddListener(() => StaCor(BtnCor(btn, i2, isCorrect)));
            }
        }
        if (B.gameIdx == 0)
            btns2.ForEach(btns => btns.ForEach(b => b.Tls(V3.I)));
    }
    bool isBtnClk = false;
    IEnumerator BtnCor(Button btn, int i, bool isCorrect) {
        if (!isBtnClk) {
            isBtnClk = true;
            if (!idxs2.Contains(i)) {
                btn.Img().sprite = isCorrect ? correctSpr : wrongSpr;
                B.Correct(isCorrect);
                if (isCorrect) {
                    idxs2.Add(i);
                    Fill(idxs2.Count);
                    if (idxs2.Count == btns2.Count) {
                        yield return Wf.Sec(2);
                        yield return WinCor();
                    }
                } else {
                    yield return Wf.Sec(1);
                    btn.Img().sprite = defSpr;
                }
            }
            isBtnClk = false;
        }
    }
}