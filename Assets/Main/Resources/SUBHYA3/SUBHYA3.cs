using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class SUBHYA3 : Singleton<SUBHYA3> {
    public static string path => nameof(SUBHYA3) + "/Data/";
    public static bool IsAuto {
        get { return PlayerPrefs.GetInt(nameof(SUBHYA3), 1) == 1; }
        set { PlayerPrefs.SetInt(nameof(SUBHYA3), value ? 1 : 0); }
    }
    class Ans {
        public string name;
        public Sprite spr;
        public Song song;
        public Ans(string s, string s2 = "") {
            name = s;
            spr = O.LoadSpr(path + "z" + s);
            song = new Song(path, s2 == "" ? s : s2);
        }
    }
    public RectTransform btnPf, btn2Pf, linePf;
    public Sprite defSpr, correctSpr, wrongSpr;
    bool isWin = true;
    bool isLvl1 => B.lvlIdx == 0;
    List<LvlData> lvlDatas = new List<LvlData>();
    LvlData lvlData => lvlDatas[B.lvlIdx];
    Transform lvl1Tf, lvl2Tf;
    List<List<int>> data1 = Lis(Lis(1, 1, 1), Lis(2, 2, 1), Lis(3, 3, 1), Lis(4, 4, 1), Lis(5, 1, 1));
    List<string> signData = Lis("d4_ongots_buulaa", "d4_solongo_harlaa", "d4_nom_surlaa", "d4_honh_duugarlaa", "d4_tsagaan_ongots_buulaa");
    List<List<int>> data2;
    List<int> cnts = Lis(3, 3);
    // Level 1
    Game1 game1 = new Game1();
    List<List<Ans>> ans1 = new List<List<Ans>>();
    Vector4 sz1 = V4.V(100, 90, 5, 150);
    // Level 2
    Game2 game2 = new Game2();
    List<Ans> ans2 = new List<Ans>();
    Vector3 sz2 = V3.V(200, 176, 20);
    List<Song> songs;
    Vector3 mpScl;
    void Awake() { General.Crt(tf, GameTp.SUBHYA3, Replay, NextLevel, SelectLevel); }
    void Start() {
        Rect r = go.Rt().rect;
        mpScl = V3.V(r.width / Screen.width, r.height / Screen.height, 1);
        songs = Lis(new Song(path, "1"), new Song(path, "2"));
        List<int> ansCnt = Lis(5, 4, 1);
        for (int i = 0; i < ansCnt.Count; i++) {
            List<Ans> l = new List<Ans>();
            for (int j = 0; j < ansCnt[i]; j++)
                l.Add(new Ans((i + 1) + "_" + (j + 1)));
            ans1.Add(l);
        }
        var dat = Song.tp2 == 0 ? (11, "m", Lis(Lis(1, 2, 3), Lis(4, 5, 3), Lis(6, 7, 3), Lis(8, 9, 10, 3), Lis(11, 1, 2, 3))) :
            Song.tp2 == 1 ? (14, "k", Lis(Lis(1, 2, 3), Lis(4, 5, 6), Lis(7, 8, 9, 10), Lis(12, 13, 14), Lis(11, 12, 13, 14))) :
            (9, "t", Lis(Lis(1, 2, 3), Lis(4, 5, 3), Lis(6, 7, 3), Lis(8, 9, 3)));
        for (int i = 0; i < dat.Item1; i++)
            ans2.Add(new Ans(dat.Item2 + (i + 1), "_" + (i + 1)));
        data2 = dat.Item3;
        for (int i = 0; i < 2; i++) {
            lvlDatas.Add(new LvlData(go.ChildGo(0, i)));
            lvlDatas[i].txt.text = Song.txts[0][i + 1].txt;
        }
        lvl1Tf = lvlDatas[0].go.Child(1);
        lvl2Tf = lvlDatas[1].go.Child(1);
        lvlDatas.ForEach(data => data.go.Tls(V3.O));
        B.Idle();
        if (IsAuto) {
            lvlData.go.Tls(V3.I);
            CrtData();
        } else B.LevelSelectShow();
    }
    void CrtData() {
        isWin = false;
        if (isLvl1) {
            if (B.gameIdx == 0) data1.Shuffle();
            game1.Init(Lis(5, 4, 1).Add(data1[B.gameIdx]));
        } else {
            if (B.gameIdx == 0) data2.Shuffle();
            game2.Init(data2[B.gameIdx], B.gameIdx == 0);
        }
        if (B.gameIdx == 0) {
            Fill(0);
            B.LevelBar();
        }
        PlaySong();
    }
    void Update() {
        if (!isWin)
            if (isLvl1) {
                game1.Upd();
                if (game1.isWin) StaCor(WinCor());
            } else {
                game2.Upd();
                if (game2.isWin) StaCor(WinCor());
            }
    }
    void Fill(int i) { B.LevelBarFill(i, cnts[B.lvlIdx]); }
    IEnumerator WinCor(float tm = 0.5f) {
        isWin = true;
        Fill(B.gameIdx + 1);
        if (B.gameIdx + 1 == cnts[B.lvlIdx]) {
            yield return B.LevelHideCor(lvlData);
            B.gameIdx = 0;
            if (IsAuto) StaCor(B.WinShowCor());
            else B.LevelSelectShow();
        } else {
            yield return Wf.Sec(1);
            if (isLvl1) {
                foreach (var tfs in game1.rts)
                    foreach (var b in tfs)
                        StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
                foreach (var b in game1.lines)
                    StaCor(SclCor(b.gameObject, V3.I, V3.O, tm, EaseTp.InSine));
            } else {
                B.SclTm(game2.rts, tm, false);
            }
            yield return Wf.Sec(tm);
            B.gameIdx++;
            CrtData();
            if (isLvl1) {
                foreach (var tfs in game1.rts)
                    foreach (var b in tfs)
                        StaCor(SclCor(b.gameObject, V3.O, V3.I, tm, EaseTp.OutSine));
            } else {
                B.SclTm(game2.rts, tm, true);
            }
            yield return Wf.Sec(tm);
        }
    }
    IEnumerator TalkCor(float tm = 0.2f) {
        if (!isLvl1) B.PlaySign(nameof(SUBHYA3) + "/" + signData[B.gameIdx]);
        for (int i = 0, n = isLvl1 ? data1[B.gameIdx].Count : data2[B.gameIdx].Count; i < n; i++) {
            Song song = isLvl1 ? ans1[i][data1[B.gameIdx][i] - 1].song : ans2[data2[B.gameIdx][i] - 1].song;
            B.PlaySong(song, isLvl1);
            yield return Wf.Sec(song.len / B.src.pitch);
            yield return Wf.Sec(tm);
        }
    }
    IEnumerator talkCor;
    public void PlayTalk() {
        if (isLvl1 ? !game1.isAnim : !game2.isAnim) {
            StopTalk();
            StaCor(talkCor = TalkCor());
        }
    }
    void StopTalk() { if (talkCor.NotNull()) StopCor(talkCor); }
    IEnumerator SongCor() {
        B.PlaySong(songs[B.lvlIdx]);
        yield return Wf.Sec(songs[B.lvlIdx].len / B.src.pitch);
        if (isLvl1 ? !game1.isAnim : !game2.isAnim)
            yield return talkCor = TalkCor();
    }
    public void PlaySong() { StaCor(SongCor()); }
    public void Replay() { StaCor(B.WinHideCor(lvlDatas[B.lvlIdx], CrtData)); }
    public void NextLevel() {
        if (!isLvl1) {
            IsAuto = false;
            General.Celebrate();
        } else StaCor(B.WinHideCor(lvlDatas[++B.lvlIdx], CrtData));
    }
    public void SelectLevel(int idx) { StaCor(B.SelectLevelCor(lvlDatas[idx], CrtData, idx)); }
    class Game1 : Mb2 {
        public List<int> n = new List<int>(), data = new List<int>();
        public List<RectTransform> lines = new List<RectTransform>();
        public List<List<RectTransform>> rts = new List<List<RectTransform>>();
        Dictionary<RectTransform, Song> songDic = new Dictionary<RectTransform, Song>();
        RectTransform rt, lineRt;
        public bool isWin, isAnim;
        int idx = 0;
        string Nm(int i) { return _.ans1[i][data[i] - 1].name; }
        bool Check(int i, RectTransform rt) { return Nm(i) == rt.name; }
        public void Init(List<int> l0) {
            isAnim = isWin = false;
            n.Clear();
            data.Clear();
            for (int i = 0; i < l0.Count; i++) {
                if (i < l0.Count / 2) n.Add(l0[i]);
                else data.Add(l0[i]);
            }
            List<List<int>> idxs = new List<List<int>>();
            for (int i = 0; i < n.Count; i++) {
                List<int> l = Lis(data[i] - 1), l2 = new List<int>();
                for (int j = 0; j < n[i]; l2.Add(j++)) ;
                l2.Rmv(l[0]);
                l.Add(Rnd.Lis(n[i] - 1, l2));
                l.Shuffle();
                idxs.Add(l);
            }
            _.lvl1Tf.DstChilds();
            idx = 0;
            lines.Clear();
            rts.Clear();
            songDic.Clear();
            for (int i = 0; i < n.Count; i++) {
                List<RectTransform> tfs = new List<RectTransform>();
                for (int j = 0; j < n[i]; j++) {
                    RectTransform btn = Ins(_.btnPf, _.lvl1Tf);
                    Ans ans = _.ans1[i][idxs[i][j]];
                    btn.name = ans.name;
                    btn.Tlp(V3.Xy(i.X(n.Count, _.sz1.x, _.sz1.w), -j.X(n[i], _.sz1.x, _.sz1.z)));
                    btn.sizeDelta = V2.V(_.sz1.x);
                    btn.Child(0, 0).Img().sprite = ans.spr;
                    btn.Child(0).Rt().sizeDelta = V2.V(_.sz1.y);
                    tfs.Add(btn);
                    songDic.Add(btn, ans.song);
                }
                rts.Add(tfs);
            }
            CrtLine();
        }
        public void Upd() {
            if (!isWin) {
                if (IsMbD && !isAnim) {
                    rt = Rt();
                    if (idx == 0 ? rt : rt && Check(idx, rt)) {
                        mp = RtP(rt, true);
                        if (idx == 0)
                            UpdBtn(rt, SelTp.Clk);
                    } else {
                        rt = null;
                    }
                }
                if (IsMb && rt) {
                    if (rt.IsRt(Mp)) {
                        lineRt.Hide();
                    } else {
                        RectTransform rt2 = Rt();
                        // Vector2 p;
                        // RectTransformUtility.ScreenPointToLocalPointInRectangle(_.tf as RectTransform, Mp, out p);
                        // Vector3 a = mp, b = rt2 ? RtP(rt2, false) : p.V3();
                        Vector3 a = mp, b = rt2 ? RtP(rt2, false) : Mp;
                        lineRt.position = (a + b) / 2;
                        lineRt.right = b - a;
                        lineRt.sizeDelta = lineRt.sizeDelta.X(V3.Dis(a, b) * _.mpScl.y);
                        lineRt.Show();
                    }
                }
                if (IsMbU && rt) {
                    RectTransform rt2 = Rt();
                    if (rt2 && Check(idx, rt) && Check(idx + 1, rt2)) {
                        B.StaCor(CorrectCor(rt, rt2));
                    } else if (rt2) {
                        B.StaCor(WrongCor(rt, rt2));
                    } else {
                        UpdBtn(rt, idx == 0 ? SelTp.None : SelTp.Clk);
                        lineRt.Hide();
                    }
                    rt = null;
                }
            }
        }
        Vector3 RtP(RectTransform rt, bool isR) { return rt.TfPnt(V3.X(rt.sizeDelta.x / (isR ? 2 : -2))); }
        RectTransform Rt() { return rts[rt ? idx + 1 : idx].Find(rt => rt.IsRt(Mp)); }
        IEnumerator CorrectCor(RectTransform rt, RectTransform rt2) {
            isAnim = true;
            UpdBtn(rt2, SelTp.Clk);
            UpdLine(SelTp.Clk);
            lineRt.Show();
            CrtLine();
            yield return TalkCor(rt, rt2);
            if (++idx == n.Count - 1)
                isWin = true;
            isAnim = false;
        }
        IEnumerator WrongCor(RectTransform rt, RectTransform rt2) {
            isAnim = true;
            UpdBtn(rt, SelTp.Wrong);
            UpdBtn(rt2, SelTp.Wrong);
            UpdLine(SelTp.Wrong);
            lineRt.Show();
            yield return TalkCor(rt, rt2);
            UpdBtn(rt, idx == 0 ? SelTp.None : SelTp.Clk);
            UpdBtn(rt2, SelTp.None);
            UpdLine(SelTp.None);
            lineRt.Hide();
            isAnim = false;
        }
        IEnumerator TalkCor(RectTransform rt, RectTransform rt2, float tm = 0.2f) {
            for (int i = 0; i <= idx + 1; i++) {
                RectTransform r = i == 0 && idx == 0 ? rt : i <= idx ? rts[i].Find(rt => rt.name == Nm(i)) : rt2;
                yield return SclCor(r.gameObject, V3.I, V3.V(1.1f), 0.1f, EaseTp.InSine);
                B.PlaySong(songDic[r]);
                yield return Wf.Sec(songDic[r].len / B.src.pitch);
                yield return SclCor(r.gameObject, V3.V(1.1f), V3.I, 0.1f, EaseTp.OutSine);
                yield return Wf.Sec(tm);
            }
        }
        enum SelTp { None, Clk, Wrong }
        void UpdBtn(RectTransform rt, SelTp tp) {
            rt.Img().sprite = tp == SelTp.Clk ? _.correctSpr : tp == SelTp.Wrong ? _.wrongSpr : _.defSpr;
        }
        void UpdLine(SelTp tp) {
            lineRt.Img().color = tp == SelTp.Clk ? C.limeGreen : tp == SelTp.Wrong ? C.lightCoral : C.black;
        }
        void CrtLine() {
            lineRt = Ins(_.linePf, _.lvl1Tf);
            lineRt.Hide();
            lines.Add(lineRt);
        }
    }
    class Game2 : Mb2 {
        public List<RectTransform> rts = new List<RectTransform>();
        public List<Vector3> pnts = new List<Vector3>();
        public List<int> idxs = new List<int>();
        float dy = 0, y = 0;
        int idx;
        RectTransform rt;
        Transform parTf;
        Vector3 rtP;
        bool isNear;
        public bool isWin, isAnim;
        LvlData data => _.lvlDatas[1];
        public void Init(List<int> l, bool isScl) {
            isNear = isAnim = isWin = false;
            Vector3 lvlScl = data.go.Tls(), lvlP = data.go.Tp();
            data.go.Tls(V3.I);
            data.go.Tp(data.staP);
            _.lvl2Tf.DstChilds();
            idx = 0;
            rts.Clear();
            pnts.Clear();
            for (int i = 0; i < l.Count; i++) {
                RectTransform btn = Ins(_.btn2Pf, _.lvl2Tf);
                Ans ans = _.ans2[l[i] - 1];
                btn.name = ans.name;
                btn.Tlp(V3.X(i.X(l.Count, _.sz2.x, _.sz2.z)));
                btn.sizeDelta = V2.V(_.sz2.x);
                btn.Child(0).Img().sprite = ans.spr;
                btn.Child(0).Rt().sizeDelta = V2.V(_.sz2.y);
                rts.Add(btn);
                pnts.Add(rts[i].Tp());
            }
            idxs = 0.Fill(l.Count);
            List<Vector3> pnts2 = pnts.Copy();
            pnts2.Shuffle();
            for (int i = 0; i < pnts2.Count; i++)
                rts[i].Tp(pnts2[i]);
            rts.ForEach(rt => rt.Tls(isScl ? V3.I : V3.zpp));
            data.go.Tls(lvlScl);
            data.go.Tp(lvlP);
        }
        public void Upd() {
            if (!isAnim) {
                if (IsMbD) {
                    isNear = true;
                    rt = rts.Find(btn => btn.Rt().IsRt(Mp));
                    if (rt) {
                        rtP = rt.Tp().Y(pnts[0].y);
                        mp = Mp;
                        parTf = rt.Par();
                        rt.Par(parTf.Par());
                        y = 50;
                    }
                }
                if (IsMb && rt) {
                    rt.Tp(rtP + V3.X(Mp.x - mp.x));
                    int i0 = idx = pnts.NearIdx(rt.Tp());
                    for (int i = 0; i < idxs.Count; i++)
                        idxs[i] = rts[i] == rt ? -1 : pnts.NearIdx(rts[i].Tp());
                    if (idxs.Contains(idx))
                        for (i0 = 0; i0 < idxs.Count && idxs.Contains(i0); i0++) ;
                    if (idx > i0) {
                        for (int i = 0; i < idxs.Count; i++)
                            if (i0 < idxs[i] && idxs[i] <= idx)
                                idxs[i]--;
                    } else if (idx < i0) {
                        for (int i = 0; i < idxs.Count; i++)
                            if (idx <= idxs[i] && idxs[i] < i0)
                                idxs[i]++;
                    }
                }
                if (IsMbU && rt) {
                    rt.Par(parTf);
                    y = 0;
                    idxs[idxs.IndexOf(-1)] = idx;
                    int i;
                    for (i = 0; i < idxs.Count && i == idxs[i]; i++) ;
                    if (i == idxs.Count)
                        _.StaCor(TalkCor());
                }
            }
            if (rt && isNear) {
                int i;
                for (i = 0; i < idxs.Count; i++)
                    if (idxs[i] >= 0)
                        rts[i].Tp(V3.Lerp(rts[i].Tp(), pnts[idxs[i]], Dt * 20));
                if (!idxs.Contains(-1)) {
                    for (i = 0; i < idxs.Count; i++)
                        if (V3.Dis(rts[i].Tp(), pnts[idxs[i]]) > 10 * rt.root.Tls().x)
                            break;
                    if (i == idxs.Count) {
                        dy = 0;
                        isNear = false;
                    }
                }
                dy = M.Lerp(dy, y, Dt * 15);
                rt.TpY(rtP.y + dy);
            }
        }
        IEnumerator TalkCor(float tm = 0.2f) {
            _.StopTalk();
            isAnim = true;
            B.PlaySign(nameof(SUBHYA3) + "/" + _.signData[B.gameIdx]);
            float len = Song.data.lenDic[_.signData[B.gameIdx]], staTm = Time.time;
            for (int i = 0; i < rts.Count; i++) {
                RectTransform r = rts[i];
                yield return SclCor(r.gameObject, V3.I, V3.V(1.1f), 0.1f, EaseTp.InSine);
                Song song = _.ans2[_.data2[B.gameIdx][i] - 1].song;
                B.PlaySong(song, false);
                yield return Wf.Sec(song.clipLen / B.src.pitch);
                yield return SclCor(r.gameObject, V3.V(1.1f), V3.I, 0.1f, EaseTp.OutSine);
                yield return Wf.Sec(tm);
            }
            if (Time.time - staTm < len)
                yield return Wf.Sec(len + staTm - Time.time);
            isWin = true;
        }
    }
}